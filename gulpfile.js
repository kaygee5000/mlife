var gulp = require('gulp'),
  gutil = require('gulp-util'),
  bower = require('bower'),
  concat = require('gulp-concat'),
  sass = require('gulp-sass'),
  minifyCss = require('gulp-minify-css'),
  sh = require('shelljs'),
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),
  rename = require('gulp-rename'),
  notify = require('gulp-notify'),
  cache = require('gulp-cache'),
  livereload = require('gulp-livereload'),
  pkg = require('./package.json'),
  replace = require('gulp-replace-task'),
  templateCache = require('gulp-angular-templatecache'),
  del = require('del');

var paths = {
  sass: ['./scss/**/*.scss']
};

gulp.task('default', ['sass', 'build_mlife']);

gulp.task('sass', function(done) {
  gulp.src('./scss/ionic.app.scss')
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest('./www/css/'))
    .pipe(minifyCss({
      keepSpecialComments: 0
    }))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./www/css/'))
    .on('end', done);
});


gulp.task('install', ['git-check'], function() {
  return bower.commands.install()
    .on('log', function(data) {
      gutil.log('bower', gutil.colors.cyan(data.id), data.message);
    });
});

gulp.task('git-check', function(done) {
  if (!sh.which('git')) {
    console.log(
      '  ' + gutil.colors.red('Git is not installed.'),
      '\n  Git, the version control system, is required to download Ionic.',
      '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
      '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
    );
    process.exit(1);
  }
  done();
});


/*PollAfrique Custom Gulp Additions*/

var src_base = 'mlife_dev/';
var dest_base = 'www/';


gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass', 'stylesheets']);
  gulp.watch(src_base+"index.html", ['index_html']);
  gulp.watch(src_base+"**/*.css", ['stylesheets']);
  gulp.watch(src_base+"**/*.js", ['concat_mlife_scripts']);
  gulp.watch(src_base+"**/*.html", ['concat_mlife_html']);
});

gulp.task('clean_destination_folder', function() {
  //return del([dest_base]);
});

gulp.task('index_html', function () {
  gulp.src(src_base +"index.html")
    .pipe(gulp.dest( dest_base))
    .pipe(notify({ message: 'Index html task complete' }));
});

gulp.task('stylesheets', function() {
  return gulp.src(src_base + '/**/*.css')
    .pipe(concat('style.css'))
    //.pipe(minifycss({compatibility: 'ie8'}))
    //.pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(dest_base + "css"))
    .pipe(notify({ message: 'Stylesheet task complete' }));
});

/*gulp.task('web_scripts', function() {
  var scriptArray = ['website_dev/js/jquery.js', 'website_dev/js/!**!/!(custom)*.js', 'website_dev/js/custom.js'];
  return gulp.src(scriptArray)
    .pipe(concat('mogo.js'))
    .pipe(gulp.dest(website_dest))
    //.pipe(rename({suffix: '.min'}))
    //.pipe(uglify())
    //.pipe(gulp.dest(website_dest))
    .pipe(notify({ message: 'Scripts task complete' }));
});*/

gulp.task('copy_compress_images', function() {
  return gulp.src(src_base +'img/**/*')
    // .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
    .pipe(gulp.dest(dest_base+'/img'))
    .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('concat_mlife_scripts', function () {
  var src_array = [src_base + 'mlife_app.js', src_base + '/**/*.js'];
  return gulp.src(src_array)
    // .pipe(concat('mlife.js'))
    .pipe(concat('app.js'))
    .pipe(gulp.dest(dest_base + '/js'))
});

gulp.task('concat_mlife_html', function () {
  return gulp.src([src_base + '/**/!(index)*.html'])
    .pipe(templateCache('templates.js', {standalone : true}))
    .pipe(gulp.dest(dest_base));
});

gulp.task('build_mlife', ['concat_mlife_scripts', 'concat_mlife_html']);
