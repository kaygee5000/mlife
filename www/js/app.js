/**
 * Created by Kaygee on 08/03/2016.
 */
// angular.module is a global place for creating, registering and retrieving Angular modules
angular.module('mLife', [
  'ionic',
  'ngStorage',
  'ngSanitize',
  'angularMoment',
  'ionicLazyLoad',
  'mn',
  'firebase',
  'ionic-material',
  'ngCordova',
  'ngFileUpload',
  // 'angular-autogrow',
  'ionic-link-tap.tapAction',
  'ionic.ion.headerShrink',
  'ngImgCrop',
  'ionic-zoom-view',
  'pouchdb',
  'cloudinary',
  'angular-cache',
  'alexjoffroy.angular-loaders',
  'templates'
])
  .config(function($stateProvider, $urlRouterProvider,
                   $httpProvider, $ionicConfigProvider, cloudinaryProvider) {

    $urlRouterProvider.otherwise('/login');

    //Http Interceptor to check auth failures for xhr requests and fix in the access token to each request
    $httpProvider.interceptors.push('mlHttpInterceptor');

    // if (!ionic.Platform.isIOS()) {
    //   $ionicConfigProvider.scrolling.jsScrolling(true);
    // }

    cloudinaryProvider
      .set("cloud_name", "meltwater")
      .set("upload_preset", "mlamlifemediaassets");


    $.ajaxSetup({
      /*security token for headers*/
      headers : {
        "token" :  "PnAax2Dy1dje8DCRexHg1IbW8siYrFyRaZ"
      }
    });




    if (!String.prototype.startsWith) {
      String.prototype.startsWith = function(searchString, position){
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
      };
    }

    // if (/webkit.*mobile/i.test(navigator.userAgent)) {
    //   (function($) {
    //     $.fn.offsetOld = $.fn.offset;
    //     $.fn.offset = function() {
    //       var result = this.offsetOld();
    //       result.top -= window.scrollY;
    //       result.left -= window.scrollX;
    //       return result;
    //     };
    //   })(jQuery);
    // }

  });

/**
 * Created by Kaygee on 08/03/2016.
 */

var baseURL = "http://mlife.meltwater.com/wp-content/themes/mlife_mobile_api/api-v1.php";
// var baseURL = "/api-v1.php";
var superAdminURL = "https://admin.meltwater.com/superadmin/rest/";
// var superAdminURL = "/sales.ws/";
var firebaseURL = 'https://mlife-1287.firebaseio.com';
// var firebaseURL = '/firebasecom';


angular.module('mLife')
  .constant('mlConstants', {
    /*this checks if the user is valid*/
    authenticateUser : baseURL + "?action=authenticateUser",

    /*this logs in a user and returns access token with user privileges*/
    loginUser : baseURL /*"?action=login"*/,

    /*this allows a LOGGED IN user to change his password*/
    resetPassword : baseURL + "?action=resetPassword",

    /*get posts*/
    getPosts : baseURL + "?action=getPost",

    /*get slider images for news tab*/
    getTopSlider : baseURL + "?action=getSliderImages",


    /*get comments for a post*/
    getPostComments : baseURL + "?action=fetchComments",

    /*create comment on a post*/
    createPostComment : baseURL + "?action=addPostComment",

    /*edit comment on a post*/
    editPostComment : baseURL + "?action=editComment",

    /*delete comment on a post*/
    deletePostComment : baseURL + "?action=deleteComment",

    /*get comments for a post*/
    getPostLikes : baseURL + "?action=fetchPostLikes",

    /*create comment on a post*/
    createPostLike : baseURL + "?action=doLike",

    /*get categories of the posts*/
    getPostCategories : baseURL + "?action=fetchCategories",

    /*get post of a category*/
    getPostUnderACategory : baseURL + "?action=getPostByCategory",

    /*get all users in the system*/
    getUsersList : baseURL + "?action=getUsersList",

    /*get all users in the system*/
    getUserFavoritePosts : baseURL + "?action=getUserProfilePost",

    /*get all users in the system*/
    storeUserPushId : baseURL + "?action=storePushId",

    /*fetch data from mw Super Admin*/
    superAdmin : superAdminURL + 'today/sales.ws',

    /*do Like of Today Sale*/
    todaySaleLike : superAdminURL + 'today/likeToggle.ws',

    /*do comment on Today Sale*/
    todaySaleComment : superAdminURL + 'today/createComment.ws',

    /*fetch employee sales history from mw Super Admin*/
    salesHistory : superAdminURL + 'employee/getRecentSales.ws',

    /*fetch territory sales history from mw Super Admin*/
    territorySaleHistory : superAdminURL + 'employee/getRecentSalesByTerritory.ws',

    /*fetch data from mw WorkDay*/
    workDay : superAdminURL + 'employee/detail.ws',

    /*fetch all territories*/
    allTerritories : superAdminURL + 'territory/territories.ws',

    /*fetch data from mw WorkDay*/
    allAreasBySuperArea : superAdminURL + 'territory/getAreasBySuperArea.ws',

    /*fetch data from mw WorkDay*/
    allOfficesByArea : superAdminURL + 'territory/getOfficesByArea.ws',

    /*fetch data from mw WorkDay*/
    allEmployeesInAnOffice : superAdminURL + 'employee/getByTerritory.ws',

    /*Push Notification API Token*/
    securityProfile : 'mlife_security_profile',

    /*Push Notification API Token*/
    firebaseUrl  : firebaseURL,


    image_base_url: 'https://s3-ap-southeast-1.amazonaws.com/mogoimagecloud/',
    aws_username :"kaygee",/*this is for mogo cloud*/
    aws_image_bucket :"mogoimagecloud",/* mogoimagecloud - name this is for mogo cloud*/
    aws_bucket_region :"ap-southeast-1",/* - bucket region this is for mogo cloud*/
    aws_access_key : "AKIAIMO3YLSCEWCFZHIQ",/* key for mogocloud*/
    aws_secret_access_key : "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",/* - secret for mogocloud*/

    /*Push Notification API Token Ionic Push*/
    // pushTokenAPI : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJiYTdmMDM0Mi1mOWUxLTQxODgtYjE0NC1iOWRlNDNlNGJjYTcifQ  .buEkLDiDp5INdfL-tYYHEpFLclyeze2qXBDv3hjsYgk'
    pushTokenAPI : 'MjBhMTYyOTAtZGFiNy00ZjA3LTkxMWYtYmRhYTYzMzU3NTI2',


    miniOrangeCertificate : '-----BEGIN CERTIFICATE-----MIID/zCCAuegAwIBAgIJAPmPOnjOfbO/MA0GCSqGSIb3DQEBCwUAMIGVMQswCQYDVQQGEwJJTjEUMBIGA1UECAwLTUFIQVJBU0hUUkExDTALBgNVBAcMBFBVTkUxEzARBgNVBAoMCk1JTklPUkFOR0UxEzARBgNVBAsMCk1JTklPUkFOR0UxEzARBgNVBAMMCk1JTklPUkFOR0UxIjAgBgkqhkiG9w0BCQEWE2luZm9AbWluaW9yYW5nZS5jb20wHhcNMTYwMTMwMDgyMzE0WhcNMjYwMTI3MDgyMzE0WjCBlTELMAkGA1UEBhMCSU4xFDASBgNVBAgMC01BSEFSQVNIVFJBMQ0wCwYDVQQHDARQVU5FMRMwEQYDVQQKDApNSU5JT1JBTkdFMRMwEQYDVQQLDApNSU5JT1JBTkdFMRMwEQYDVQQDDApNSU5JT1JBTkdFMSIwIAYJKoZIhvcNAQkBFhNpbmZvQG1pbmlvcmFuZ2UuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsSKksJcuQbZm/cw67JJkHvlzrzd3+aPLhfqy/FCKj/BmZ7mBZf0s3+89O0rOamsZP1NKzOE29ZL6ONLJHxU48LUbGciZupm/wf7dY/Av34uDcZ61ufKz0u17UTxgjLULIWBy//68EOr4Xbm/4bqhmKcB3CclMaom0LWeCrqittiLqunVCjFIbxXMT010WiBBnFwqjUqfuMnVLL+HrPPqgPqNhiKDdxBxHk9GDPq2+GEruM1jw7TUjVa+zbhekvdNTbj2Fo2sqf+mIkFLSaS6cHg0UeL7sX0wvQFDMwx+hpfRLMcYpFAmRMn3dI2zUnPgwvWeKHrnNOjHVkRTV4hgZQIDAQABo1AwTjAdBgNVHQ4EFgQU85bt1wVl/f2LftBu1jeO499VUbYwHwYDVR0jBBgwFoAU85bt1wVl/f2LftBu1jeO499VUbYwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEApenCY36LGThXLAIRIvDQ6XnHELL7Wm53m3tHy+GA2MxUbcTqQC3tgXM+yC18EstjRHgWdQMtOcq9ohb5/TqWPoYAYnbg1SG9jEHJ3LLaIMI0idYo+zfPCtwliHLnsuZXH6ZU3mh/IQEBqINini6R/cSh9BpIjqwXKpjWoegl9XLI/RQ7Bbbya89TUBwm5KR3deWXdMZEj/d7hV8XdSWyi2CvWTeHIfkZVhcHg1ues9+Mt3kaBr4Z5/NkQPANjfMdKjZ8tfNTN7PgYAYyRW6C8aXcw+w0zIoGrcO2gVM9/3oR4gHm5MUHOMdAyONkg59+T+7NDlN7y4YmVIZQBgHByA==-----END CERTIFICATE-----'


  });

/**
 * Created by Kaygee on 08/03/2016.
 */
// angular.module is a global place for creating, registering and retrieving Angular modules
angular.module('mLife')

  .run(function($ionicPlatform, $rootScope, $state, $ionicSideMenuDelegate, mlOfficeAreaService,
                $localStorage, $timeout, mlAuthService, mlNotifierService, mlPostService, mlSuperAdminService,
                mlFirebaseService, $ionicTabsDelegate, $cordovaAppVersion, $ionicLoading, $ionicPopup, mlKickOffService ) {

    $localStorage.$default({
      notifications : [],
      user : {}
    });



    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        // StatusBar.styleDefault();
        StatusBar.styleLightContent();
        //   $cordovaStatusbar.overlaysWebView(true);
        //   $cordovaStatusBar.style(1); //Light
        // $cordovaStatusBar.style(2); //Black, transulcent
        // $cordovaStatusBar.style(3); //Black, opaque
      }


      var notificationOpenedCallback = function(jsonData) {
        console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
      };

      // Enable to debug issues.
      // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});



      window.plugins.OneSignal.init("29b7a8b4-7e51-4fef-86fe-c0f3d0c566f6",
        {googleProjectNumber: "671515376105"}, notificationOpenedCallback);

      // Show an alert box if a notification comes in when the user is in your app.
      // window.plugins.OneSignal.enableInAppAlertNotification(true);

      window.plugins.OneSignal.getIds(function(ids) {
        $localStorage.user.notificationID = ids.userId;
        $localStorage.user.notificationPushToken = ids.pushToken;

        mlNotifierService.storeUserPushId(ids.userId);
      });

      $rootScope.deviceIsReady = true;



      $cordovaAppVersion.getVersionNumber().then(function (version) {
        $rootScope.appVersion = version;
      });

      $cordovaAppVersion.getVersionCode().then(function (build) {
        $rootScope.appBuild = build;
      });


      $cordovaAppVersion.getAppName().then(function (name) {
        $rootScope.appName = name;
      });


      $cordovaAppVersion.getPackageName().then(function (packageName) {
        $rootScope.appPackage = packageName;
      });

      $rootScope.isIOS = ionic.Platform.isIOS();
      $rootScope.isAndroid = ionic.Platform.isAndroid();
    });

    $rootScope.loadInitialPosts = function() {
      // mlPostService.getPosts()
      //     .then(
      //         function (successData) {
      //             // mlPostService.posts = successData;
      //         }, function (errorData) {
      //             $timeout(function () {
      //                 $rootScope.error = errorData;
      //             });
      //         })
      //     .finally(function () {
      //     });
    };

    mlPostService.getPostCategories()
      .then(
        function (successData) {
          // mlPostService.posts = successData;
        }, function (errorData) {
          // $scope.error = errorData;
        })
      .finally(function () {
        /*  <preference name="AutoHideSplashScreen" value="true"/>*/
      });


    //var isWebView = ionic.Platform.isWebView();
    //var isIPad = ionic.Platform.isIPad();
    //var isIOS = ionic.Platform.isIOS();
    //var isAndroid = ionic.Platform.isAndroid();
    //var isWindowsPhone = ionic.Platform.isWindowsPhone();
    //var currentPlatformVersion = ionic.Platform.version();
    //ionic.Platform.exitApp(); // stops the app

    $rootScope.platformSpecificClass = function (iosClass, androidClass, genericClass) {
      if (!ionic.Platform.isAndroid || ionic.Platform.isIPad()) {
        return iosClass
      }else if (ionic.Platform.isAndroid()) {
        return androidClass
      }else{
        return genericClass
      }
    };

    $rootScope.$on('$stateChangeStart ed', function(event, toState, toStateParams) {
      // track the state the user wants to go to;
      /*if the stat is not the login screen*/
      if (toState.name !== 'login') {
        /*check if he has an authorization token*/
        if (angular.isUndefined($localStorage.authorization)){
          mlNotifierService.alert("Auth Check", "Please confirm your identity.");
          /*prevent him if he doesn't have, and redirect to login*/
          event.preventDefault();
          $state.go('login');
        }
      }
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toStateParams) {
      if($ionicSideMenuDelegate.isOpenLeft()){
        $ionicSideMenuDelegate.toggleLeft(false);
      }
    });

    $rootScope.$localStorage = $localStorage;
    $rootScope.$state = $state;

    $rootScope.mlifeImageCloud = "https://mogoimagecloud.s3-ap-southeast-1.amazonaws.com/";

    mlKickOffService.initKickOff();

    mlSuperAdminService.fetchData();

    mlOfficeAreaService.getSuperAreas();

    $rootScope.increasePostPageViewCount = function () {
      if ($localStorage.user && $localStorage.user.ID) {
        mlFirebaseService.increasePostPageViewCount();
      }
    };

    $rootScope.increaseTodayPageViewCount = function () {
      mlFirebaseService.increaseTodayPageViewCount();
    };

    $rootScope.increaseOfficesPageViewCount = function () {
      mlFirebaseService.increaseOfficesPageViewCount();
    };

    $rootScope.increaseProfilePageViewCount = function () {
      mlFirebaseService.increaseProfilePageViewCount();
    };

    $rootScope.increaseKickoffPageViewCount = function () {
      mlFirebaseService.increaseKickoffPageViewCount();
    };


    /*this runs once when the app bootstraps*/
    /*if the user is authorized*/

    if (angular.isDefined($localStorage.authorization)) {
      /*put his auth code in the headers*/
      $timeout(function () {
        $.ajaxSetup({
          headers : {
            "ACCESS-TOKEN" :  $localStorage.authorization
          }
        });
        $rootScope.loadInitialPosts();

        /*Setup Notification checker on Firebase*/
        mlFirebaseService.retrieveUserNotifications();


        $state.go('home.posts');

      });

    }else{
      $state.go('login');
    }



    $rootScope.openMenu = function () {
      $ionicSideMenuDelegate.toggleLeft();
    };

    $rootScope.showHomeTab = function() {
      $ionicTabsDelegate.select(0);
    };



    $rootScope.logout = function () {
      window.localStorage.clear();
      $localStorage.$reset({
        notifications : []
      });
      $.ajaxSetup({
        headers : { "ACCESS-TOKEN" :  "" }
      });
      $state.go('login');
    };

  });


/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlSSOAuthService', function(mlConstants, $http, $q, $localStorage, $timeout,  $rootScope, mlWorkDayService, mlFirebaseService) {

    var that = this;


  });

/**
 * Created by Kaygee on 21/03/2016.
 */

/*

var users = [
  {"ID":"306","user_login":"aaron.arakawa@meltwater.com","user_nicename":"aaron-arakawameltwater-com","user_email":"aaron.arakawa@meltwater.com","display_name":"Aaron Arakawa"},
  {"ID":"376","user_login":"aaron.galliner@meltwater.com","user_nicename":"aaron-gallinermeltwater-com","user_email":"aaron.galliner@meltwater.com","display_name":"Aaron Galliner"},
  {"ID":"1054","user_login":"aaron.low@meltwater.com","user_nicename":"aaron-lowmeltwater-com","user_email":"aaron.low@meltwater.com","display_name":"aaron.low@meltwater.com"},
  {"ID":"535","user_login":"aaron.soh@meltwater.com","user_nicename":"aaron-sohmeltwater-com","user_email":"aaron.soh@meltwater.com","display_name":"aaron.soh@meltwater.com"},
  {"ID":"21","user_login":"abhinav","user_nicename":"abhinav","user_email":"abhinav.bhardwaj@a3logics.in","display_name":"Abhinav","push_id":"jhjhdas"},
  {"ID":"1161","user_login":"adam.brender@meltwater.com","user_nicename":"adam-brendermeltwater-com","user_email":"adam.brender@meltwater.com","display_name":"adam.brender@meltwater.com"},
  {"ID":"1099","user_login":"adam.dealy@meltwater.com","user_nicename":"adam-dealymeltwater-com","user_email":"adam.dealy@meltwater.com","display_name":"adam.dealy@meltwater.com"},
  {"ID":"647","user_login":"adam.kragerud@meltwater.com","user_nicename":"adam-kragerudmeltwater-com","user_email":"adam.kragerud@meltwater.com","display_name":"adam.kragerud@meltwater.com"},
  {"ID":"774","user_login":"adam.naasz@meltwater.com","user_nicename":"adam-naaszmeltwater-com","user_email":"adam.naasz@meltwater.com","display_name":"adam.naasz@meltwater.com"}];
*/

angular.module('mLife')
  .service('mlAuthService', function(mlConstants, $http, $q, $localStorage, $timeout,  $rootScope,
                                     mlWorkDayService, mlFirebaseService, $state, $ionicLoading, CacheFactory) {

    var that = this;

    if (!CacheFactory.get('userCache')) {
      CacheFactory.createCache('userCache', {
        deleteOnExpire: 'aggressive',
        maxAge: 30 * 60 * 1000, // 30minutes
        recycleFreq: 5 * 60 * 1000,
        onExpire: function (key, value) {
          that.getUsersList()
        }
      });
    }

    var userCache = CacheFactory.get('userCache');



    /*this checks if the user is valid*/
    this.authenticateUser = function (loginData) {
      var defer = $q.defer();

      $http({
        method: 'POST',
        url: mlConstants.authenticateUser,
        //data: $.param(dataToPost),
        data: loginData
        // headers: {
        //   //'Content-Type': 'application/x-www-form-urlencoded',
        //   'Content-Type': 'application/json; charset=utf-8',
        //   'Access-Control-Allow-Origin': '*'
        // }
      })
        .success(function (successData) {
          if (successData.code == 200) {
            defer.resolve(true);
          }
        })
        .error(function (errorData) {
          defer.reject(false);
        });
      return defer.promise
    };

    /*this logs the user in*/
    function postAuthedUser(successData, defer, paEmail) {

      $localStorage.user = {
        data : {},
        roles : {}
      };

      if (successData.code == 200) {

        $localStorage.authorization = successData['ACCESS-TOKEN'];

        $.ajaxSetup({
          headers: {"ACCESS-TOKEN": $localStorage.authorization}
        });

        $localStorage.user = successData.result;

        mlWorkDayService.getEmployeeInfo($localStorage.user.data.user_email)
          .then(function (userData) {

            if (!userData.userImageLink) {
              userData.userImageLink = "http://a2.mzstatic.com/us/r30/Purple69/v4/dc/55/d1/dc55d1e8-57a0-3b50-25cb-68de23ad2c74/icon350x350.png"
            }

            angular.merge($localStorage.user.data, userData);

            delete $localStorage.user.data.active;
            delete $localStorage.user.allcaps;
            delete $localStorage.user.cap_key;
            delete $localStorage.user.caps;
            delete $localStorage.user.data.errorCode;
            delete $localStorage.user.data.errorMessage;
            delete $localStorage.user.filter;
            delete $localStorage.user.data.linkedin;
            delete $localStorage.user.data.phone;
            delete $localStorage.user.roles;
            delete $localStorage.user.data.twitter;
            delete $localStorage.user.data.skype;
            delete $localStorage.user.data.user_activation_key;
            delete $localStorage.user.data.user_pass;
            delete $localStorage.user.data.user_status;
            delete $localStorage.user.data.user_url;

            mlFirebaseService.updateUserLogin();

            $state.go('home.posts');

            /*Setup Notification checker on Firebase*/
            mlFirebaseService.retrieveUserNotifications();

            $ionicLoading.hide();


          }, function () {
            console.log('userData error');
          });

        defer.resolve(true);
      }
      else if(paEmail && paEmail.search('pollafrique.com') > 0){
        $localStorage.user.data.area = "Global";
        $localStorage.user.data.division = "Office of the Developers";
        $localStorage.user.data.email = $localStorage.user.data.user_email;
        $localStorage.user.data.manager = "Samuel Dzidzornu";
        $localStorage.user.data.salesOffice = "Software Development";
        $localStorage.user.data.salesPerson = "false";
        $localStorage.user.data.title = "App Developer";


        $state.go('home.posts');

        /*Setup Notification checker on Firebase*/
        mlFirebaseService.retrieveUserNotifications();

        $ionicLoading.hide();

        defer.reject(successData);
      }else {
        defer.reject(successData);
      }
    }

    this.loginUser = function (userEmail) {
      var defer = $q.defer();

      var loginData = {
        action : "login",
        username : userEmail
      };

      $.ajax({
        url : mlConstants.loginUser,
        data : loginData,
        method : "POST",
        dataType : 'json',
        success : function (successData) {
          postAuthedUser(successData, defer, userEmail);
        },
        error : function (errData, err2, err3) {
          defer.resolve(true);
        }
      });

      return defer.promise
    };


    /*this functions adds the label and value properties to the user objects*/
    function prepUserList(){
      for (var i = 0; i < that.userList.length; i++) {
        var user = that.userList[i];
        user.label = user.display_name;
        that.userList[i] = user;
      }
      $localStorage.userList = that.userList;
      $rootScope.$broadcast('usersLoaded');
    }

    /*this gets all users in the system*/
    this.getUsersList = function () {
      var defer = $q.defer(),

        cachedData = userCache.get(mlConstants.getUsersList);

      // that.userList = users;
      // prepUserList();
      // return;

      if (!cachedData) {
        $.ajax({
          url: mlConstants.getUsersList,
          data: {'action': 'getUsersList'},
          method: "POST",
          dataType: 'json',
          success: function (successData, status, jqxhr) {
            if (successData.code == 200) {
              that.userList = successData.result;
              userCache.put(mlConstants.getUsersList, successData.result);
              prepUserList();
              defer.resolve(true);
            }
          },
          error: function (errData, err2, err3) {
            defer.reject(false);
          }
        });
      }else{
        that.userList = cachedData;
        prepUserList();
        defer.resolve(true);
      }


      return defer.promise
    };


    /*this stores/updates a users push ID in the system*/
    this.storeUserPushId = function (pushID) {
      var defer = $q.defer();

      $.ajax({
        url : mlConstants.storeUserPushId,
        data : {'action' : 'storePushId', 'push_id' : pushID},
        method : "POST",
        dataType : 'json',
        headers : {'ACCESS-TOKEN' : $localStorage.authorization },
        success : function (successData, status, jqxhr) {
          if (successData.code == 200) {
            defer.resolve(successData);
          }
        },
        error : function (errData, err2, err3) {
          defer.reject(errData);
        }
      });


      return defer.promise
    };

    this.authWithSSO = function () {
      var appSecret = "1YduikPmGc7adywt";

      var tokenKey = "hrmbdjt8XrkwhMOI";

      var customerId = "41865";

      // var publicKey = "<CONTENT_OF_DOWNLOADED_CERTIFICATE";
      var publicKey = KEYUTIL.getKey(mlConstants.miniOrangeCertificate);

      var redirectUrl = "https://auth.miniorange.com/moas/broker/login/jwt/"

        + customerId;

      var responseUrl = "https://auth.miniorange.com/moas/jwt/mobile";


      var currentTimestamp = Date.now();

      var inputString = currentTimestamp + ":" + appSecret;

      var keyHex = CryptoJS.enc.Utf8.parse(tokenKey);

      var cipherText =

        CryptoJS.enc.Base64.stringify(CryptoJS.AES.encrypt(inputString,

          keyHex, {mode: CryptoJS.mode.ECB}).ciphertext);

      redirectUrl += "?token=" + encodeURIComponent(cipherText);

      // Note: The cipherText needs to URL Encoded.

      var ref = cordova.InAppBrowser.open(redirectUrl, '_blank',
        // var ref = window.open(redirectUrl, '_blank',
        'location=yes');


      ref.addEventListener('loadstop', function(event){

        //Read URL of In App Browser

        var eventUrl = event.url;

        if(eventUrl.indexOf(responseUrl) !== -1){
          var idToken = eventUrl.substring(eventUrl.indexOf("id_token") + 9, event.url.length);

          //  all user's details are in idToken
          var payloadObj = KJUR.jws.JWS.readSafeJSONString(b64utoutf8(idToken.split(".")[1]));

          var subject = payloadObj.sub;

          if (validateSignature(idToken, publicKey) && validateJWTToken(idToken, publicKey)) {

            payloadObj = KJUR.jws.JWS.readSafeJSONString(b64utoutf8(idToken.split(".")[1]));

            subject = payloadObj.sub;

            ref.close();

            // postAuthedUser(idToken, subject);
            that.loginUser(subject);

            $timeout(function(){
              $state.go('home.posts');
            });
            // Note: JWT Subject contains the values received in the NameID of the SAML Response from ADFS.
          }
        }


      });
    };


    function validateSignature(token, publicKey){

      return KJUR.jws.JWS.verify(token, publicKey,

        ['RS256']);

    }

    function validateJWTToken(token, publicKey){

      var acceptField = {

        alg: ['RS256', 'RS512', 'PS256', 'PS512'],

        iss: ['MO'],

        aud: ['https://auth.miniorange.com/moas/jwt/mobile']

      };

      return KJUR.jws.JWS.verifyJWT(token, publicKey, acceptField);

    }

  });

/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlFirebaseService', function(mlConstants, $localStorage, $q,
                                         $rootScope, $ionicLoading, $timeout, mlKickOffService) {

    var that = this;


    var fireBaseRef = firebase.database().ref();

    this.increasePostPageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the post page has been viewed*/
      fireBaseRef.child('post/page_views/' + dateKey).transaction(function(currentPostObj){

        if (currentPostObj && currentPostObj.views) {

          currentPostObj.views ++;

          currentPostObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentPostObj = {};

          currentPostObj.views = 1;

          currentPostObj.users = {};

          currentPostObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentPostObj;
      });

    };


    /*params :
     * postObj is the post/article object
     * increasePageReads is to update the post when a comment or a like occurs,
     * without increasing read count*/
    this.increaseAPostsViewCount = function (postObj, increasePageReads) {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();



      /* This function increases the number of times a post has been viewed/read by day/date */
      fireBaseRef.child('post/posts_by_date/'+dateKey+'/'+postObj.ID).transaction(function(currentPostArticleObject){
        if (currentPostArticleObject) {

          currentPostArticleObject.comments = postObj.comments_count;

          currentPostArticleObject.likes = postObj.user_likes;

          if (increasePageReads) {
            if (currentPostArticleObject.reads) {
              currentPostArticleObject.reads ++;
            }else{
              currentPostArticleObject.reads = 1;
            }
          }


          currentPostArticleObject.title = postObj.post_title;
          currentPostArticleObject.post_image = postObj.image;

          currentPostArticleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentPostArticleObject = {};

          currentPostArticleObject.comments = postObj.comments_count;

          currentPostArticleObject.likes = postObj.user_likes;

          if (increasePageReads) {
            currentPostArticleObject.reads = 1;
          }

          currentPostArticleObject.title = postObj.post_title;
          currentPostArticleObject.post_image = postObj.image;


          currentPostArticleObject.users = {};
          currentPostArticleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentPostArticleObject;

      });


      /* This function increases the number of times a post has been viewed/read with post content */
      fireBaseRef.child('post/posts_viewed/'+postObj.ID).transaction(function(currentPostArticleObject){
        if (currentPostArticleObject) {

          currentPostArticleObject.comments = postObj.comments_count;

          currentPostArticleObject.likes = postObj.user_likes;

          currentPostArticleObject.content = postObj.post_content;

          if (increasePageReads) {
            if (currentPostArticleObject.reads) {
              currentPostArticleObject.reads ++;
            }else{
              currentPostArticleObject.reads = 1;
            }
          }


          currentPostArticleObject.title = postObj.post_title;
          currentPostArticleObject.post_image = postObj.image;


          currentPostArticleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentPostArticleObject = {};


          currentPostArticleObject.content = postObj.post_content;

          currentPostArticleObject.comments = postObj.comments_count;

          currentPostArticleObject.likes = postObj.user_likes;

          if (increasePageReads) {
            currentPostArticleObject.reads = 1;
          }

          currentPostArticleObject.title = postObj.post_title;
          currentPostArticleObject.post_image = postObj.image;


          currentPostArticleObject.users = {};
          currentPostArticleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentPostArticleObject;

      });
    };



    this.increaseTodayPageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the today page has been viewed*/
      fireBaseRef.child('today/page_views/' + dateKey).transaction(function(currentTodayObj){

        if (currentTodayObj && currentTodayObj.views) {

          currentTodayObj.views ++;

          currentTodayObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentTodayObj = {};

          currentTodayObj.views = 1;

          currentTodayObj.users = {};

          currentTodayObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentTodayObj;
      });

      /*fireBaseRef.child('posts/'+postObj.id).transaction(function(currentValue){
       return (currentValue || 0) + 1;
       });*/

    };


    /*params :
     * saleObject is the post/article object
     */
    this.increaseASalesViewCount = function (saleObject) {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();



      /* This function increases the number of times a post has been viewed/read by day/date */
      fireBaseRef.child('today/sale_by_date/'+dateKey+'/'+saleObject.todayFeedItemId).transaction(function(currentSaleObject){
        if (currentSaleObject) {

          /*
           * "time": "08:15",
           "accountName": "The University of Tennessee Medical Center",
           "comments": null,
           "likes": null,
           "products": [
           "Fairhair"
           ],
           "salesValue": "1,500",
           "salesTerm": "12",
           "commentCount": "0",
           "likeCount": "0",
           "salesCollaborators": [
           {
           "userName": "Stephanie Alvarez",
           "userId": "11059",
           "division": "CS",
           "salesOffice": "CS Miami",
           "userEmail": "stephanie.alvarez@meltwater.com",
           "firstDeal": "true",
           "userImageLink": "http://admin.meltwater.com/superadmin/public/image.html?file=user_11059_today_1447210077481.jpg",
           "userProfileLink": "http://admin.meltwater.com/superadmin/userprofile/detail.html?id=11059"
           }
           ],
           "todayFeedItemId": "844815",
           "orderLink": "http://admin.meltwater.com/superadmin/orderfeed/detail.html?id=683709",
           "accountLink": "http://admin.meltwater.com/superadmin/accountfeed/detail.html?id=76233"
           */

          currentSaleObject.accountName = saleObject.accountName;
          currentSaleObject.products = saleObject.products;
          currentSaleObject.salesCollaborators = saleObject.salesCollaborators;

          currentSaleObject.salesValue = saleObject.salesValue;
          currentSaleObject.salesTerm = saleObject.salesTerm;
          currentSaleObject.comments = saleObject.commentCount;
          currentSaleObject.likes = saleObject.likeCount;


          if (currentSaleObject.views) {
            currentSaleObject.views ++;
          }else{
            currentSaleObject.views = 1;
          }

          currentSaleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentSaleObject = {};

          currentSaleObject.accountName = saleObject.accountName;
          currentSaleObject.products = saleObject.products;
          currentSaleObject.salesCollaborators = saleObject.salesCollaborators;

          currentSaleObject.salesValue = saleObject.salesValue;
          currentSaleObject.salesTerm = saleObject.salesTerm;
          currentSaleObject.comments = saleObject.commentCount;
          currentSaleObject.likes = saleObject.likeCount;

          currentSaleObject.views = 1;

          currentSaleObject.users = {};
          currentSaleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentSaleObject;

      });


      /* This function increases the number of times a post has been viewed/read with post content */
      fireBaseRef.child('today/sales_viewed/'+saleObject.todayFeedItemId).transaction(function(currentSaleObject){
        if (currentSaleObject) {

          currentSaleObject.accountName = saleObject.accountName;
          currentSaleObject.products = saleObject.products;
          currentSaleObject.salesCollaborators = saleObject.salesCollaborators;

          currentSaleObject.salesValue = saleObject.salesValue;
          currentSaleObject.salesTerm = saleObject.salesTerm;
          currentSaleObject.comments = saleObject.commentCount;
          currentSaleObject.likes = saleObject.likeCount;


          if (currentSaleObject.views) {
            currentSaleObject.views ++;
          }else{
            currentSaleObject.views = 1;
          }

          currentSaleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentSaleObject = {};



          currentSaleObject.accountName = saleObject.accountName;
          currentSaleObject.products = saleObject.products;
          currentSaleObject.salesCollaborators = saleObject.salesCollaborators;

          currentSaleObject.salesValue = saleObject.salesValue;
          currentSaleObject.salesTerm = saleObject.salesTerm;
          currentSaleObject.comments = saleObject.commentCount;
          currentSaleObject.likes = saleObject.likeCount;


          if (currentSaleObject.views) {
            currentSaleObject.views ++;
          }else{
            currentSaleObject.views = 1;
          }

          currentSaleObject.users = {};
          currentSaleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentSaleObject;

      });
    };



    this.increaseOfficesPageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the offices page has been viewed*/
      fireBaseRef.child('offices/page_views/' + dateKey).transaction(function(currentOfficeObj){

        if (currentOfficeObj && currentOfficeObj.views) {

          currentOfficeObj.views ++;

          currentOfficeObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentOfficeObj = {};

          currentOfficeObj.views = 1;

          currentOfficeObj.users = {};

          currentOfficeObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentOfficeObj;
      });

    };

    /*params :
     * territoryObject is the territory object
     */
    this.increaseATerritorysViewCount = function (territoryObject, territoryTypeKey) {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();

      /* This function increases the number of times a post has been viewed/read by day/date */
      fireBaseRef.child(territoryTypeKey + '/' + territoryTypeKey + '_by_date/'+dateKey+'/'+territoryObject.name).transaction(function(currentTerritoryObject){
        if (currentTerritoryObject) {

          /* {
           "name": "Americas",
           "active": true,
           "type": "Super Area"
           "type": "Area"
           "type": "Sales Office"

           }*/

          currentTerritoryObject.name = territoryObject.name;
          currentTerritoryObject.type = territoryObject.type;


          if (currentTerritoryObject.views) {
            currentTerritoryObject.views ++;
          }else{
            currentTerritoryObject.views = 1;
          }

          currentTerritoryObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentTerritoryObject = {};


          currentTerritoryObject.name = territoryObject.name;
          currentTerritoryObject.type = territoryObject.type;

          currentTerritoryObject.views = 1;

          currentTerritoryObject.users = {};
          currentTerritoryObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentTerritoryObject;

      });


      /* This function increases the number of times a post has been viewed/read with post content */
      fireBaseRef.child(territoryTypeKey + '/' + territoryTypeKey + '_viewed/'+territoryObject.name).transaction(function(currentTerritoryObject){
        if (currentTerritoryObject) {

          currentTerritoryObject.name = territoryObject.name;
          currentTerritoryObject.type = territoryObject.type;


          if (currentTerritoryObject.views) {
            currentTerritoryObject.views ++;
          }else{
            currentTerritoryObject.views = 1;
          }

          currentTerritoryObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentTerritoryObject = {};


          currentTerritoryObject.name = territoryObject.name;
          currentTerritoryObject.type = territoryObject.type;

          currentTerritoryObject.views = 1;

          currentTerritoryObject.users = {};
          currentTerritoryObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentTerritoryObject;

      });
    };



    this.increaseProfilePageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the profile page has been viewed*/
      fireBaseRef.child('profile/page_views/' + dateKey).transaction(function(currentProfileObj){

        if (currentProfileObj && currentProfileObj.views) {

          currentProfileObj.views ++;

          currentProfileObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentProfileObj = {};

          currentProfileObj.views = 1;

          currentProfileObj.users = {};

          currentProfileObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentProfileObj;
      });

    };

    this.increaseKickoffPageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the profile page has been viewed*/
      /* fireBaseRef.child('profile/page_views/' + dateKey).transaction(function(currentProfileObj){

       if (currentProfileObj && currentProfileObj.views) {

       currentProfileObj.views ++;

       currentProfileObj.users[ $localStorage.user.ID ] = $localStorage.user.data

       }else{
       currentProfileObj = {};

       currentProfileObj.views = 1;

       currentProfileObj.users = {};

       currentProfileObj.users[ $localStorage.user.ID ] = $localStorage.user.data

       }

       return currentProfileObj;
       });*/

    };


    this.increaseAnEmployeesViewCount = function (employeeObj) {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      var empEmailKey = employeeObj.email.replace(/\./g, "_");
      empEmailKey = empEmailKey.split('@')[0];

      delete employeeObj.errorCode;
      delete employeeObj.errorMessage;
      delete employeeObj.linkedin;
      delete employeeObj.phone;
      delete employeeObj.skype;
      delete employeeObj.twitter;

      /* This function increases the number of times a post has been viewed/read by day/date */
      fireBaseRef.child('profile/profile_by_date/'+dateKey+'/'+empEmailKey).transaction(function(currentEmpObject){
        if (currentEmpObject) {

          /*
           {
           active: "false",
           area: "Japan & SEA",
           email: "aaron.soh@meltwater.com",
           errorCode: "200",
           errorMessage: "",
           firstName: "Aaron",
           lastName: "Soh",
           linkedin: null,
           manager: "Weldon Fung",
           phone: null,
           salesOffice: "CA Singapore",
           skype: "aaron.soh-meltwater",
           superArea: "APAC",
           title: "Sales Manager-SGP",
           twitter: null
           }
           */

          currentEmpObject = angular.copy(employeeObj);

          if (currentEmpObject.views) {
            currentEmpObject.views ++;
          }else{
            currentEmpObject.views = 1;
          }

          if (currentEmpObject.users) {
            currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data
          }else{
            currentEmpObject.users = {};
            currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data
          }

        }else{
          currentEmpObject = angular.copy(employeeObj);

          currentEmpObject.views = 1;

          currentEmpObject.users = {};
          currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentEmpObject;

      });


      /* This function increases the number of times a post has been viewed/read with post content */
      fireBaseRef.child('profile/users_viewed/'+empEmailKey).transaction(function(currentEmpObject){
        if (currentEmpObject) {

          currentEmpObject = angular.copy(employeeObj);

          if (currentEmpObject.views) {
            currentEmpObject.views ++;
          }else{
            currentEmpObject.views = 1;
          }

          if (currentEmpObject.users) {
            currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data
          }else{
            currentEmpObject.users = {};
            currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data
          }



        }else{
          currentEmpObject = angular.copy(employeeObj);

          currentEmpObject.views = 1;

          if (currentEmpObject.views) {
            currentEmpObject.views ++;
          }else{
            currentEmpObject.views = 1;
          }

          currentEmpObject.users = {};
          currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentEmpObject;

      });
    };



    this.updateUserLogin = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that updates the user and his login times*/
      fireBaseRef.child('users/user_data/' + $localStorage.user.ID).transaction(function(currentProfileObj){

        if (currentProfileObj) {

          angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.last_login = date;

        }else{
          currentProfileObj = {};

          angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.last_login = date;

        }

        return currentProfileObj;
      });

      /*This is the function that updates logins by date*/
      fireBaseRef.child('users/logins_by_date/' + dateKey).transaction(function(usersObject){

        if (usersObject) {

          if (usersObject.logins) {
            usersObject.logins ++;
          }else{
            usersObject.logins = 1;
          }
          usersObject.users[$localStorage.user.ID] = $localStorage.user.data;


        }else{
          usersObject = {
            logins : 1,
            users : {}
          };

          usersObject.users[$localStorage.user.ID] = $localStorage.user.data;
        }

        return usersObject;
      });

    };

    this.updateUserLastLike = function (postArticleObj) {

      var date = new Date();

      // var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that updates the user and his login times*/
      fireBaseRef.child('users/' + $localStorage.user.ID).transaction(function(currentProfileObj){

        if (currentProfileObj) {

          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.most_recent_activity = date;
          currentProfileObj.recent_post_like = {
            title : postArticleObj.post_title,
            postId : postArticleObj.ID,
            like_date : date,
            post_image : postArticleObj.image
          };

        }else{
          currentProfileObj = {};

          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);
          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.most_recent_activity = date;

          currentProfileObj.recent_post_like = {
            title : postArticleObj.post_title,
            postId : postArticleObj.ID,
            like_date : date,
            post_image : postArticleObj.image
          };
        }

        return currentProfileObj;
      });

    };

    this.updateUserLastComment = function (postArticleObj, userComment) {

      var date = new Date();

      // var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that updates the user and his login times*/
      fireBaseRef.child('users/' + $localStorage.user.ID).transaction(function(currentProfileObj){

        if (currentProfileObj) {

          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.most_recent_activity = date;
          currentProfileObj.recent_post_comment = {
            title : postArticleObj.post_title,
            postId : postArticleObj.ID,
            comment_date : date,
            post_image : postArticleObj.image,
            commentPosted : userComment
          };

        }else{
          currentProfileObj = {};

          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.most_recent_activity = date;
          currentProfileObj.recent_post_comment = {
            title : postArticleObj.post_title,
            postId : postArticleObj.ID,
            comment_date : date,
            post_image : postArticleObj.image,
            commentPosted : userComment
          };

        }

        return currentProfileObj;
      });

    };


    this.saveUserTaggingAsNotification = function (employeeToBeTaggedObj, tagObject) {

      var date = new Date();

      tagObject.dateCreated = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();

      var empEmailKey = employeeToBeTaggedObj.replace(/\./g, "_");
      empEmailKey = empEmailKey.split('@')[0];

      /* This function saves a notification for an employee being tagged*/
      fireBaseRef.child('notifications/'+empEmailKey).push()
        .set(tagObject)

    };


    this.retrieveUserNotifications = function () {

      var empEmailKey = ($localStorage.user.data.email || $localStorage.user.data.user_email).replace(/\./g, "_");
      empEmailKey = empEmailKey.split('@')[0];

      // empEmailKey = 'abhinav_bhardwaj';

      var notificationRef = fireBaseRef.child('notifications/' + empEmailKey).orderByKey();

      notificationRef.on('value', function(snapshot) {
        angular.forEach(snapshot.val(), function (notificationItem) {
          $localStorage.notifications.unshift(notificationItem);
        });


        /* This function removes all the notifications after it has been read*/
        fireBaseRef.child('notifications/'+empEmailKey).remove();
      });

    };




    this.doCommentOnKickOffPost = function (postId, comment) {

      var defer = $q.defer();

      /* This function saves a comment in the comment table and updates the count on the post table*/
      fireBaseRef.child('/kickoff_posts_comments/' + postId).push().set(comment)
        .then(function(){
          fireBaseRef.child('kickoff_timeline_posts/'+postId).transaction(function(post) {

            if (post){
              var uid = $localStorage.user.ID;

              if (!post.commenters) {
                post.commenters = {};
                post.commentCount = 0;
              }
              post.commentCount++;
              post.commenters[uid] = true;
            }

            defer.resolve(post);

            return post;

          });
        });

      return defer.promise
    };

    this.retrieveAPostsComments = function (postId) {

      var kickOffPostsRef = fireBaseRef.child('kickoff_posts_comments/' + postId).orderByKey();

      kickOffPostsRef.once('value', function(snapshot) {
        mlKickOffService.selected_posts_comments = [];

        angular.forEach(snapshot.val(), function (commentItem) {
          mlKickOffService.selected_posts_comments.unshift(commentItem);
        });

        mlKickOffService.filterComments();

      });

    };

  });

/**
 * Created by Kaygee on 22/03/2016.
 */



angular.module('mLife')
  .service('mlHttpInterceptor', function ($localStorage) {


    function getCurrentTimeTokenHeader(){
      var timeNow = new Date().getTime();
      var timeCreated = timeNow;

      /*get stored token*/
      if ($localStorage["header_token"]) {
        var storedToken = atob($localStorage["header_token"]);
        timeCreated = storedToken.split(':')[1];
      }

      /*check if the token is expired*/
      var minutes = Math.floor((timeNow - timeCreated) / 60000);

      /*token expires after 70 minutes*/
      if (minutes > 60 || minutes == 0) {
        var mwTokenBase = "M3ltW@ter:"+ timeNow;

        /*regenerate token and save in localstorage*/
        $localStorage["header_token"] = btoa(mwTokenBase);
      }
      return  $localStorage["header_token"];
    }


    return{
      'request' : function (config) {
        if (config.url.startsWith("https://admin.meltwater.com")) {
          var token = getCurrentTimeTokenHeader();
          config.headers['token'] = token;
          if (config.params) {
          config.params.token = token;
          }else{
            config.params = {
              token : token
            };
          }
        }
        config.headers['Content-Type'] = "application/json";
        return config;
      },

      'response' : function (config) {

        if (config.data && config.data.code == 400) {
          // console.log("intercepted");
        }

        return config;
      }
    }
  });

/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlKickOffService', function(mlConstants, $firebaseArray, $q, $rootScope, $localStorage, $http, $timeout) {

    this.venueSet = '';

    var that = this;

    this.lookupComment = {};

    this.fireKickOffPostRef = firebase.database().ref('kickoff_timeline_posts');

    this.selected_posts_comments = [];

    this.lastLoadedTracker = '';

    this.lookupPost = {};

    var kickOffPostsRef = that.fireKickOffPostRef.orderByChild('descSort').limitToFirst(7);

    this.initKickOff = function () {
      this.kickOffTimelineArray = [];

      $firebaseArray(kickOffPostsRef).$loaded()
        .then(function(x) {
          for (var i = 0; i < x.length; i++) {
            var obj = x[i];
            that.lookupPost[obj.$id] = obj;
            that.kickOffTimelineArray.push(obj)
          }

          $rootScope.$broadcast('kickOffPostsLoaded');

        });

    };



    this.loadMoreKickOffPosts = function (lastKnownValue) {
      var defer = $q.defer();

      if (that.lastLoadedTracker === lastKnownValue) {
        defer.reject(false);
        return;
      }

      that.lastLoadedTracker = lastKnownValue;

      var kickOffPostsRef = that.fireKickOffPostRef.orderByChild('descSort').startAt(lastKnownValue).limitToFirst(7);

      var newArray = [];
      $firebaseArray(kickOffPostsRef).$loaded()
        .then(function(x) {
          for (var i = 0; i < x.length; i++) {
            var obj = x[i];
            that.lookupPost[obj.$id] = obj;
            newArray.push(obj)
          }
          defer.resolve(newArray);
        })
        .catch(function(error) {
          defer.reject(false)
        });

      return defer.promise;
    };

    this.createAKickOffPost = function (dataToPost) {

      var defer = $q.defer();
      dataToPost.dateCreated = firebase.database.ServerValue.TIMESTAMP;
      dataToPost.descSort =  -1 * (new Date().getTime());
      dataToPost.owner = {
        email:  $localStorage.user.data.email,
        user_email:  $localStorage.user.data.user_email,
        firstName: $localStorage.user.data.firstName,
        lastName: $localStorage.user.data.lastName
      };
      dataToPost.likeCount = 0;
      dataToPost.likers = {};
      dataToPost.commentCount = 0;
      dataToPost.commenters = {};
      dataToPost.shareCount = 0;
      dataToPost.sharers = {};

      if ($localStorage.user.data.userImageLink) {
        dataToPost.owner.userImageLink = $localStorage.user.data.userImageLink;
      }else{
        // dataToPost.owner.userImageLink = "http://admin.meltwater.com/superadmin/public/image.html?file=user_904_today_1453834543133.jpg";
      }

      $firebaseArray(that.fireKickOffPostRef).$add(dataToPost).then(function(ref) {
        dataToPost.$id = ref.key;

        dataToPost.dateCreated = new Date().getTime(); /*temporal by pass*/
        that.lookupPost[ref.key] = dataToPost;
        that.kickOffTimelineArray.unshift(dataToPost);

        $rootScope.$broadcast('kickOffPostsLoaded');

        defer.resolve(dataToPost);
        // var empEmailKey = $localStorage.user.data.user_email.replace(/\./g, "_");
        // empEmailKey = empEmailKey.split('@')[0];

        // that.kickOffTimelineArray.$indexFor(newPostKey); // returns location in the array
      });

// rs
//       wc
//       sc
//       West Hills Mall



      // // Write the new post's data simultaneously in the posts list and the user's post list.
      // var updates = {};
      // updates['/kickoff_timeline_posts/' + newPostKey] = dataToPost;
      // updates['/kickoff_user_posts/' + empEmailKey + '/' + newPostKey] = dataToPost;
      //
      // /* This function saves a the post by an employee being tagged*/
      // return fireBaseRef.update(updates);
      return defer.promise;

    };

    this.updateKickoffPost = function (dataToPost) {
      $firebaseArray(that.fireKickOffPostRef).$add(dataToPost).then(function(ref) {
        that.lookupPost[ref.key] = dataToPost;
        $rootScope.$broadcast('kickOffPostsLoaded');
      });
    };

    this.doLikeOnKickOffPost = function (post) {
      if (post) {
        var uid = $localStorage.user.ID;

        if (!post.likers) {
          post.likers = {};
          post.likeCount = 0;
        }
        if (post.likers[uid]) {
          post.likeCount--;
          post.likers[uid] = null;
        } else {
          post.likeCount++;
          post.likers[uid] = {
            email:  $localStorage.user.data.email || $localStorage.user.data.user_email,
            name: $localStorage.user.data.firstName + ' ' + $localStorage.user.data.lastName,
            id : uid,
            liked : true
          };
        }
        $firebaseArray(that.fireKickOffPostRef).$save(post);
      }
      return post;
    };


    this.shareAPost = function (dataToPost) {

      var defer = $q.defer();

      delete dataToPost.$$hashKey;

      dataToPost.dateCreated = firebase.database.ServerValue.TIMESTAMP;
      dataToPost.descSort =  -1 * (new Date().getTime());

      dataToPost.originalDateCreated = dataToPost.dateCreated;
      dataToPost.sharer = {
        email:  $localStorage.user.data.email,
        user_email:  $localStorage.user.data.user_email,
        firstName: $localStorage.user.data.firstName,
        lastName: $localStorage.user.data.lastName
      };
      dataToPost.likeCount = 0;
      dataToPost.likers = {};
      dataToPost.commentCount = 0;
      dataToPost.commenters = {};
      dataToPost.shareCount = 0;
      dataToPost.sharers = {};

      if ($localStorage.user.data.userImageLink) {
        dataToPost.sharer.userImageLink = $localStorage.user.data.userImageLink;
      }else{
        // dataToPost.sharer.userImageLink = "http://admin.meltwater.com/superadmin/public/image.html?file=user_904_today_1453834543133.jpg";
      }

      dataToPost = that.doShareOnKickOffPost(dataToPost);


      // Write the new post's data simultaneously in the posts list and the user's post list.
      // var updates = {};
      // updates['kickoff_timeline_posts/' + newPostKey] = dataToPost;
      // updates['kickoff_user_posts/' + empEmailKey + '/' + newPostKey] = dataToPost;

      if (!dataToPost.parent_id) {
        dataToPost.parent_id = dataToPost.$id;
      }

      $firebaseArray(that.fireKickOffPostRef).$add(dataToPost).then(function(ref) {
        dataToPost.$id = ref.key;
        dataToPost.dateCreated = new Date().getTime(); /*temporal by pass*/
        that.lookupPost[ref.key] = dataToPost;
        that.kickOffTimelineArray.unshift(dataToPost);

        $rootScope.$broadcast('kickOffPostsLoaded');

        defer.resolve(true);
        // var empEmailKey = $localStorage.user.data.user_email.replace(/\./g, "_");
        // empEmailKey = empEmailKey.split('@')[0];

        // that.kickOffTimelineArray.$indexFor(newPostKey); // returns location in the array
      });

      return defer.promise;
    };


    this.doShareOnKickOffPost = function (post) {
      if (post) {
        var uid = $localStorage.user.ID;

        if (!post.sharers) {
          post.sharers = {};
          post.shareCount = 0;
        }

        post.shareCount++;
        post.sharers[uid] = true;

      }

      return post;
    };


    this.filterComments = function () {

      if (this.selected_posts_comments.length) {
        this.lookupComment = {};

        for (var i = 0; i < this.selected_posts_comments.length; i++) {
          var commentItem = this.selected_posts_comments[i];

          this.lookupComment[commentItem.id] = commentItem;
        }
      }

      $rootScope.$broadcast('kickOffCommentLoaded');

    };

  });

/**
 * Created by Kaygee on 21/03/2016.
 */



angular.module('mLife')
  .service('mlNotifierService', function($ionicPopup, $q, $http, $window,  mlConstants, $localStorage, $cordovaToast, mlFirebaseService) {

    var that = this;

    this.alert = function (title, message) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: message
      });
    };

    this.showLoader = function () {
      $('.loader')
        .css('display','block')
        .css('position','fixed')
        .css('z-index',1000)
        .css('top', ($window.innerHeight / 2) - $('.loader').css( "height"))
        .css('left', ($window.innerWidth / 2) - $('.loader').width());
    };

    this.hideLoader = function () {
      $('.loader').css('display','none')
    };

    this.checkForUpdates = function () {
      return $http.get('https://mlife-1287.firebaseapp.com/version.json')
    };


    /*this stores/updates a users push ID in the system*/
    this.storeUserPushId = function (pushID) {
      var defer = $q.defer();

      $.ajax({
        url : mlConstants.storeUserPushId,
        data : {'action' : 'storePushId', 'push_id' : pushID},
        method : "POST",
        dataType : 'json',
        headers : {'ACCESS-TOKEN' : $localStorage.authorization },
        success : function (successData, status, jqxhr) {
          if (successData.code == 200) {
            defer.resolve(successData);
          }
        },
        error : function (errData, err2, err3) {
          defer.reject(errData);
        }
      });


      return defer.promise
    };



    this.prepTaggingArray =
      function (atPushIds, peopleInMentions, selected_post, comment, section_to_go_open_open, tag_type) {

        var postThatWasTagged = {
          comment_posted : comment,
          ID : selected_post.ID || selected_post.$id ||selected_post.id,
          section : section_to_go_open_open,
          type : tag_type,
          post_title : selected_post.post_title || 'mFeed',
          tagger  : {
            firstName : $localStorage.user.data.firstName,
            lastName : $localStorage.user.data.lastName,
            email : $localStorage.user.data.email,
            user_email : $localStorage.user.data.user_email,
            userImageLink : $localStorage.user.data.userImageLink
          }

        };

        for (var i = 0; i < peopleInMentions.length; i++) {
          var peepObj = peopleInMentions[i];
          mlFirebaseService.saveUserTaggingAsNotification(peepObj, postThatWasTagged);
        }

        var phraseToShow = postThatWasTagged.tagger.firstName + " " + postThatWasTagged.tagger.lastName;

        that.pushANotification(atPushIds, postThatWasTagged, comment, phraseToShow)
          .then(function (success) {
            },
            function (err) {

            })

      };

    /*this posts a push notification One Signal Push*/
    this.pushANotification = function (pushIdsArray, selected_post, comment, phraseToShow) {

      var defer = $q.defer();

      // Build the request object
      var req = {
        method: 'POST',
        url: 'https://onesignal.com/api/v1/notifications',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + mlConstants.pushTokenAPI
        },
        data: {
          "app_id": "29b7a8b4-7e51-4fef-86fe-c0f3d0c566f6",
          "ios_badgeType" : "Increase",
          "ios_badgeCount" : "1",
          "contents": {"en": comment},
          "content_available" : true,
          "headings": {"en": phraseToShow}
        }
      };

      if (pushIdsArray && pushIdsArray.length) {
        req.data.include_player_ids = pushIdsArray
      }else{
        req.data.included_segments = ['All']
      }


      // Make the API call
      $http(req).success(function(resp){
        // Handle success
        // console.log("One Signal: Push success", resp);
        defer.resolve(resp)
      }).error(function(error){
        // Handle error
        console.log("One Signal: Push error", error);
        defer.reject(error)

      });



      return defer.promise
    };



  });

/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlOfficeAreaService', function(mlConstants, $http, $q,
                                           $rootScope, $ionicLoading, $timeout) {

    var that = this;


    this.getSuperAreas = function () {
      var defer = $q.defer();

      var ter = {
        "errorMessage": null,
        "territories": [
          {
            "id": 1535,
            "name": "Americas",
            "active": true,
            "type": "Super Area"
          },
          {
            "id": 1543,
            "name": "APAC",
            "active": true,
            "type": "Super Area"
          },
          {
            "id": 1731,
            "name": "EMEA",
            "active": true,
            "type": "Super Area"
          }
        ],
        "errorCode": 200
      };

      function pushExtraSuperAreas() {
        that.superAreas.push({
            "id": "1541CO",
            "name": "CentralOps",
            "active": true,
            "type": "Super Area"
          },
          {
            "id": "1539ENG",
            "name": "Engineering",
            "active": true,
            "type": "Super Area"
          })
      }
      /*fetch employee info on WorkDay*/
      $http.post(mlConstants.allTerritories+'?type=superarea&includeDeactive=false', {cache : true})
        .success(function (successData, status, jqxhr) {

          if (successData.errorCode == "200") {
            that.superAreas = successData.territories;
            pushExtraSuperAreas();
            defer.resolve(true);
          }else{
            defer.reject(false);
          }
        })
        .error(function (errorData) {
          that.superAreas = ter.territories;
          pushExtraSuperAreas();

          defer.resolve(true);
        })
        .finally(function () {
        });

      return defer.promise
    };


    this.getAreasUnderSuperArea = function (superArea) {
      var defer = $q.defer();

      $ionicLoading.show({
        template: 'Getting areas in ' + superArea + '...',
        duration : 30000,
        noBackdrop : true
      });

      var ter = {
        "errorMessage": null,
        "errorCode": 200,
        "areas": [
          {
            "id": 1713,
            "name": "Admin APAC",
            "active": true,
            "type": "Area"
          },
          {
            "id": 1583,
            "name": "APAC Enterprise",
            "active": true,
            "type": "Area"
          },
          {
            "id": 1585,
            "name": "Australia",
            "active": true,
            "type": "Area"
          },
          {
            "id": 1587,
            "name": "China",
            "active": true,
            "type": "Area"
          },
          {
            "id": 1589,
            "name": "Japan & SEA",
            "active": true,
            "type": "Area"
          }
        ]
      };

      $http.post(mlConstants.allAreasBySuperArea+'?superareaName='+superArea+'&excludeParent=true&includeDeactive=false', {cache : true})
        .success(function (successData, status, jqxhr) {

          if (successData.errorCode == "200") {
            defer.resolve(successData.areas);
          }else{
            defer.reject(false);
          }
        })
        .error(function (errorData) {
          defer.resolve(ter.areas);
        })
        .finally(function () {
          $ionicLoading.hide();
        });

      return defer.promise
    };


    this.getOfficesUnderArea = function (area) {
      var defer = $q.defer();

      $ionicLoading.show({
        template: 'Getting offices in ' + area + '...',
        duration : 30000,
        noBackdrop : true
      });

      var ter = {
        "errorMessage": null,
        "offices": [
          {
            "id": 1611,
            "name": "APAC Australia Admin",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1211,
            "name": "CA Brisbane",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1251,
            "name": "CA Melbourne",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1477,
            "name": "CA Perth",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1281,
            "name": "CA Sydney",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1393,
            "name": "CS Melbourne",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1411,
            "name": "CS Sydney",
            "active": true,
            "type": "Sales Office"
          }
        ],
        "errorCode": 200
      };

      $http.post(mlConstants.allOfficesByArea+'?areaName='+area+'&excludeParent=true&includeDeactive=false', {cache : true})
        .success(function (successData, status, jqxhr) {

          if (successData.errorCode == "200") {
            defer.resolve(successData.offices);
          }else{
            defer.reject(false);
          }
        })
        .error(function (errorData) {
          defer.resolve(ter.offices);
        })
        .finally(function () {
          $ionicLoading.hide();
        });

      return defer.promise
    };



  });

/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlPostService', function(mlConstants, $http, $q, $rootScope, $localStorage, $timeout) {

    this.lookupPost = {};
    this.posts = [];

    this.favouritePosts = [];

    this.categoryPosts = [];
    this.lookupCategoryPost = {};
    this.lookupCategoryType = {};

    var that = this;

    function prepPosts(postObject) {

      if (postObject.post_data) {
        for (var i = 0; i < postObject.post_data.length; i++) {
          var postItem = postObject.post_data[i];

          /*cut off time from date*/
          if (postItem.post_date) {
            postItem.post_date = postItem.post_date.split(' ')[0];
          }

          /*If the image url isnt a real image, cut it off*/
          if (!(postItem.image && (postItem.image.substring(0, 4) == 'http'))) {
            postItem.image = "";

          }

          that.lookupPost[postItem.ID] = postItem;
        }
        $.merge(that.posts, postObject.post_data);

      }
      $rootScope.$broadcast('postsLoaded');
    }




    this.fetching = false;
    this.getPosts = function (start, quantity) {
      var defer = $q.defer();

      if (this.fetching) {
        defer.reject("false");
        return defer.promise
      }
      this.fetching = true;

      var that = this;


      if (!start) start = 0;

      if (!quantity) quantity = 5;

      $.ajax({
        url : mlConstants.getPosts,
        data :  {action : 'getPost' , number : quantity, start : start },
        method : "POST",
        dataType : 'json',
        success : function (successData, status, jqxhr) {
          if (successData.post_data && successData.post_data.length) {
            prepPosts(successData);
            defer.resolve(true);
          }
          that.fetching = false;
          defer.resolve(false);
        },
        error : function (errorData) {
          // prepPosts(tempPosts);
          that.fetching = false;
          defer.reject(errorData);
        }
      });


      return defer.promise
    };


    this.fetchingSlider = false;


    this.getNewsSlider = function () {
      var defer = $q.defer();

      if (this.fetchingSlider) {
        defer.reject("false");
        return defer.promise
      }
      this.fetchingSlider = true;

      var that = this;

      $.ajax({
        url : mlConstants.getTopSlider,
        data :  {action : 'getSliderImages' , slider_type : 'top', cat : "" },
        method : "POST",
        dataType : 'json',
        success : function (successData, status, jqxhr) {
          if (successData.code == 200 && successData.result.length) {
            defer.resolve(successData);
          }
          that.fetchingSlider = false;
        },
        error : function (errorData) {
          that.fetchingSlider = false;
          defer.reject(false);
        }
      });


      return defer.promise
    };


    function prepFavPosts(postObject) {
      if (postObject.post_data) {
        for (var i = 0; i < postObject.post_data.length; i++) {
          var postItem = postObject.post_data[i];

          /*cut off time from date*/
          if (postItem.post_date) {
            postItem.post_date = postItem.post_date.split(' ')[0];
          }

          if (!(postItem.image && (postItem.image.substring(0, 4) == 'http'))) {
            postItem.image = ""
          }
          that.lookupPost[postItem.ID] = postItem;

          that.favouritePosts.push(postItem);
        }
      }
      $rootScope.$broadcast('favPostsLoaded');
    }

    this.fetchingFavorites = false;

    this.getFavoritePosts = function (empEmail, start, quantity) {
      var defer = $q.defer();

      if (this.fetchingFavorites) {
        defer.reject("false");
        return defer.promise
      }

      this.fetchingFavorites = true;

      var that = this;


      if (!start) start = 0;

      if(start == 0) that.favouritePosts = [];

      if (!quantity) quantity = 5;

      $.ajax({
        url : mlConstants.getUserFavoritePosts,
        data :  {action : 'getUserProfilePost', user_email : empEmail, number : quantity, start : start },
        method : "POST",
        dataType : 'json',
        success : function (successData, status, jqxhr) {
          if (successData.post_data && successData.post_data.length) {
            prepFavPosts(successData);
            defer.resolve(true);
          }
          that.fetching = false;
          defer.reject(false);
        },
        error : function (errorData) {
          // prepFavPosts(tempPosts, 0);
          that.fetchingFavorites = false;
          defer.reject(errorData);
        }
      });


      return defer.promise
    };

    this.getSinglePostById = function (postID) {
      var defer = $q.defer();

      $.ajax({
        url : mlConstants.getUserFavoritePosts,
        data :  {action : 'getUserProfilePost', post_id : postID },
        method : "POST",
        dataType : 'json',
        success : function (successData, status, jqxhr) {
          if (successData.post_data && successData.post_data.length) {
            defer.resolve(successData.post_data[0]);
          }
          defer.reject(false);
        },
        error : function (errorData) {
          // defer.resolve(tempPosts.post_data[0]);

          // defer.reject(errorData);
        }
      });


      return defer.promise
    };


    this.getPostComments = function (postId) {
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.getPostComments,
        data: {action: "fetchComments", post_id: postId},
        dataType : 'json',
        cache : true,
        success: function (successData) {
          if (successData.code && successData.code == '200') {
            defer.resolve(successData.result)
          }else if(successData.code && successData.code == '401'){
            defer.resolve("no_comments");
          }else{
            defer.reject(successData);
          }
        },
        error: function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                defer.resolve(responseTextObj.result);
              }else if(responseTextObj.code && responseTextObj.code == '401'){
                defer.resolve("no_comments");
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };


    this.createPostComment = function (postId, comment) {
      comment = emojione.toShort(comment);

      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.createPostComment,
        dataType : 'json',
        data: {action : "addPostComment", post_id : postId, comment_data : comment},
        success : function (successData) {
          if (successData.code == 200){
            defer.resolve(true);
          }else{
            defer.reject(successData);
          }
        },
        error : function (errorData) {
          defer.reject(errorData);
        }
      });

      return defer.promise
    };

    this.editPostComment = function (commentId, comment) {
      comment = emojione.toShort(comment);
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.editPostComment,
        data: {action: "editComment", comment_id: commentId, comment_content : comment},
        dataType : 'json',
        cache : false,
        success: function (successData) {
          if (successData.code && successData.code == '200') {
            defer.resolve(successData.result)
          }else if(successData.code && successData.code == '401'){
            defer.resolve("something");
          }else{
            defer.reject(successData);
          }
        },
        error: function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                defer.resolve(responseTextObj.result);
              }else if(responseTextObj.code && responseTextObj.code == '401'){
                defer.resolve("no_comments");
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };

    this.deletePostComment = function (commentId) {
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.deletePostComment,
        data: {action: "deleteComment", comment_id: commentId, force_delete : true},
        dataType : 'json',
        cache : false,
        success: function (successData) {
          if (successData.code && successData.code == '200') {
            defer.resolve(successData.result)
          }else if(successData.code && successData.code == '401'){
            defer.resolve("something");
          }else{
            defer.reject(successData);
          }
        },
        error: function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                defer.resolve(responseTextObj.result);
              }else if(responseTextObj.code && responseTextObj.code == '401'){
                defer.resolve("no_comments");
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };

    this.getPostLikes = function (postId) {
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.getPostLikes,
        data: {action: "fetchPostLikes", post_id: postId},
        dataType : 'json',
        success: function (successData) {
          if (successData.code && successData.code == '200') {
            defer.resolve(successData.result)
          }else{
            defer.reject(successData);
          }
        },
        error: function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                defer.resolve(responseTextObj.result);
              }else  defer.reject(errData);
            }else  defer.reject(errData);
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };


    this.createPostLike = function (postId) {
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.createPostLike,
        data: {action : "doLike", id  : postId,  type  : "likeThis" },
        dataType : 'json',
        success : function (successData) {
          if (successData.code && successData.code == '200') {
            var numberoflikes = 0;
            try{
              numberoflikes = successData.result.split('+')[0]
            }catch (e){
              console.log(e);
            }
            defer.resolve(numberoflikes);
          }
          defer.resolve(successData);
        },
        error : function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                var numberoflikes = 0;
                try{
                  numberoflikes =responseTextObj.result.split('+')[0];
                }catch (e){
                  console.log(e);
                }
                defer.resolve(numberoflikes);
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };

    function prepCategories(categoryArray) {
      $localStorage.categories = [];
      /*loop through all the categories*/
      for(var p = 0; p < categoryArray.length; p++){
        var tempParentObj = categoryArray[p];
        /*if it is a parent category, remove it from the parent array*/
        if (tempParentObj.parent == '0') {
          tempParentObj.sub_categories = [];
          that.lookupCategoryType[tempParentObj.term_id] = tempParentObj;
          /*loop through the array and find all of its children*/
          for(var c = 0; c < categoryArray.length; c++){
            var subItem = categoryArray[c];
            if (subItem.parent == tempParentObj.term_id) {
              subItem.parent_name = tempParentObj.name;
              subItem.parent_term_id = tempParentObj.term_id;
              tempParentObj.sub_categories.push(subItem);
              that.lookupCategoryType[subItem.term_id] = subItem;
            }
          }
          // categoryArray.splice(p, 1);
          $localStorage.categories.push(tempParentObj);
        }

      }
      $localStorage.lookupCategoryType = that.lookupCategoryType;
      $rootScope.$broadcast('categoriesLoaded');
    }

    this.getPostCategories = function () {
      var defer = $q.defer();

      if ($localStorage.categories && $localStorage.categories.length) {
        $timeout(function () {
          $rootScope.$broadcast('categoriesLoaded');
          defer.resolve(true);
        }, 1000);
        return defer.promise
      }

      $.ajax({
        method: 'POST',
        url: mlConstants.getPostCategories,
        data: {action : "fetchCategories"},
        dataType : 'json',
        success : function (successData) {
          if (successData.code == '200') {
            prepCategories(successData.result);
            defer.resolve(true);
          }else {
            defer.reject(false);
          }
        },
        error : function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                prepCategories(responseTextObj.result);
                defer.resolve(true);
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };

    function prepCategoryPosts(postObject, hasPrevContent) {
      if (postObject.post_data) {
        for (var i = 0; i < postObject.post_data.length; i++) {
          var postItem = postObject.post_data[i];

          /*cut off time from date*/
          if (postItem.post_date) {
            postItem.post_date = postItem.post_date.split(' ')[0];
          }

          if (!(postItem.image && (postItem.image.substring(0, 4) == 'http'))) {
            postItem.image = ""
          }
          else {
            // if (i < 5) {/*load images for slider*/
            // that.categoryPostsForSlider.push(postItem)
            // }
          }
          that.lookupCategoryPost[postItem.ID] = postItem;
        }
        if (hasPrevContent == 0) {
          that.categoryPosts = postObject.post_data;
        }else{
          $.merge(that.categoryPosts, postObject.post_data);
        }
      }
      $rootScope.$broadcast('categoryPostsLoaded');
    }


    this.getCategoryPosts = function (category_id, start, quantity) {
      var defer = $q.defer();

      if (!quantity) quantity = 5;

      if (!start) start = 0;

      $.ajax({
        url : mlConstants.getPostUnderACategory,
        data :  {action : 'getPostByCategory', cat_id : category_id, number : quantity, start : start },
        method : "POST",
        dataType : 'json',
        success : function (successData) {
          if (successData.code == 200) {
            if ($.trim(successData.message) == 'Query returned 0 Rows') {
              $rootScope.$broadcast('categoryPostsLoaded');
              defer.resolve(false);
            }
          }else{
            prepCategoryPosts(successData, start);
          }
          defer.resolve(true);
        },
        error : function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code == 200) {
                if ($.trim(responseTextObj.message) == 'Query returned 0 Rows') {
                  $rootScope.$broadcast('categoryPostsLoaded');
                  defer.resolve(false);
                }
              }else{
                prepCategoryPosts(responseTextObj, start);
              }
              defer.resolve(true);
            }
          }else{
            defer.reject(errData);
          }
        }
      });


      return defer.promise
    };

  });

/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlSuperAdminService', function(mlConstants, $http, $q,
                                           $rootScope, $localStorage, $timeout) {

    this.todaySalesData = [];

    var that = this;

    this.fetchData = function () {
      var defer = $q.defer();

      var limit = 10;
      /*fetch today page on SuperAdmin*/
      $http.post(mlConstants.superAdmin+'?limit=' + limit, {})
        .success(function (successData, status, jqxhr) {
          if (successData.errorCode == "200") {
            that.todaySalesData = successData;
            $rootScope.$broadcast('todaySalesLoaded');
            defer.resolve(successData);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
          defer.resolve(false);
        });
      return defer.promise
    };

    this.fetchEmployeeSaleHistory = function (email) {
      var defer = $q.defer();

      /*fetch sale history on SuperAdmin of an employee*/
      $http.post(mlConstants.salesHistory+'?email=' + email, {})
        .success(function (successData, status, jqxhr) {
          // console.log("success", successData);
          if (successData.code == "200") {
            defer.resolve(successData.data);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
        });
      return defer.promise
    };

    this.postSaleLike = function (email, feedItemId) {
      var defer = $q.defer();

      $http.post(mlConstants.todaySaleLike+'?userEmail=' + email + '&todayFeedItemId=' + feedItemId , {})
        .success(function (successData, status, jqxhr) {
          if (successData.code == "200") {
            defer.resolve(successData);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
        });
      return defer.promise
    };

    this.postSaleComment = function (email, feedItemId, comment) {
      var defer = $q.defer();
      $http.post(mlConstants.todaySaleComment+'?email=' + email + '&userEmail=' + email + '&todayFeedItemId=' + feedItemId + '&comment=' + comment, {})
        .success(function (successData, status, jqxhr) {
          if (successData.code == "200") {
            defer.resolve(successData);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
        });
      return defer.promise
    };

    this.fetchRecentTerritorySalesData = function (territoryId) {
      var defer = $q.defer();

      /*fetch today page by territory on SuperAdmin*/
      $http.post(mlConstants.territorySaleHistory+'?territoryId=' + territoryId, {})
        .success(function (successData, status, jqxhr) {
          if (successData.code == "200") {
            defer.resolve(successData.data);
          }
          defer.reject(false);

        })

        .error(function (errorData) {
          // defer.resolve(sample.data);
        });

      return defer.promise
    };

  });

/**
 * Created by KayGee on 14-Jul-16.
 */

angular.module('mLife')
    .service("UploadImage", ['mlConstants','$q', 'Upload', 'cloudinary',  function (mlConstants, $q, $upload, cloudinary) {


        // Configure The S3 Object
        AWS.config.update({ accessKeyId: mlConstants.aws_access_key, secretAccessKey: mlConstants.aws_secret_access_key });
        AWS.config.region = mlConstants.aws_bucket_region;
        var bucket = new AWS.S3({ params: { Bucket: mlConstants.aws_image_bucket } });

        this.upload = function(file, nameStructure){
            var defer = $q.defer();

            var params = {
                Key : nameStructure || file.name,
                ContentType : file.type|| 'image/jpg',
                Body : file,
                ServerSideEncryption : 'AES256' };

            bucket.putObject(params, function(err, data) {
                if(err) {
                    // There Was An Error With Your S3 Config
                    console.log("error", err.message);
                    defer.reject(false);
                    return false;
                }
                else {
                    // Success!
                    defer.resolve(data);
                }
            })
           /* .on('httpUploadProgress',function(progress) {
                // Log Progress Information
                console.log(Math.round(progress.loaded / progress.total * 100) + '% done');
            })*/;

          // console.log('file', file);
          // console.log('cloudinary.config().upload_preset', cloudinary.config().upload_preset);
          // console.log('cloudinary.config().cloud_name', cloudinary.config().cloud_name);
          // console.log('nameStructure', nameStructure);


         /* $upload.upload({
                  url: "https://api.cloudinary.com/v1_1/" + cloudinary.config().cloud_name + "/upload",
                  data: {
                    upload_preset: cloudinary.config().upload_preset,
                    tags: 'mfeed',
                    context: 'photo=' + nameStructure,
                    file: file
                  }
                }).progress(function (e) {
                  // file.progress = Math.round((e.loaded * 100.0) / e.total);
                  // file.status = "Uploading... " + file.progress + "%";
                }).success(function (data, status, headers, config) {
                  // $rootScope.photos = $rootScope.photos || [];
                  // data.context = {custom: {photo: $scope.title}};
                  // file.result = data;
                  // $rootScope.photos.push(data);
                 defer.resolve(data)
                }).error(function (data, status, headers, config) {
                  // file.result = data;
                });
*/

            return defer.promise;
        };

    }]);



/**
 * Created by Kaygee on 30/03/2015.
 */


angular.module('mLife')
  .factory('Blobber', [function(){

    var blobber = {};

    blobber.blobify = function(dataURI) {
      // convert base64/URLEncoded data component to raw binary data held in a string
      var byteString;
      if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
      else
        byteString = unescape(dataURI.split(',')[1]);

      // separate out the mime component
      var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

      // write the bytes of the string to a typed array
      var ia = new Uint8Array(byteString.length);
      for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
      }

      return new Blob([ia], {type:mimeString});
    };

    return blobber

}]);

/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlWorkDayService', function(mlConstants, $http, $q,
                                        $rootScope, $ionicLoading, $timeout) {

    this.getEmployeesInAnOffice = function (office, officeId) {
      var defer = $q.defer();

      $ionicLoading.show({
        template: 'Getting employees in ' + office,
        duration : 30000,
        noBackdrop : true
      });

      var empData = {"message":"SUCCESS","data":[{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"georgina.bitcon@meltwater.com","phone":"03 9667 9506 ext. 306","firstName":"Georgina","lastName":"Bitcon","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Manager-AUS","skype":"georgiebitcon","salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_9365_today_1412057775819.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=9365"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"beena.yamin@meltwater.com","phone":"03 9667 9514 exr. 313","firstName":"Beena","lastName":"Yamin","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Manager - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_10127_today_1431858118646.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=10127"},{"errorCode":"200","active":"true","manager":"David Hickey","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"cimon.constantine@meltwater.com","phone":"03 9667 9505 ext. 305","firstName":"Cimon","lastName":"Constantine","salesOffice":"CA Melbourne","area":"Australia","title":"Managing Director-AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":"https://au.linkedin.com/in/cimonc","userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_2417_today_1406084355137.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=2417"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"emma.boccalatte@meltwater.com","phone":null,"firstName":"Emma","lastName":"Boccalatte","salesOffice":"CA Melbourne","area":"Australia","title":"Contractor - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=null","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=10463"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"amira.enayatzada@meltwater.com","phone":null,"firstName":"Amira","lastName":"Enayatzada","salesOffice":"CA Melbourne","area":"Australia","title":"Intern - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=null","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=11331"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"michael.oneill@meltwater.com","phone":null,"firstName":"Michael","lastName":"O'Neill","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Consultant - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_11741_today_1475630452760.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=11741"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"amanda.miltinan@meltwater.com","phone":null,"firstName":"Amanda","lastName":"Miltinan","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Consultant - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_11747_today_1466655165617.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=11747"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"steven.wan@meltwater.com","phone":null,"firstName":"Steven","lastName":"Wan","salesOffice":"CA Melbourne","area":"Australia","title":"Intern - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=null","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=12235"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"cushla.mccarthny@meltwater.com","phone":null,"firstName":"Cushla","lastName":"McCarthny","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Consultant - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=null","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=12343"}],"description":null,"errorMessages":[],"code":"200","success":"true"};

      /*fetch employee info on WorkDay*/
      $http.post(mlConstants.allEmployeesInAnOffice+'?territoryId='+officeId, {}, {cache : true})
        .success(function (successData, status, jqxhr) {
          if (successData.code == "200") {
            defer.resolve(successData.data);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
          defer.resolve(empData.data);
        }).finally(function () {
          $ionicLoading.hide();
      });
      return defer.promise
    };


    this.getEmployeeInfo = function (userEmail) {
      var defer = $q.defer();

      $ionicLoading.show({
        template: 'Getting employee details...',
        duration : 30000,
        noBackdrop : true
      });


      if (userEmail == 'info@pollafrique.com') {
        userEmail = 'belen.aleman@meltwater.com';
      }

      /*fetch employee info on WorkDay*/
      $http.post(mlConstants.workDay+'?email='+userEmail, {}, {cache : true})
        .success(function (successData, status, jqxhr) {
          if (successData.errorCode == "200") {
            defer.resolve(successData);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
          // prepPosts(tempPosts, 0);
          defer.resolve({
            active: "false",
            userImageLink: "http://a2.mzstatic.com/us/r30/Purple69/v4/dc/55/d1/dc55d1e8-57a0-3b50-25cb-68de23ad2c74/icon350x350.png",
            area: "Unavailable",
            email: "Unavailable",
            errorCode: "200",
            errorMessage: "",
            firstName: "Details",
            lastName: "Unavailable",
            manager: "Unavailable",
            salesOffice: "Unavailable",
            superArea: "Unavailable",
            title: "Unavailable"
          });
        }).finally(function () {
        $ionicLoading.hide();
      });
      return defer.promise
    };



  });

/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .filter('newlines', function () {
    return function(text) {
      if(text) return text.replace(/\n/g, '<br/>');
      else return '';
    }
  })
  .filter('breaks', function () {
    return function(text) {
      if(text) return text
        .replace('&nbsp;', ' ')
        .replace('&amp;', '&')
        // .replace(/&/g, '&amp;')
        // .replace(/>/g, '&gt;')
        // .replace(/</g, '&lt;');
        ;return '';
    }
  }).filter('entityReplace', function () {
  return function(text) {
    if (text) return text
      .replace('&nbsp;', ' ')
      .replace('&amp;', '&')
      .replace('&gt;', '>')
      .replace('&lt;', '>');
    else return '';
  }
}).filter('parseWpShortCodes', function () {
  return function (input) {
    if (input) return input
      .replace(/\[([a-z0-9])+([^\]]+)*\]/g, '')
      .replace(/\[\/[a-z]+\]/g, '');
    else return '';

  };
})
  .filter('hrefToJS', function ($sce, $sanitize) {
    return function (text) {
      var hrefLink = /href="([\S]+)"/g;
      var targetAtr = /target="_blank"/g;
      var widthReplacement = /width="[\S]+"/g;
      var heightReplacement = /height="[\S]+"/g;
      var iframeDivParent = /<div><iframe/g;
      var iframePeeParent = /<p><iframe/g;
      var styleRemover = /style="[^"]+"/g;
      var iframeUrlProtocol = /<iframe([\w\d\s="])*src="([htps]+)/g;
      var brainsharkUrl = /https:\/\/www\.brainshark\.com\/meltwatergroupg/g;

      var newString = /*$sanitize*/(text)
        .replace(hrefLink, "href=\"#\" onclick=\"return openExtUrlInSystemBrowser('$1')\" ")
        .replace(targetAtr, "target=\"_system\"")
        .replace(widthReplacement, "")
        .replace(heightReplacement, "")
        .replace(styleRemover, " style=\"\" ")
        .replace(iframeDivParent, "<div class=\"iframeParent\"><iframe")
        .replace(iframePeeParent, "<p class=\"iframeParent\"><iframe");

      newString.replace(brainsharkUrl, "www.pollafrique.com");



      // .replace(iframeUrlProtocol, "<div class=\"iframeParent\"><iframe");

      return $sce.trustAsHtml(newString);/*onClick=\" cordova.InAppBrowser.open('$1',  '_system', 'location=yes')\"*/
    }
  })
  .filter('toTrusted', function ($sce) {
    return function (text) {
      return $sce.trustAsHtml(text);
    }
  })

  .filter('trustedResourceUrl', function ($sce) {
    return function (text) {
      return $sce.trustAsResourceUrl(text);
    }
  })

  .filter('reverseArray', function() {
    return function(items) {
      return items.slice().reverse();
    };
  });

/**
 * Created by Kaygee on 21/03/2016.
 *
 * This directive is used in the Today Tab to maintain the same heights on each sale item/child
 */

angular.module('mLife')
  .directive('checkChildrenHeight', function($interval, $compile, mlPostService) {

    function doHeightCheck(element, attrs) {
      $(element).children("div").each(function () {
        var height = 0;
        if (height < $(this).height()) {
          height = $(this).height() + (attrs.height || 1);
        }
        $(element).height(height);
      });
    }

    return{

      scope :{},

      controller : function ($scope) {
        $scope.checkHeightAgain = function ($event, attrs) {
          doHeightCheck($event.target, attrs)
        }
      },

      link :  function(scope, element, attrs) {
        $interval(function () {
          doHeightCheck(element, attrs);
        }, 200, 2);
      }
    };
  });

/**
 * Created by Kaygee on 21/03/2016.
 *
 * This is the directive that controls the Tag list in the comment section of News, Today and mFeed
 */


angular.module('mLife')
  .directive('atInComment', function($timeout, $ionicPopup, mlPostService, mlAuthService) {
    return{

      scope : {
        peopleInMentions : '=peopleInMentions',
        atPushIds : '=atPushIds'
      },

      controller : function ($scope) {


        // $scope.keepKeyboardInFocus = false;

        $scope.filterMWUsers = function (term) {
          $scope.people = [];
          if (term && term.length >= 3) {
            for (var i = 0; i < mlAuthService.userList.length; i++) {
              var user = mlAuthService.userList[i];

              if (user.display_name.toLowerCase().startsWith(term.toLocaleLowerCase())) {
                $scope.people.push(user)
              }
            }
          }
        };


      },

      link :  function(scope, element, attrs) {
        $(element).atwho({
          at: "@",
          data: mlAuthService.userList,
          searchKey: "display_name",
          limit: 10,
          maxLen: 10,
          displayTpl: "<li data-email='${user_email}' data-pushid='${push_id}' data-userid='${ID}'>${display_name}</li>",
          insertTpl: "${atwho-at}${display_name}",
          callbacks : {
            // beforeInsert : function (value, $li) {
            //   console.log(value);
            //   console.log($li);
            //   console.log('atwho-view');
            //   return value;
            // }
            // tplEval: function (tpl, map){
            //     console.log(tpl);
            //     console.log(map);
            // }
          }
        });

        // $(element).on("matched.atwho", function(event, flag, query) {
        //   console.log(event, "matched " + flag + " and the result is " + query);
        // });

        /*
         * inserted.atwho

         Triggered after user choose a popup item in any way. It would receive these arguments:

         atwho event - jQueryEvent : Just a jQuery Event.
         $li - jQuery Object : List Item which have been chosen
         browser event - jQueryEvent : real browser event
         */

        var keyBoardHeight = 0;
        window.addEventListener('native.keyboardshow', function(e){
          keyBoardHeight = e.keyboardHeight;
        });

        $(element).on("reposition.atwho", function(event, offset) {
          // console.log('reposition', event);
          // console.log('offset', offset);
          // console.log('keyBoardHeight', keyBoardHeight);
          // if (ionic.Platform.isIOS()) {
          var atWhoView = $($(this).data('atwho').$el[0]).find('.atwho-view');

          $(atWhoView).css('top', (offset.top - keyBoardHeight + 50) + "px");
          // }
        });


        $(element).on("beforeDestroy.atwho", function() {
          console.log("destroy called");
        });

        $(element).on("inserted.atwho", function(event, liElem, browser) {
          // element.on('blur', function() {
          //   element[0].focus();
          // });

          var email = $(liElem).data('email');
          var pushId = $(liElem).data('pushid');

          if(scope.peopleInMentions.indexOf(email) == -1){
            scope.peopleInMentions.push(email);
          }

          if(pushId !== 'undefined' && scope.atPushIds.indexOf(pushId) == -1){
            scope.atPushIds.push(pushId);
          }
          // $timeout(function () {
          //   element[0].focus();
          // },1000);
          // browser.preventDefault();
          // event.preventDefault();

          return false;
        });

      }
    };
  });

/**
 * Created by Kaygee on 28/12/2016.
 *
 * Directive for resizing and cropping images in mFeed
 */


angular.module('mLife')
  .directive('cropAndResize', function($rootScope, $timeout, $ionicModal, mlFirebaseService, $state, mlKickOffService, $cordovaToast, UploadImage, $ionicActionSheet, $q, $ionicLoading) {
    return {

      scope: {
        venueSet : '=whatsOnYourMind'
      },

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/kickoff/whats_on_your_mind_modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function() {
          $scope.modal.show();
        };

        $scope.closeModal = function() {
          $scope.modal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          delete $scope.model;
          $scope.modal.remove();
        });


        $scope.data = {
          file : '',
          video : ''
        };


        $scope.model = {
          post_content : '',
          image_url : ''
        };

        $scope.upload = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
          console.log($files);
          console.log($file);
          console.log($newFiles);
          console.log($duplicateFiles);
          console.log($invalidFiles);
          console.log($event);
        };


        function uploadImageToS3() {
          var defer = $q.defer();
          var fileName =  new Date().getTime() + '_' +$scope.data.file.name;

          $ionicLoading.show({
            template: 'Uploading image, hang on...',
            duration : 10000
          });

          UploadImage.upload($scope.data.file, 'mlifeimage/' + fileName)
            .then(function (data) {
              $scope.model.image_url = $rootScope.mlifeImageCloud+'mlifeimage/'+fileName;
              defer.resolve(true);
              $ionicLoading.hide();
            }, function () {
              console.log('upload error');
              // $cordovaToast.showShortBottom('Image not uploaded, check network connection' );
              $scope.posting = false;
              $ionicLoading.hide();
              defer.resolve(false);
            });
          return defer.promise;

        }

        function uploadVideoToS3() {
          var defer = $q.defer();
          var fileName =  new Date().getTime() + '_' +$scope.data.video.name;

          $ionicLoading.show({
            template: 'Uploading video, hang on...',
            duration : 10000
          });

          UploadImage.upload($scope.data.video, 'mlifevideo/' + fileName)
            .then(function (data) {
              $scope.model.video_url = $rootScope.mlifeImageCloud+'mlifevideo/'+fileName;
              $ionicLoading.hide();
              defer.resolve(true);
            }, function () {
              // $cordovaToast.showShortBottom('Video not uploaded, check network connection' );
              $scope.posting = false;
              $ionicLoading.hide();
              defer.resolve(false);

            });
          return defer.promise;
        }

        function createPostAndUploadToServer() {
          mlFirebaseService.createAKickOffPost(angular.copy($scope.model));
          $scope.posting = false;
          $ionicLoading.hide();
          $scope.closeModal();

          // $cordovaToast.showShortBottom('Post was successful')
          //   .then(function(success) {
          //     $scope.closeModal();
          //   }, function (error) {
          //     // error
          //   });
        }

        $scope.postToServer = function () {
          if ($scope.model.post_content.length > 1) {

            $scope.posting = true;

            if ($scope.data.file || $scope.data.video) {
              var buttons = [];
              if ($scope.data.file) {
                buttons.push({ text: 'Upload Image' })
              }
              if ($scope.data.video) {
                buttons.push({ text: 'Upload Video' })
              }
              var hideSheet = $ionicActionSheet.show({
                buttons: buttons,
                // destructiveText: 'Delete',
                titleText: 'Select media to upload',
                cancelText: 'Cancel',
                cancel: function() {
                  // add cancel code..
                  $scope.posting = false;
                  hideSheet();
                },
                buttonClicked: function(index) {
                  var uploader;
                  if (index == 0 && $scope.data.file) {
                    uploader = uploadImageToS3()
                  }else if (index == 0 && $scope.data.video) {
                    uploader = uploadVideoToS3()
                  }else if(index == 1){
                    uploader = uploadVideoToS3()
                  }
                  uploader.then(function () {
                    createPostAndUploadToServer()
                  });
                  hideSheet();

                  return true;
                }
              });
            }
            else{
              createPostAndUploadToServer();
            }


            // if ($scope.data.file) {
            //   var fileName =  new Date().getTime() + '_' +$scope.data.file.name;
            //   // UploadImage.upload($scope.data.file, 'placeimage/'+successData._id)
            //   UploadImage.upload($scope.data.file, 'mlifeimage/' + fileName)
            //     .then(function (data) {
            //       $scope.model.image_url = $rootScope.mlifeImageCloud+'mlifeimage/'+fileName;
            //       createPostAndUploadToServer()
            //     }, function () {
            //       console.log('upload error');
            //     });
            // }else{
            //   createPostAndUploadToServer()
            // }
          }else{
            // $cordovaToast.showShortBottom('Please write something in the text area')
          }
        }


      },

      link: function (scope, element, attrs) {
        element.bind('click', function () {
          scope.openModal();
        })
      }
    }
  });

/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive that allows editting and deleting on comments in News Tab
 */


angular.module('mLife')
    .directive('modifyComment', function($timeout, $rootScope, $ionicActionSheet, mlPostService) {
      return{

        scope :{
          comment : '=modifyComment'
        },

        link :  function(scope, element, attrs) {
          var isBeingEdited = false;

          scope.$on('editComment', function (evt, data) {
            if (data.comment.comment_ID === scope.comment.comment_ID) {
              isBeingEdited = true;
            }
          });

          element.bind('click mousedown', function (evt) {
            var buttonsArray = [], deleteText ='';
            if ($rootScope.$localStorage && $rootScope.$localStorage.user) {
              if($rootScope.$localStorage.user.ID === scope.comment.user_id ){
                if (!isBeingEdited) {
                  buttonsArray.push({ text: '<span class="purple">Edit</span>' });
                }else{
                  buttonsArray.push({ text: '<span class="energized">Cancel Editing</span>' });
                }

                // buttonsArray.push({ text: '<span class="positive">Emoji Sample</span>' });
                deleteText = 'Delete';
              }else{
                // buttonsArray.push({ text: 'Tag Author' })
                evt.preventDefault();
                return;
              }
            }


            var commentTitle = scope.comment.comment_content.substring(0, 50);
            if (scope.comment.comment_content.length > 50) {
              commentTitle += '...';
            }

            var div = $('<div />');
            div.text(emojione.shortnameToUnicode(commentTitle));
            div.minEmoji();

            var hideSheet = $ionicActionSheet.show({
              buttons: buttonsArray,
              titleText: div.html(),
              destructiveText: deleteText,
              cancelText: 'Cancel',
              destructiveButtonClicked: function() {
                $rootScope.$broadcast('deleteComment', {comment : scope.comment });
                return true; /*return true to close Action sheet*/
              },
              cancel: function() {
                hideSheet();
              },
              buttonClicked: function(index) {
                if($rootScope.$localStorage.user && $rootScope.$localStorage.user.ID == scope.comment.user_id ){
                  if (index == 0) {
                    if (!isBeingEdited) {
                      $rootScope.$broadcast('editComment', {comment : scope.comment });
                    }else{
                      isBeingEdited = false;
                      $rootScope.$broadcast('cancelEditComment', {comment : scope.comment });
                    }
                  }
                  // if (index == 1) {
                  //   $rootScope.$broadcast('showEmojiComment');
                  // }
                }
                return true;
              }
            });
          });
        }
      };
    });

/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive that makes emojis appear in comments
 */


angular.module('mLife')
    .directive('emojifyComment', function($timeout, $rootScope, $ionicActionSheet, mlPostService) {
        return{

            // scope :{
            //   comment : '@emojifyComment'
            // },

            link :  function(scope, element, attrs) {
                // emojione.toShort(str) - native unicode -> shortnames
                // emojione.shortnameToImage(str) - shortname -> images
                // emojione.unicodeToImage(str) - native unicode -> images
                // emojione.toImage(str) - native unicode + shortnames -> images (mixed input)
                //  emojione.shortnameToUnicode(input) - shortname to Unicode

                // $('.txt').minEmoji();
                // scope.covertedcomment = emojione.toImage(scope.comment);

                $timeout(function () {
                    $(element).text( emojione.shortnameToUnicode( $(element).text() ));
                    $(element).minEmoji();
                }, 10);



            }
            // template : '<span ng-bind-html="comment | embed">span>'
        };
    });


/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive that shows an error image when the src url fails
 */

angular.module('mLife')
  .directive('errSrc', function() {
    return {
      link: function(scope, element, attrs) {
        element.bind('error', function() {
          if (attrs.src != attrs.errSrc) {
            attrs.$set('src', attrs.errSrc);
          }
        });
      }
    }
  });

/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive that lists venues around a location
 */


angular.module('mLife')
  .directive('loadCheckInVenues', function($timeout, $http, $ionicModal, $ionicLoading, mlKickOffService, $cordovaGeolocation, $ionicPopup) {
    return{

      scope : {
        venueSet : '=loadCheckInVenues'
      },

      controller : function ($scope) {

      },

      link :  function(scope, element, attrs) {
        element.bind('click', function () {

          $ionicModal.fromTemplateUrl('mlife/kickoff/list_of_checkin_venues_modal.html', {
            scope: scope,
            animation: 'slide-in-up'
          }).then(function(modal) {
            scope.modal = modal;

            scope.models = {
              searchTerm :  '',
              loading : false
            };

            var posOptions = {timeout: 10000, maximumAge : 1000000, enableHighAccuracy: false};


            var watcher =scope.$watch('models.searchTerm', function (newVal, oldVal) {
              if (newVal && newVal.length > 1) {

                $cordovaGeolocation.getCurrentPosition(posOptions)
                  .then(function (position){

                    var link = 'https://api.foursquare.com/v2/venues/search' +
                      '?client_id=XUB3Z3QNTUMYN3GNZDJORWVON5CTXVMHWPRA51TVFPWDWVBM' +
                      '&client_secret=M34BPHLVRZXZRSGHVOVCB0VZBF5EVOW20SQTDB1IL5IUPWXT' +
                      '&v=20130815' +
                      '&ll='+position.coords.latitude +',' + position.coords.longitude +
                      '&query=' + newVal;
                    // var link = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json' +
                    //   '?key=AIzaSyDF1TcWI_vOha3yMH_wmMzBZ7OHBQ-ybIY' +
                    //   '&radius=500' +
                    //   '&location='+position.coords.latitude +',' + position.coords.longitude
                      /*'&keyword=' + newVal*/

                    scope.models.loading = true;

                    $http.get(link)
                      .success(function (data) {
                        console.log('data : ',data);
                        if (data.meta.code == 200) {
                          scope.venues = data.response.venues;
                        }
                        // if (data.status == "OK") {
                        //   scope.venues = data.results
                        // }
                      })
                      .error(function (some, tin) {
                        console.log(some);
                        console.log(tin);
                        console.log("error");
                      })
                      .finally(function () {
                        scope.models.loading = false;
                      })

                  }, function(err) {


                    $ionicPopup.alert({
                      title: 'Location Error',
                      template: '<p class="text-center">'+ err.message +'</p>'
                    });
                  });
              }

            });

            scope.venueTapped = function (venue) {
              $timeout(function () {
                mlKickOffService.venueSet = venue;
                $('#whats-on-your-mind').trigger('click');
                scope.venueSet = venue;
                // scope.closeModal();
                watcher();
                scope.modal.hide();
                scope.modal.remove();
              })
            };


            scope.closeModal = function() {
              if (scope.venueSet && scope.venueSet.name) {
                scope.venueSet = '';
                mlKickOffService.venueSet = '';
              }
              watcher();
              scope.modal.hide();
              scope.modal.remove();
            };

            // Cleanup the modal when we're done with it!
            scope.$on('$destroy', function() {
              delete scope.model;
              delete scope.venueSet;
              watcher();
              scope.modal.remove();
            });

            modal.show();

          });



        });
      }
    };
  });

/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive to show spinners when an image is loading
 */


angular.module('mLife')
  .directive('imageLoader', function($timeout, $compile, $ionicPopover) {
    function hideLoaderImage(element) {
      $timeout(function () {
        var spinner = $(element).parent().find('.img-spinner').find('ion-spinner');
        spinner.hide();
      });
    }
    return{

      link :  function(scope, element, attrs) {
// show loading image
//         $(element).parent().find('.img-spinner').removeClass('hide');


// main image loaded ?
        $(element).on('load', function(){
          // hide/remove the loading image
          hideLoaderImage(element)
        });

        $(element).on('abort', function(){
          // hide/remove the loading image
          hideLoaderImage(element)
        });

        $(element).on('error', function(){
          // hide/remove the loading image
          hideLoaderImage(element)
        });


      }
    };
  });

/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive to hide keyboard when the return key is tapped.
 */


angular.module('mLife')
  .directive('inputReturn', function($timeout){
    return {
      restrict: 'E',
      // scope: {
      //   'returnClose': '=',
      //   'onReturn': '&'
      // },
      link: function(scope, element, attr){
        element.bind('keydown', function(e){
          if(e.which === 13){
            if(scope.returnClose){
              element[0].blur();
            }
            if(scope.onReturn){
              $timeout(function(){
                scope.onReturn();
              });
            }
          }
        });
      }
    }
  });

/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('likeKickOffPost', function($timeout, $compile, mlKickOffService) {
    return{

      scope :{
        article : '=likeKickOffPost'
      },

      link :  function(scope, element, attrs) {
        element.bind('click', function () {
          if (scope.article) {
            scope.article = mlKickOffService.doLikeOnKickOffPost(scope.article)
          }
        });
      },

      replace : true,
      template : '<span  class="button reg-font col action-row-buttons text-center no-border-active"' +
      ' ng-class="{\'purple\' : article.likers[$localStorage.user.ID].liked == true}">' +
      '<i class="icon ion-thumbsup" ng-hide="loadingLikes"></i>' +
      // '<ion-spinner class="spinner-positive text-center"  ng-show="loadingLikes"></ion-spinner>'{{ article.likeCount || 0 }}  +
      '&nbsp;Like'+
      '</span>'

    };
  });

/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('likeArticle', function($timeout, $compile, mlPostService) {
    return{

      scope :{
        article : '=likeArticle'
      },

      link :  function(scope, element, attrs) {
        element.bind('click', function () {
          if (scope.article) {
            // scope.loadingLikes = true;
            mlPostService.createPostLike(scope.article.ID)
              .then(function (successData) {
                if (successData && successData.code && successData.code == 401) {
                }else{
                  scope.article.user_likes = successData
                }
              }, function (errorData) {
                // scope.error = errorData;
              })
              .finally(function () {
                // scope.loadingLikes = false;
              });

            $timeout(function () {
              if (scope.article.is_liked) {
                scope.article.user_likes --;
              }else{
                scope.article.user_likes ++;
              }
              scope.article.is_liked = !scope.article.is_liked;
            });

          }
        });
      },
      replace : true,
      template : '<span class="subdued col-50 text-center" ng-class="{\'purple\' : article.is_liked == true}">' +
      '<i class="icon ion-thumbsup" ng-hide="loadingLikes"></i>' +
      // '<ion-spinner class="spinner-positive text-center"  ng-show="loadingLikes"></ion-spinner>' +
      '&nbsp;<span ng-show="article.user_likes">{{ article.user_likes }}</span> Like<span ng-show="article.user_likes > 1">s</span>' +
      '</span>'
    };
  });

/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('likeSale', function($timeout, $compile, mlSuperAdminService, $localStorage) {
    return{

      scope :{
        sale : '=likeSale'
      },

      link :  function(scope, element, attrs) {
        element.bind('click', function () {
          if (scope.sale) {
            // scope.loadingLikes = true;
            mlSuperAdminService.postSaleLike($localStorage.user.data.user_email, scope.sale.todayFeedItemId)
              .then(function (successData) {

                if (successData.code == '200') {
                  scope.sale.is_liked = false;

                  scope.sale.likeCount = successData.data.length;
                  if (successData.data.length > 0) {

                    for (var i = 0; i < successData.data.length; i++) {
                      var liker = successData.data[i];
                      if (liker.name == $localStorage.user.data.firstName + "  " + $localStorage.user.data.lastName) {
                        scope.sale.is_liked = true;
                      }
                    }
                  }
                }

              }, function (errorData) {
                console.log('like error', errorData);
              })
              .finally(function () {
                // scope.loadingLikes = false;
              });
          }
        });

        if (scope.sale && scope.sale.likes && scope.sale.likes.length) {
          for (var i = 0; i < scope.sale.likes.length; i++) {
            var liker = scope.sale.likes[i];
            if (liker.name == $localStorage.user.data.firstName + "  " + $localStorage.user.data.lastName) {
              scope.sale.is_liked = true;
            }
          }
        }
      },
      replace : true,
      template : '<span class="subdued" ng-class="{\'purple\' : sale.is_liked == true}">' +
      '<i class="icon ion-thumbsup" ng-hide="loadingLikes"></i>' +
      // '<ion-spinner class="spinner-positive text-center"  ng-show="loadingLikes"></ion-spinner>' +
      '&nbsp;<span ng-show="sale.likeCount != 0">{{ sale.likeCount || "" }}</span> Like<span ng-show="sale.likeCount > 1">s</span>' +
      '</span>'
    };
  });


angular.module('mLife')
  .directive('openCategories', function($ionicPopover, $localStorage) {
    return {

      scope : true,

      controller : function ($scope) {
        $ionicPopover.fromTemplateUrl('mlife/base/news_categories_popover.html', {
          scope: $scope
        }).then(function(popover) {
          $scope.popover = popover;
        });


        $scope.openPopover = function($event) {
          $scope.popover.show($event);
        };

        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.popover.remove();
        });

        $scope.lookupCategory = {
          95 : 'Jorn\'s Corner',
          164 : 'Culture',
          165 : 'Engineering',
          1037 : 'HR',
          1050 : 'IT',
          170 : 'Central Ops',
          000 : 'L&D',
          163 : 'Marketing',
          806 : 'MEST',
          607 : 'Product',
          945 : 'Sales',
          423 : 'SuperAreas'
        };

        $scope.changeNewsCategoryShowing = function (category) {

          $scope.newsCategoryShowing = category;
          if ($scope.popover) {
            $scope.popover.hide();
          }
        }
      },

      link: function(scope, element, attrs) {
        element.bind('click', function(event) {

          scope.openPopover(event);

        });
      }
    }
  });

/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
    .directive('quickComment', function($timeout, $ionicPopup, mlPostService, $state, mlAuthService, $cordovaToast) {
      return{

        scope :{
          article : '=quickComment'
        },

        link :  function(scope, element, attrs) {
          scope.data = {
            comment : ''
          };
          function showCommentSuccess(commentEntered) {
            // A confirm dialog
            var confirmPopup = $ionicPopup.confirm({
              title: 'Comment Posted Successfully',
              subTitle: scope.article.post_title,
              template: ' <i>\" ' + commentEntered + ' \"</i> ',
              cancelText: 'Close',
              okText: 'View'
            });

            confirmPopup.then(function(res) {
              if(res) {
                $state.go('home.select_post', { id : scope.article.ID, scroll : 'toComment' })
              }
            });
          }

          scope.peopleInMentions = [];
          scope.atPushIds = [];

          element.bind('click', function () {
            var myPopup = $ionicPopup.show({
              template: '<textarea  ng-model="data.comment" ' +
              'at-in-comment="" ' +
              'people-in-mentions="peopleInMentions" ' +
              'at-push-ids="atPushIds" class="padding" rows="2" placeholder="Enter comment"></textarea>',
              title: 'Quick Comment',
              subTitle: scope.article.post_title,
              animation : 'none',
              scope: scope,
              buttons: [
                { text: 'Cancel' },
                {
                  text: '<b>Comment</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                    if (scope.postingComment) {
                      e.preventDefault();
                    }
                    scope.postingComment = true;

                    if (scope.data.comment) {
                      scope.article.comments_count ++ ;
                      myPopup.close();
                      /*do app notification tagging*/


                      mlPostService.createPostComment(scope.article.ID, scope.data.comment)
                          .then(function (successData) {
                            showCommentSuccess(angular.copy(scope.data.comment));

                            if (scope.atPushIds.length) {
                              mlAuthService.pushANotification(scope.atPushIds, scope.article, "You have been tagged in a comment", scope.data.comment)
                                  .then(function (success) {
                                        $cordovaToast.showShortBottom('Tagging was successful' )
                                            .then(function(success) {
                                              // success
                                            }, function (error) {
                                              // error
                                            });

                                      },
                                      function (err) {

                                      })
                            }


                          }, function (errorData) {
                            /*error stuff*/
                            // showCommentSuccess(angular.copy(scope.data.comment))

                          })
                          .then(function () {
                            scope.postingComment = false;
                            scope.data.comment = '';
                          })
                    }
                  }
                }
              ]
            });

          });
        }
      };
    });

/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('input', function($timeout){
    return {
      restrict: 'E',
      // scope: {
      //   'returnClose': '=',
      //   'onReturn': '&'
      // },
      link: function(scope, element, attr){
        element.bind('keydown', function(e){
          if(e.which == 13){
            // if(scope.returnClose){
              element[0].blur();
            // }
            if(scope.onReturn){
              $timeout(function(){
                scope.onReturn();
              });
            }
          }
        });
      }
    }
  });

/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('sharePost', function($rootScope, $localStorage, $timeout, $ionicModal, mlFirebaseService, $state, mlKickOffService, $cordovaToast, UploadImage, $ionicActionSheet, $q, $ionicLoading) {
    return {

      scope: {
        sharedPost : '=sharePost'
      },

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/kickoff/sharing_post_modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function() {
          $scope.modal.show();
        };

        $scope.closeModal = function() {
          $scope.modal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          delete $scope.model;
          $scope.modal.remove();
        });

        $timeout(function () {
          $scope.$localStorage = $rootScope.$localStorage || $localStorage;
        });

        $scope.model = {
          shared_post_content : ''
        };


        function createPostAndUploadToServer() {
          mlKickOffService.shareAPost(angular.copy($scope.model));
          $scope.posting = false;
          $ionicLoading.hide();
          $scope.closeModal();
        }

        $scope.postToServer = function () {
          if ($scope.model.shared_post_content.length > 1) {

            $scope.sharedPost.shared_post_content = $scope.model.shared_post_content;

            delete $scope.sharedPost.checkedIn;

            angular.extend($scope.model, $scope.sharedPost);

            $scope.posting = true;

            createPostAndUploadToServer()

          }else{
            try{
              $cordovaToast.showShortBottom('Please write something in the text area')
            }catch(e){

            }
          }
        }


      },

      link: function (scope, element, attrs) {
        element.bind('click', function () {
          scope.openModal();
        })
      }
    }
  });

/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
    .directive('profileImage', function($timeout, $compile, $ionicPopover) {
      return{

        scope : {
          name : '@',
          image : '@profileImage'
        },

        link :  function(scope, element, attrs) {

          $ionicPopover.fromTemplateUrl('mlife/profile/image_popover.html', {
            scope: scope
          }).then(function(popover) {
            scope.popover = popover;
          });


          element.bind('click', function () {
            scope.popover.show(element);
          });

          scope.closePopover = function() {
            scope.popover.hide();
          };

          //Cleanup the popover when we're done with it!
          scope.$on('$destroy', function() {
            scope.popover.remove();
          });

        }
      };
    });

/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('atInWoym', function($timeout, $ionicPopup, mlPostService, mlAuthService) {
    return{

      scope : {
        peopleInMentions : '=peopleInMentions',
        atPushIds : '=atPushIds'
      },

      controller : function ($scope) {


        // $scope.keepKeyboardInFocus = false;

        $scope.filterMWUsers = function (term) {
          $scope.people = [];
          if (term && term.length >= 3) {
            for (var i = 0; i < mlAuthService.userList.length; i++) {
              var user = mlAuthService.userList[i];

              if (user.display_name.toLowerCase().startsWith(term.toLocaleLowerCase())) {
                $scope.people.push(user)
              }
            }
          }
        };


      },

      link :  function(scope, element, attrs) {
        var tribute = new Tribute({
          // collection: [],
          // symbol that starts the lookup
          trigger: '@',

          // element to target for @mentions
          iframe: document.getElementById('menutag'),

          // class added in the flyout menu for active item
          selectClass: 'purple',

          // function called on select that returns the content to insert
          selectTemplate: function (item) {
            console.log('selected', item);
            var email = item.original.user_email;
            var pushId = item.original.push_id;
            //
            if(scope.peopleInMentions.indexOf(email) === -1){
              scope.peopleInMentions.push(email);
            }

            if(pushId !== 'undefined' && scope.atPushIds.indexOf(pushId) === -1){
              scope.atPushIds.push(pushId);
            }
            return '@' + item.original.display_name;
          },

          // template for displaying item in menu
          menuItemTemplate: function (item) {
            return item.string;
          },

          // template for when no match is found (optional),
          // If no template is provided, menu is hidden.
          noMatchTemplate: null,

          // specify an alternative parent container for the menu
          menuContainer: document.getElementById('menutag'),

          // column to search against in the object (accepts function or string)
          lookup: 'display_name',

          // column that contains the content to insert by default
          fillAttr: 'display_name',

          // REQUIRED: array of objects to match
          values: mlAuthService.userList,

          // specify whether a space is required before the trigger character
          requireLeadingSpace: false,

          // specify whether a space is allowed in the middle of mentions
          allowSpaces: false,

          // optionally specify a custom suffix for the replace text
          // (defaults to empty space if undefined)
          replaceTextSuffix: '\n'
        });
        tribute.attach(element);

      }
    };
  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('login', {
        url: '/login',
        controller : 'mlAuthController',
        templateUrl: 'mlife/authentication/auth_login.html'
      });
      /*.state('login', {
        url: '/login',
        controller : 'mlSSOController',
        templateUrl: 'mlife/authentication/sso_login.html'
      })*/

  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlAuthController', function($scope, $state, $ionicLoading, $localStorage,
                                           mlAuthService, mlNotifierService, mlFirebaseService, mlWorkDayService) {


    $scope.login = {
      username : ''
    };

    var timesTapped = 0;
    $scope.fillinLogin = function(){
      if (timesTapped > 7) {
        $scope.login.username = 'belen.aleman@meltwater.com';
        // $scope.login.password = 'pollafrique2016';
        $scope.showLoginForm = true;
      }
      //   $scope.login.username = 'Test.user5@meltwater.com ';
      //   $scope.login.password = 'Password12';
      timesTapped ++;
    };

    $scope.authenticateWithSSO = function () {

      if ($scope.showLoginForm && $scope.login.username) {
        $ionicLoading.show({
          template: 'Verifying details, hang on...',
          duration : 10000,
          noBackdrop : true
        });

        mlAuthService.loginUser($scope.login.username);

      }else {
        mlAuthService.authWithSSO()
      }


    };


  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'mlife/base/base.html',
        controller : 'mlBaseController'
        /*views: {
         'home-tab': {
         }
         }*/
      });
    // .state('tabs.home.profile', {
    //     url: '/profile',
    //     templateUrl: 'mlife/base/demo/mat_profile.html',
    //     controller : 'ProfileCtrl'
    // })
    // .state('tabs.home.friends', {
    //     url: '/friends',
    //     templateUrl: 'mlife/base/demo/mat_friends.html',
    //     controller : 'FriendsCtrl'
    // })
    // .state('tabs.home.gallery', {
    //     url: '/gallery',
    //     templateUrl: 'mlife/base/demo/mat_gallery.html',
    //     controller : 'GalleryCtrl'
    // })
    // .state('tabs.home.activity', {
    //     url: '/activity',
    //     templateUrl: 'mlife/base/demo/mat_activity.html',
    //     controller : 'ActivityCtrl'
    // })
  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlBaseController', function($rootScope, $scope, $state, mlPostService,
                                           $ionicLoading, mlAuthService,
                                           $localStorage, $window, $timeout,
                                           $ionicHistory, $ionicNavBarDelegate) {

    // $scope.$on('$ionicView.loaded', function(e) {
    // console.log('$ionicView.loaded', $ionicHistory.viewHistory());
    // });
    /*
     * if given group is the selected group, deselect it
     * else, select the given group
     */

    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };

    $scope.$on('categoriesLoaded', function () {
      prepCategories();
    });

    // $scope.categories = [{
    //   name : "Test",
    //   sub_categories : [{
    //     name : "Test 2"
    //   }]},
    //   {
    //     name : "Science",
    //     sub_categories : [{
    //       name : "Mat2"
    //     }]
    //   }];

    function prepCategories() {
      $scope.categories = $localStorage.categories;
      mlPostService.lookupCategoryType = $localStorage.lookupCategoryType;
    }

    if ($localStorage.categories && $localStorage.categories.length) {
      prepCategories();
    }else{
      mlPostService.getPostCategories();
    }


    if(!(mlAuthService.userList && mlAuthService.userList.length)){
      // if ($localStorage.authorization) {
        mlAuthService.getUsersList();
      // }
    }

    $window.openExtUrlInSystemBrowser = function (url) {
      $window.open(url,  '_system', 'location=yes');
      return false;
    };


    $scope.$on('$ionicView.enter', function(e) {
      $ionicNavBarDelegate.showBar(true);
    });



  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home.categories_posts', {
        url: '/:cat_id/categories/:cat_name',
        views: {
          'home-tab': {
            controller : 'mlCategoryPostListController',
            templateUrl: 'mlife/category_posts/cat_post_list.html'
          }
        }

      })

      .state('home.select_category_post', {
        url: '/category_post/:id/:scroll',
        views: {
          'home-tab': {
            controller : 'mlSelectedCategoryPostController',
            templateUrl: 'mlife/base/select_post.html'
          }
        }

      })


  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlCategoryPostListController', function($scope, $state, mlPostService, $rootScope, $timeout, $ionicLoading, mlFirebaseService) {


    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };


    if ($state.params.cat_id) {
      $ionicLoading.show({
        template: 'Fetching posts...',
        duration : 30000,
        noBackdrop : true
      });

      if (mlPostService.lookupCategoryType && mlPostService.lookupCategoryType[$state.params.cat_id]) {
        var categoryChosen = mlPostService.lookupCategoryType[$state.params.cat_id];
        $scope.cat_name = categoryChosen.name;
        if (categoryChosen.parent_name) {
          $scope.cat_parent_name = categoryChosen.parent_name;
          $scope.cat_parent_id = categoryChosen.parent_term_id;
        }
      }else{
        $scope.cat_parent_name = $state.params.cat_name;
      }


      delete mlPostService.categoryPosts;
      mlPostService.getCategoryPosts($state.params.cat_id)
        .finally(function () {
          $ionicLoading.hide();
        });
    }

    function prepPosts(){
      $scope.posts = mlPostService.categoryPosts;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }

    if(mlPostService.categoryPosts && mlPostService.categoryPosts.length ){
      prepPosts();
    }

    $scope.$on('categoryPostsLoaded', function () {
      prepPosts();
    });

    $scope.loadComments = function (postId) {
      mlPostService.getPostComments(postId);
    };


    $scope.selectPost = function (articleId, scroll) {
      var obj = {id : articleId};
      if (scroll) {
        obj.scroll = 'toComment';
      }

      $state.go('home.select_category_post', obj)
    };

    $scope.loadMore = function () {

      if ( $scope.posts && $state.params.cat_id) {
        mlPostService.getCategoryPosts($state.params.cat_id, $scope.posts.length, 5)
          .then(function (data) {
            if (!data) {
              $scope.models.more_posts_is_available = false;
            }
          })
          .finally(function () {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          })
      }
    };

    $scope.doRefresh = function () {
      mlPostService.categoryPosts = [];
      mlPostService.getCategoryPosts($state.params.cat_id, 0, 5)
        .then(function (data) {
          if (data) {
            $scope.$broadcast('scroll.refreshComplete');
          }
        })
    };

  })
  .controller('mlSelectedCategoryPostController',
    function($scope, $state, mlPostService, $ionicPopup, $ionicHistory,
             $ionicScrollDelegate, $timeout, mlNotifierService, mlFirebaseService, $localStorage, $cordovaToast) {


      $scope.$on('categoryPostsLoaded', function () {
        prepCommentsAndLikes();
      });


      function getPostComments(){
        $scope.loadingComments = true;
        mlPostService.getPostComments($state.params.id)
          .then(function (successData) {
            if(successData == "no_comments"){
              $scope.post_comments = []
            }else{
              $scope.post_comments = successData;
            }
          }, function (errorData) {
            $scope.error = errorData;
          })
          .finally(function () {
            $scope.loadingComments = false;
            if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
              $timeout(function () {
                scrolltoLastComment();
              },100);
              // $ionicScrollDelegate.scrollBottom(true);

            }
          });
      }

      function prepCommentsAndLikes() {
        if ($state.params.id) {
          if (mlPostService.lookupCategoryPost[$state.params.id]) {

            $scope.selected_post = mlPostService.lookupCategoryPost[$state.params.id];

            if (!$scope.selected_post.read) {
              mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
              mlPostService.lookupCategoryPost[$state.params.id].read = true;
            }

            getPostComments();

          }
        }
      }
      prepCommentsAndLikes();


      $scope.$on('$ionicView.loaded', function(e) {
        if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
          scrolltoLastComment();
        }
      });

      $scope.comment = {
        comment : ''
      };

      function scrolltoLastComment() {
        $timeout(function () {
          var position = $ionicScrollDelegate.$getByHandle('comments').getScrollPosition();

          $ionicScrollDelegate.resize();

          $ionicScrollDelegate.scrollBottom(true);
        }, 100);
      }

      $scope.peopleInMentions = [];
      $scope.atPushIds = [];
      $scope.postComment = function () {

        var comment = angular.copy($scope.comment.comment);
        if (angular.isDefined(comment) && comment.length > 0  && $state.params.id) {
          $scope.postingComment = true;

          if (!$scope.edittingComment) {
            mlPostService.createPostComment($state.params.id, comment)
              .then(function (successData) {
                $scope.selected_post.comments_count ++;

                /*update comment count on firebase*/
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), false);
                mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment);


                getPostComments();
                scrolltoLastComment();

              }, function (errorData) {
                $scope.error = errorData;
              })
              .then(function () {
                $scope.postingComment = false;
                $scope.comment.comment = '';

                /*do app notification tagging*/
                if ($scope.atPushIds.length) {
                  mlNotifierService.prepTaggingArray(
                    $scope.atPushIds,
                    $scope.peopleInMentions, angular.copy($scope.selected_post),
                    comment, 'category', 'tagged_in_news_comment')
                }
              })
          }
          else{
            mlPostService.editPostComment($scope.edittingComment.comment_ID, comment)
              .then(function (successData) {
                  getPostComments();
                  mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

                },
                function (errorData) {
                  $scope.error = errorData;
                })
              .then(function () {
                $scope.postingComment = false;
                $scope.comment.comment = '';
                delete $scope.edittingComment;

              })
          }


        }
      };

      $scope.postLike = function () {
        if ($state.params.id) {
          mlPostService.createPostLike($state.params.id)
            .then(function (successData) {

              $scope.selected_post.user_likes = successData;

              /*update comment count on firebase*/
              mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), false);
              mlFirebaseService.updateUserLastLike(angular.copy($scope.selected_post))

            }, function (errorData) {
              $scope.error = errorData;
            })
            .finally(function () {
            });

          $timeout(function () {
            if ($scope.selected_post.is_liked) {
              $scope.selected_post.user_likes --;
            }else{
              $scope.selected_post.user_likes ++;
            }
            $scope.selected_post.is_liked = !$scope.selected_post.is_liked;

          });


        }
      };

      $scope.$on('editComment', function (event, data) {
        $scope.edittingComment = data.comment;
        $scope.comment.comment =  data.comment.comment_content ;
        $ionicScrollDelegate.scrollBottom(true);
      });

      $scope.$on('cancelEditComment', function () {
        delete $scope.edittingComment;
        $scope.comment.comment = "";
      });

      $scope.$on('deleteComment', function (event, data) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Delete Comment',
          template: 'Are you sure you want to delete <br><div class="text-center assertive"><small >'+ data.comment.comment_content +'</small></div>',
          okText: 'Delete', // String (default: 'OK'). The text of the OK button.
          okType : 'button-assertive'
        });

        confirmPopup.then(function(res) {
          if(res) {
            // $scope.postingComment = true;
            for(var i = 0; i < $scope.post_comments.length; i++){
              if ($scope.post_comments[i].comment_ID == data.comment.comment_ID) {
                $scope.post_comments.splice(i, 1);
              }
            }
            mlPostService.deletePostComment(data.comment.comment_ID)
              .then(function (successData) {
                // getPostComments();
                $scope.selected_post.comments_count --;

                /*update comment count on firebase*/
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
              }, function (errorData) {
                $scope.error = errorData;
              })
              .then(function () {
                for(var i = 0; i < $scope.post_comments.length; i++){
                  if ($scope.post_comments[i].comment_ID == data.comment.comment_ID) {
                    $timeout(function () {
                      $scope.post_comments.splice(i, 1);
                    }, 10);
                  }
                }
                $scope.comment.comment = '';
              })

          } else {
            console.log('You are not sure');
          }
        });
      });


      $scope.moveToNextArticle = function () {
        for (var i = 0; i < mlPostService.categoryPosts.length; i++) {
          var post = mlPostService.categoryPosts[i];
          if (post.ID == $state.params.id) {
            if ((i + 1) < mlPostService.categoryPosts.length) {
              $state.go('home.select_category_post', { id : mlPostService.categoryPosts[i + 1].ID });
            }else{
              $ionicPopup.alert({
                title: 'No more articles'
                // template: 'It might taste good'
              });
            }
            break;
          }
        }
      };

      $scope.moveToPreviousArticle = function () {
        // for (var i = 0; i < mlPostService.categoryPosts.length; i++) {
        //     var post = mlPostService.categoryPosts[i];
        //     if (post.ID == $state.params.id) {
        //         if ((i - 1) >= 0) {
        //             $state.go('home.select_category_post', { id : mlPostService.categoryPosts[i - 1].ID });
        //         }else{
        //             $state.go('home.categories_posts');
        //         }
        //         break;
        //     }
        // }
      };



    });


/**
 * Created by KayGee on 21-Jun-16.
 */


angular.module('mLife')
  .controller('TerritorySuperAreaCtrl', function($scope, $stateParams, $timeout, mlOfficeAreaService, mlFirebaseService, $ionicLoading) {

    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };


    $scope.reloadSuperAreas = function () {
      mlOfficeAreaService.getSuperAreas()
        .then(function (status) {
          $scope.superAreas = mlOfficeAreaService.superAreas;
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        });
    };

    if (mlOfficeAreaService.superAreas && mlOfficeAreaService.superAreas.length){
      $scope.superAreas = mlOfficeAreaService.superAreas;
      $scope.$broadcast('scroll.refreshComplete');
    }else{
      $ionicLoading.show({
        template: 'Getting Super Areas...',
        duration : 30000,
        noBackdrop : true
      });
      $scope.reloadSuperAreas()
    }

  })

  .controller('TerritoryAreaCtrl', function($scope, $stateParams, $timeout, mlOfficeAreaService, mlFirebaseService) {

    $scope.reloadAreas = function () {
      if ($stateParams.superarea) {
        mlOfficeAreaService.getAreasUnderSuperArea($stateParams.superarea)
          .then(function (successData) {
            $scope.areas = successData;
            $scope.$broadcast('scroll.refreshComplete');
          })
          .finally(function () {
            mlFirebaseService.increaseATerritorysViewCount({
              name : $stateParams.superarea,
              type : "Super Area"
            }, 'super_areas')
          })
      }
    };


    $scope.reloadAreas()

  })

  .controller('TerritoryOfficeCtrl', function($scope, $stateParams, $timeout, mlOfficeAreaService, mlFirebaseService) {

    $scope.reloadOffices = function () {
      if ($stateParams.area) {
        mlOfficeAreaService.getOfficesUnderArea($stateParams.area)
          .then(function (successData) {
            $scope.offices = successData;
            $scope.$broadcast('scroll.refreshComplete');
          })
          .finally(function () {
            mlFirebaseService.increaseATerritorysViewCount({
              name : $stateParams.area,
              type : "Area"
            }, 'areas')
          });
      }
    };

    $scope.selectOffice = function (office) {
      mlFirebaseService.increaseATerritorysViewCount(office, 'offices')
    };


    $scope.reloadOffices()

  })
  .controller('TerritoryEmployeesCtrl', function($scope, $stateParams, $timeout, mlWorkDayService, mlFirebaseService, mlSuperAdminService) {

    $scope.notloading = false;

    $scope.reloadEmployees = function () {
      if ($stateParams.office) {

        mlWorkDayService.getEmployeesInAnOffice($stateParams.office, $stateParams.officeId)
          .then(function (successData) {
            $scope.employees = successData;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.notloading = true;

          })
          .finally(function () {
            // mlFirebaseService.increaseATerritorysViewCount({
            //   name : $stateParams.area,
            //   type : "Area"
            // }, 'areas')


            $scope.models = {
              more_users_is_available : true,
              userShowLimit : 8,
              isShowing : false,
              searchTerm : ''
            };
          });


        mlSuperAdminService.fetchRecentTerritorySalesData($stateParams.officeId)
          .then(function (successData) {
            $scope.salesData = successData;
          })
      }
    };

    $scope.tabToShow = {
      employees : true,
      sales : false
      // favorites : false
    };

    $scope.showTab = function (tab) {
      angular.forEach($scope.tabToShow, function (val, key) {
        $scope.tabToShow[key] = false;
      });
      $scope.tabToShow[tab] = true;
    };


    $scope.loadMore = function () {
      if ($scope.employees && $scope.employees.length) {
        if ($scope.models.userShowLimit >= $scope.employees.length) {
          $scope.models.more_users_is_available = false;
        }else{
          $timeout(function () {
            $scope.models.userShowLimit += 15;
          });
        }
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    $scope.reloadEmployees()

  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home.territories', {
        url: '/territories',
        views : {
          'territories-tab' : {
            template: '<ion-nav-view name="territories"/>',
            controller : 'TerritorySuperAreaCtrl'
          }
        }
      })

      .state('home.territories.superarea', {
        url: '/superarea',
        views : {
          'territories' : {
            templateUrl: 'mlife/offices/super_areas.html',
            controller : 'TerritorySuperAreaCtrl'
          }
        }
      })

      .state('home.territories.area', {
        url: '/area/:superarea',
        views : {
          'territories' : {
            templateUrl: 'mlife/offices/areas_list.html',
            controller : 'TerritoryAreaCtrl'
          }
        }
      })

      .state('home.territories.office', {
        url: '/office/:area',
        views : {
          'territories' : {
            templateUrl: 'mlife/offices/office_list.html',
            controller : 'TerritoryOfficeCtrl'
          }
        }
      })
      .state('home.territories.employees', {
        url: '/employees/:office/:officeId',
        views : {
          "territories" : {
            templateUrl: 'mlife/offices/offices/office_main.html',
            controller : 'TerritoryEmployeesCtrl'
          }
        }
      })
      .state('home.territories.users', {
        url: '/employees/:office',
        views : {
          "territories" : {
            templateUrl: 'mlife/offices/employees.html',
            controller : 'mlUsersController'
          }
        }
      })
  });

/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('countNotifications', function($rootScope, $timeout, $ionicModal, mlFirebaseService, $state, mlAuthService, $localStorage, mlNotifierService, $ionicLoading, $cordovaInAppBrowser, $ionicPopup, $ionicTabsDelegate) {
    return {

      scope: true,

      link: function ($scope, element, attrs) {
        $scope.$watchGroup(['update', '$localStorage'], function(newValues, oldValues, scope){
            if ($scope.update && $scope.update.available) {

              if ($localStorage.notifications && $localStorage.notifications.length) {
                $scope.avaliableNotifications = $localStorage.notifications.length + 1;
                console.log('$scope.avaliableNotifications', $scope.avaliableNotifications);
                console.log('$localStorage.notifications',$localStorage.notifications);
              }else{
                $scope.avaliableNotifications = 1;
                console.log('$scope.avaliableNotifications', "one p3");
              }

            }else{
              if ($localStorage.notifications && $localStorage.notifications.length) {
                $scope.avaliableNotifications = $localStorage.notifications.length;
                console.log('else down $localStorage.notifications',$localStorage.notifications);

              }else{
                $scope.avaliableNotifications = 0;
                console.log('$scope.avaliableNotifications', "zero kpooor");

              }
            }

          })

      }
    }
  });

/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('notificationAlerts', function($rootScope, $timeout, $ionicModal, mlFirebaseService, $state, mlAuthService, $localStorage, mlNotifierService, $ionicLoading, $cordovaInAppBrowser, $ionicPopup, $ionicTabsDelegate) {
    return {

      scope: true,

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/notifications/notification_alerts_modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function() {
          $scope.modal.show();
        };

        $scope.closeModal = function() {
          $scope.modal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.modal.remove();
        });


        $scope.models = {
          more_users_is_available : false,
          notificationShowLimit : 15,
          isShowing : false,
          shouldShowDelete : false,
          listCanSwipe  : true,
          searchTerm : ''
        };

        $scope.deleteUpdate = function () {
          delete $scope.update;
        };

        $scope.checkIfUpdateIsAvailable = function () {
          mlNotifierService.checkForUpdates()
            .success(function (data) {
              if (data && (data.version != $scope.appVersion || data.build != $scope.appBuild)) {
                $scope.update = {
                  type : 'new_update',
                  available : true,
                  version : data.version+'.'+ data.build
                };
              }else{
                $scope.deleteUpdate();
              }
            })
        };
        $scope.checkIfUpdateIsAvailable();



        $scope.loadMore = function () {
          if ($localStorage.notification && $localStorage.notification.length) {
            if ($scope.models.userShowLimit >= $localStorage.notification.length) {
              $scope.models.more_users_is_available = false;
            }else{
              $timeout(function () {
                $scope.models.notificationShowLimit += 15;
              });
            }
          }
          $scope.$broadcast('scroll.infiniteScrollComplete');
        };

        $scope.openUpdateSite = function () {

          $ionicLoading.show({
            template: 'Preparing to update...',
            duration : 3000,
            // noBackdrop : true,
            hideOnStateChange : true
          });

          if ($rootScope.isAndroid) {
            window.open('https://play.google.com/store/apps/details?id=app.pollafrique.mlife', '_system', 'location=yes');
          }else{
            window.open('https://mlifeapp.co/updatemlife/', '_system', 'location=yes');
          }

          $scope.deleteUpdate();

        };

        $scope.selectNotification = function (post, index) {
          var objParams = {
            id : post.ID,
            scroll : 'toComment'
          };

          var tabToGo = 0;

          if (post.section == 'main') {
            $ionicTabsDelegate.select(tabToGo);
            $state.go("home.select_post", objParams);

          }else if (post.section == 'category'){
            $ionicTabsDelegate.select(tabToGo);
            $state.go('home.select_category_post', objParams);

          }else if (post.section == 'today'){
            $ionicTabsDelegate.select(tabToGo);
            objParams.index = objParams.id;
            $state.go('home.today.select_sale', objParams);

          }else if (post.section == "kickoff"){
            tabToGo = 4;
            $ionicTabsDelegate.select(tabToGo);
            $state.go('home.kickoff.selected', objParams);
          }

          $scope.closeModal();

          $localStorage.notifications.splice(index, 1)
        };

      },

      link: function (scope, element, attrs) {
        scope.checkIfUpdateIsAvailable();

        element.bind('click', function () {
          scope.checkIfUpdateIsAvailable();
          scope.openModal();
        })
      }
    }
  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('home.notifications', {
        url: '/notifications',
        controller : 'mlNotificationsController',
        templateUrl: 'mlife/notifications/notifications.html'
      })




  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home.posts', {
        url: '/:cat_id/posts',
        views: {
          'home-tab': {
            templateUrl: 'mlife/posts/post_list.html',
            controller : 'mlPostListController'
          }
        }
      })

      .state('home.select_post', {
        url: '/post/:id/:scroll',
        views: {
          'home-tab': {
            controller : 'mlSelectedPostController',
            templateUrl: 'mlife/base/select_post.html'
          }
        }
      })


  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlPostListController', function($scope, $state, mlPostService,
                                               $rootScope, $timeout, $ionicLoading, mlNotifierService, $ionicHistory, mlFirebaseService) {

    // $ionicLoading.show({
    //   template: '',
    //   duration : 9900000000000
    // });

    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };
    $scope.options = {
      loop: false,
      effect: 'slide',
      speed: 500
    };

    $scope.doRefresh = function () {
      mlNotifierService.showLoader();

      mlPostService.posts = [];
      if (!mlPostService.fetching ) {

        mlPostService.getPosts(0, 5)
          .then(function (data) {
            $scope.$broadcast('scroll.refreshComplete');
          });

        mlPostService.getNewsSlider()
          .then(function (data) {
            $scope.sliderData = data.result;
            $timeout(function () {
              dataChangeHandler();

              mlNotifierService.hideLoader();

            }, 2000)
          });

      }
    };

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      // grab an instance of the slider
      $scope.slider = data.slider;
    });

    function dataChangeHandler(){
      // call this function when data changes, such as an HTTP request, etc
      if ( $scope.slider ){
        $scope.slider.updateLoop();
      }
    }

    $scope.loadComments = function (postId) {
      // mlPostService.getPostComments(postId);
    };



    function prepPosts(){
      $scope.posts = mlPostService.posts;
      // $scope.postsForSlider = mlPostService.postsForSlider;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }


    $scope.$on('postsLoaded', function () {
      prepPosts();
    });

    $scope.selectPost = function (articleId, scroll) {
      var obj = {id : articleId};
      if (scroll) {
        obj.scroll = 'toComment';
      }

      $state.go('home.select_post', obj)
    };

    $scope.loadMore = function () {
      // $scope.$broadcast('scroll.infiniteScrollComplete');
      //
      // return false;
      if ($scope.posts) {
        mlPostService.getPosts($scope.posts.length + 1, 5)
          .then(function (data) {
            if (!data) {
              $scope.models.more_posts_is_available = false;
            }
          })
          .finally(function () {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          })
      }else{
        $scope.doRefresh();
      }
    };

    if(mlPostService.posts && mlPostService.posts.length ){
      prepPosts();
    }else{
      // $scope.loadInitialPosts();
      // $scope.doRefresh();
      $scope.loadMore();
    }



    $timeout(function () {
      // $ionicHistory.clearHistory();
      // $ionicHistory.clearCache();
      $state.params.cat_id = 0;

      // delete mlPostService.deletecategoryPosts;
      // delete mlPostService.lookupCategoryPost;

    }, 1000);




  })
  .controller('mlSelectedPostController',
    function($scope, $state, mlPostService, $ionicSideMenuDelegate, $ionicPopup,
             $cordovaToast, $ionicLoading, $ionicHistory, mlNotifierService, $ionicScrollDelegate, $timeout,
             mlFirebaseService, $localStorage) {

      $scope.$on('postsLoaded', function () {
        prepCommentsAndLikes();
      });


      function getPostComments(){
        $scope.loadingComments = true;
        if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
          $ionicLoading.show({
            template: 'Loading comments, just a sec...',
            duration : 30000,
            noBackdrop : true
          });
        }
        mlPostService.getPostComments($state.params.id)
          .then(function (successData) {
            if(successData == "no_comments"){
              $scope.post_comments = []
            }else{
              $scope.post_comments = successData;
            }
          }, function (errorData) {
            $scope.error = errorData;
          })
          .finally(function () {
            $scope.loadingComments = false;
            if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
              $timeout(function () {
                scrolltoLastComment();
                $ionicLoading.hide();
              },100);
              // $ionicScrollDelegate.scrollBottom(true);

            }
          });
      }

      function prepCommentsAndLikes() {
        if ($state.params.id) {
          if (mlPostService.lookupPost[$state.params.id]) {

            $scope.selected_post = mlPostService.lookupPost[$state.params.id];

            if (!$scope.selected_post.read) {/*increase read per user, may not always apply on refresh*/
              mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
              mlPostService.lookupPost[$state.params.id].read = true;
            }
            getPostComments();

          }else{
            mlPostService.getSinglePostById($state.params.id)
              .then(function (postDataReturned) {
                $scope.selected_post = postDataReturned;
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
                getPostComments();
              })
          }
        }

      }

      prepCommentsAndLikes();

      $scope.comment = {
        comment : ''
      };

      function scrolltoLastComment() {
        $timeout(function () {
          // var position = $ionicScrollDelegate.$getByHandle('comments').getScrollPosition();
          $ionicScrollDelegate.resize();
          $ionicScrollDelegate.scrollBottom(true);
        }, 100);
      }

      $scope.peopleInMentions = [];
      $scope.atPushIds = [];


      $scope.postComment = function () {
        // console.log($scope.peopleInMentions);
        // console.log($scope.atPushIds);

        var comment = angular.copy($scope.comment.comment);
        if (angular.isDefined(comment) && comment.length > 0  && $state.params.id) {
          $scope.postingComment = true;

          if (!$scope.edittingComment) {
            mlPostService.createPostComment($state.params.id, comment)
              .then(function (successData) {
                $state.params.scroll = 'toComment';


                getPostComments();
                $scope.selected_post.comments_count ++;

                /*update comment count on firebase*/
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), false);
                mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

              }, function (errorData) {
                $scope.error = errorData;
              })
              .then(function () {
                $scope.postingComment = false;
                $scope.comment.comment = '';

                /*do app notification tagging*/
                if ($scope.atPushIds.length) {
                  mlNotifierService.prepTaggingArray(
                    $scope.atPushIds,
                    $scope.peopleInMentions, angular.copy($scope.selected_post),
                    comment, 'main', 'tagged_in_news_comment')
                }


              })
          }
          else{
            mlPostService.editPostComment($scope.edittingComment.comment_ID, comment)
              .then(function (successData) {

                  getPostComments();
                  mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

                },
                function (errorData) {
                  $scope.error = errorData;
                })
              .then(function () {
                $scope.postingComment = false;
                $scope.comment.comment = '';
                delete $scope.edittingComment;

              })
          }
        }
      };

      $scope.postLike = function () {
        if ($state.params.id) {
          // $scope.loadingLikes = true;

          mlPostService.createPostLike($state.params.id)
            .then(function (successData) {

              $scope.likes = successData;

              /*update comment count on firebase*/
              mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
              mlFirebaseService.updateUserLastLike(angular.copy($scope.selected_post))


            }, function (errorData) {
              $scope.error = errorData;
            })
            .finally(function () {
              // $scope.loadingLikes = false;
            });

          $timeout(function () {
            if ($scope.selected_post.is_liked) {
              $scope.selected_post.user_likes --;
            }else{
              $scope.selected_post.user_likes ++;
            }
            $scope.selected_post.is_liked = !$scope.selected_post.is_liked;
          });

        }
      };

      $scope.$on('editComment', function (event, data) {
        $scope.edittingComment = data.comment;
        $scope.comment.comment =  data.comment.comment_content ;
        $ionicScrollDelegate.scrollBottom(true);
      });

      $scope.$on('cancelEditComment', function () {
        delete $scope.edittingComment;
        $scope.comment.comment = "";
      });

      $scope.$on('deleteComment', function (event, data) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Delete Comment',
          template: 'Are you sure you want to delete <br><div class="text-center assertive"><small >'+ data.comment.comment_content +'</small></div>',
          okText: 'Delete', // String (default: 'OK'). The text of the OK button.
          okType : 'button-assertive'
        });

        confirmPopup.then(function(res) {
          if(res) {
            // $scope.postingComment = true;
            for(var i = 0; i < $scope.post_comments.length; i++){
              if ($scope.post_comments[i].comment_ID == data.comment.comment_ID) {
                $scope.post_comments.splice(i, 1);
              }
            }
            mlPostService.deletePostComment(data.comment.comment_ID)
              .then(function (successData) {
                // getPostComments();

                $scope.selected_post.comments_count --;

                /*update comment count on firebase*/
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);

              }, function (errorData) {
                $scope.error = errorData;
              })
              .then(function () {
                for(var i = 0; i < $scope.post_comments.length; i++){
                  if ($scope.post_comments[i].comment_ID == data.comment.comment_ID) {
                    $timeout(function () {
                      $scope.post_comments.splice(i, 1);
                    }, 10);
                  }
                }
                $scope.comment.comment = '';
              })

          } else {
            console.log('You are not sure');
          }
        });
      });


      $scope.moveToNextArticle = function () {

        for (var i = 0; i < mlPostService.posts.length; i++) {
          var post = mlPostService.posts[i];
          if (post.ID == $state.params.id) {
            if ((i + 1) < mlPostService.posts.length) {
              $state.go('home.select_post', { id : mlPostService.posts[i + 1].ID });
            }else{
              $ionicPopup.alert({
                title: 'No more articles'
                // template: 'It might taste good'
              });
            }
            break;
          }
        }
      };

      $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
      });
      $scope.$on('$ionicView.leave', function(){
        $ionicSideMenuDelegate.canDragContent(true);
      });

      $scope.moveToPreviousArticle = function ($event) {
        // for (var i = 0; i < mlPostService.posts.length; i++) {
        //     var post = mlPostService.posts[i];
        //     if (post.ID == $state.params.id) {
        //         if ((i - 1) >= 0) {
        //             $state.go('tabs.select_post', { id : mlPostService.posts[i - 1].ID });
        //
        //         }else{
        //             $state.go('tabs.home.posts');
        //         }
        //         break;
        //     }
        // }
      };


      // window.addEventListener('native.keyboardshow', function(){
      //   scrollBy(left, top, [shouldAnimate])
      //   Param
      // });



    });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

          .state('home.kickoff', {
            url: '/kickoff',
            // abstract : true,
            views : {
              'kickoff-tab' : {
                template: '<ion-nav-view/>',
                controller : 'mlKickOffController'
              }
            }
          })

          .state('home.kickoff.main', {
            url: '/main',
            templateUrl: 'mlife/kickoff/kickoff_home.html',
            controller : 'mlKickOffMainController'
          })

          .state('home.kickoff.selected', {
            url: '/selected/:id/:scroll',
            templateUrl: 'mlife/kickoff/selected_kickoff_post.html',
            controller : 'mlSelectedKickOffPostController'
          })
    });



/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlKickOffController', function(mlFirebaseService, mlKickOffService, $scope, $ionicPopover, $cordovaToast, $timeout){

    $ionicPopover.fromTemplateUrl('mlife/kickoff/share_options_popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });


    $scope.openPopover = function($event, post) {
      $scope.popover.show($event);
      $scope.tapped_post = post;
    };

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.popover.remove();
    });

    $scope.shareDirectly = function () {
      try{
        $cordovaToast.showShortBottom('Sharing post...' )
      }catch (e){
      }

      var post = angular.copy($scope.tapped_post);
      delete post.checkedIn;
      mlKickOffService.shareAPost(post)
        .then(function (data) {
          try{
            $cordovaToast.showShortBottom('Post shared successfully' )
          }catch (e){
          }
        });
      $scope.popover.hide();
    };

    $scope.writeAndShare = function () {
      $scope.popover.hide();
    };


    $scope.venueTapped = function (venue) {
      $scope.$broadcast('closeModal');
      $timeout(function () {
        $('#whats-on-your-mind').trigger('click');
      });
    };


    $scope.hasImageHeight = function (post) {
      if (post.image_url) return '230px';
      else return 'auto'
    }



  })
  .controller('mlKickOffMainController', function($scope, $state, mlKickOffService, $rootScope, $timeout,
                                                  $ionicHistory, mlFirebaseService, $ionicScrollDelegate, pouchDB) {

    // var db = pouchDB('kickoff');

    $scope.kick_off_posts = mlKickOffService.kickOffTimelineArray;

    // console.log( ' $scope.kick_off_posts' ,$scope.kick_off_posts);

    $scope.$on('kickOffPostsLoaded', function () {
      // humane.log("New posts",
      //   { timeout: 4000, waitForMove : true, clickToClose: true, addnCls: 'humane-error' }, function () {
      //   });

      $scope.kick_off_posts = mlKickOffService.kickOffTimelineArray;
      $ionicScrollDelegate.scrollTop(true);
      $scope.$broadcast('scroll.refreshComplete');

    });


    $scope.model = {
      postShowLimit : 100,
      more_posts_available : true
    };

    $scope.loadMore = function () {

      // if ($scope.kick_off_posts && $scope.kick_off_posts.length) {
      //   if ($scope.model.postShowLimit >= $scope.kick_off_posts.length) {
      //     $scope.model.more_posts_available = false;
      //   }else {
      //     $timeout(function () {
      //       $scope.model.postShowLimit += 3;
      //     }, 1000);
      // console.log("fired load more");

      mlKickOffService.loadMoreKickOffPosts($scope.kick_off_posts[$scope.kick_off_posts.length - 1].descSort)
        .then(function (newArray) {
          angular.forEach(newArray, function (item) {
            $scope.kick_off_posts.push(item);
          });
          $scope.$broadcast('scroll.infiniteScrollComplete');

        }, function () {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.model.more_posts_available = false;

        });

    };

    $scope.doRefresh = function () {
      mlKickOffService.initKickOff();
    }



  })
  .controller('mlSelectedKickOffPostController',
    function($scope, $state, $ionicScrollDelegate, $localStorage, $ionicPopup,
             $cordovaToast, $ionicLoading, $ionicHistory, mlNotifierService, mlKickOffService, $timeout,
             mlFirebaseService) {

      $scope.resetCommentForm = function () {
        $scope.model = {
          comment : '',
          owner : {
            email:  $localStorage.user.data.email,
            user_email:  $localStorage.user.data.user_email,
            firstName: $localStorage.user.data.firstName,
            lastName: $localStorage.user.data.lastName,
            userImageLink : $localStorage.user.data.userImageLink
          }
        };
      };

      $scope.setSelectedPost = function() {
        $scope.selected_post = mlKickOffService.lookupPost[$state.params.id];

        $scope.selected_post.$id = $state.params.id;
        $scope.peopleWhoLikedPost = [];
        angular.forEach($scope.selected_post.likers, function (item) {
          $scope.peopleWhoLikedPost.push(item)
        })
      };

      $scope.showmFeedButton = false;

      if ($state.params.id) {
        try{
          $scope.setSelectedPost();
        }catch (e){

          $scope.showmFeedButton = true;

          mlKickOffService.initKickOff();
          $ionicLoading.show({
            template: 'Loading mFeed post',
            duration : 30000
          });
          $scope.$on('kickOffPostsLoaded', function () {
            $scope.setSelectedPost();
          });
        }

        $scope.resetCommentForm();

        $scope.showMfeed = function () {
          $state.go('home.kickoff.main', {}, {reload : true});
        };

        mlFirebaseService.retrieveAPostsComments($state.params.id);
      }


      $scope.scrolltoLastComment = function() {
        $timeout(function () {
          // var position = $ionicScrollDelegate.$getByHandle('comments').getScrollPosition();
          $ionicScrollDelegate.resize();
          $ionicScrollDelegate.scrollBottom(true);
        }, 100);
      };


      $scope.$on('kickOffCommentLoaded', function () {

        $timeout(function () {
          $scope.post_comments = mlKickOffService.selected_posts_comments;

          if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
            $timeout(function () {
              $scope.scrolltoLastComment();
              $ionicLoading.hide();
            },100);
          }
        });
      });

      $scope.peopleInMentions = [];
      $scope.atPushIds = [];


      $scope.postComment = function () {

        $scope.postingComment = true;

        $scope.model.dateCreated = new Date().getTime();

        var commentObj = angular.copy($scope.model);

        $scope.resetCommentForm();

        mlFirebaseService.doCommentOnKickOffPost($state.params.id, commentObj)
          .then(function (data) {

            $scope.selected_post = data;
            $scope.selected_post.ID = $state.params.id;

            mlKickOffService.updateKickoffPost(data);


            /*do app notification tagging*/
            if ($scope.atPushIds.length) {
              console.log('$scope.atPushIds', $scope.atPushIds);
              mlNotifierService.prepTaggingArray(
                $scope.atPushIds,
                $scope.peopleInMentions, angular.copy($scope.selected_post),
                commentObj.comment, 'kickoff', 'tagged_in_kickoff_comment')
            }

            mlFirebaseService.retrieveAPostsComments($state.params.id);
            $scope.postingComment = false;
            // $cordovaToast.showShortBottom('Comment was successful' )
          })
      };






    });

/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('whatsOnYourMind', function($rootScope, $timeout, $ionicModal, mlFirebaseService, mlNotifierService, $state, mlKickOffService, $cordovaToast, UploadImage, $ionicActionSheet, $q, $ionicLoading, Blobber) {
    return {

      scope: true,

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/kickoff/whats_on_your_mind_modal.html', {
          scope : $scope,
          animation : 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function() {

          $scope.peopleInMentions = [];
          $scope.atPushIds = [];

          $scope.modal.show();
        };



        $scope.closeWhatsOnYourMindModal = function() {
          $scope.data ='';
          delete $scope.data;
          $scope.model = '';
          delete $scope.model;
          $scope.venueSet = '';
          delete $scope.venueSet;

          $scope.modal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          delete $scope.model;
          $scope.modal.remove();
        });

        $scope.hideKeyBoard = function(){
          if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.close()
          }
        };

        $scope.data = {
          file : '',
          video : ''
        };


        $scope.model = {
          post_content : '',
          image_url : ''
        };



        $scope.cropPicture = false;

        var originalFileName = '';

        $scope.upload = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
          $scope.image = {
            originalImage: '',
            croppedImage: ''
          };
          if ($newFiles && $newFiles.length) {
            var file = $newFiles[0];
            originalFileName = file.name;
            var reader = new FileReader();
            reader.onload = function (evt) {
              $scope.$apply(function($scope){
                $scope.image.originalImage = evt.target.result;
                $scope.data.file = Blobber.blobify($scope.image.originalImage);

                $scope.data.fileProper = file;

                $scope.cropPicture = false;
              });
            };
            reader.readAsDataURL(file);
          }

        };

        $scope.saveCroppedImage = function () {
          // $timeout(function () {
          //   $scope.data.file = Blobber.blobify($scope.image.croppedImage);
          //   $scope.cropPicture = false;
          // });
        };

        $scope.cancelImageCropping = function () {
          $scope.data = {
            file : '',
            video : ''
          };

          delete $scope.data.file;
          $scope.cropPicture = false;
        };


        function uploadImageToS3() {
          var defer = $q.defer();
          var fileName =  new Date().getTime() + '_' + originalFileName || $scope.data.file.name;

          $ionicLoading.show({
            template: 'Uploading image, hang on...',
            duration : 10000
          });

          UploadImage.upload($scope.data.fileProper, 'mlifeimage_' + fileName)
            .then(function (data) {
              console.log("image uploaded" + data);

              $scope.model.image_url = $rootScope.mlifeImageCloud+'mlifeimage_'+fileName;
              defer.resolve(true);
              $ionicLoading.hide();
            }, function () {
              console.log('upload error');
              // $cordovaToast.showShortBottom('Image not uploaded, check network connection' );
              $scope.posting = false;
              $ionicLoading.hide();
              defer.reject(false);
            });
          return defer.promise;

        }

        function uploadVideoToS3() {
          var defer = $q.defer();
          var fileName =  new Date().getTime() + '_' +$scope.data.video.name;

          $ionicLoading.show({
            template: 'Uploading video, hang on...',
            duration : 10000
          });

          UploadImage.upload($scope.data.video, 'mlifevideo_' + fileName)
            .then(function (data) {
              $scope.model.video_url = $rootScope.mlifeImageCloud+'mlifevideo_'+fileName;
              $ionicLoading.hide();
              defer.resolve(true);
            }, function () {
              // $cordovaToast.showShortBottom('Video not uploaded, check network connection' );
              $scope.posting = false;
              $ionicLoading.hide();
              defer.reject(false);

            });
          return defer.promise;
        }

        function createPostAndUploadToServer() {
          mlKickOffService.createAKickOffPost(angular.copy($scope.model))
            .then(function (post) {

              console.log('on success post : ',post);
              /*do app notification tagging*/
              if ($scope.atPushIds.length) {
                console.log('$scope.atPushIds', $scope.atPushIds);
                mlNotifierService.prepTaggingArray(
                  $scope.atPushIds,
                  $scope.peopleInMentions, post,
                  "You've been tagged in a post", "kickoff", "tagged_in_kickoff_post")
              }

            });

          try{
            $cordovaToast.showShortBottom('Post was successful')
          }catch (e){
          }



          $scope.closeWhatsOnYourMindModal();
          $ionicLoading.hide();
          $timeout(function () {
            $scope.posting = false;
            delete $scope.data;
            delete $scope.model;
            $scope.venueSet = null;
            delete $scope.venueSet;

          }, 3000);
        }

        $scope.postToServer = function () {
          if ($scope.model.post_content.length > 1 || $scope.venueSet || ($scope.data && $scope.data.file) || ($scope.data && $scope.data.video)) {

            if ($scope.venueSet) {
              $scope.model.checkedIn = {
                name : angular.copy($scope.venueSet.name),
                location : angular.copy($scope.venueSet.location)
              }
            }

            $scope.posting = true;

            if (($scope.data && $scope.data.file) || ($scope.data && $scope.data.video)) {
              var buttons = [];
              if ($scope.data.file) {
                buttons.push({ text: 'Upload Image' })
              }
              if ($scope.data.video) {
                buttons.push({ text: 'Upload Video' })
              }
              var hideSheet = $ionicActionSheet.show({
                buttons: buttons,
                // destructiveText: 'Delete',
                titleText: 'Select media to upload',
                cancelText: 'Cancel',
                cancel: function() {
                  // add cancel code..
                  $scope.posting = false;
                  hideSheet();
                },
                buttonClicked: function(index) {
                  var uploader;
                  if (index == 0 && $scope.data.file) {
                    uploader = uploadImageToS3()
                  }else if (index == 0 && $scope.data.video) {
                    uploader = uploadVideoToS3()
                  }else if(index == 1){
                    uploader = uploadVideoToS3()
                  }
                  uploader.then(function () {
                    createPostAndUploadToServer()
                  }, function () {
                    try{
                      $cordovaToast.showShortBottom('Media could not be uploaded')
                    }catch (e){
                    }
                  });
                  hideSheet();

                  return true;
                }
              });
            }
            else{
              createPostAndUploadToServer();
            }
          }else{
            try{
              $cordovaToast.showShortBottom('Please write something in the text area')
            }catch (e){
            }
          }
        }


      },

      link: function ($scope, element, attrs) {
        element.bind('click', function () {
          if (mlKickOffService.venueSet != '') {
            $scope.venueSet = mlKickOffService.venueSet;
          }
          $scope.data = {
            file : '',
            video : ''
          };

          $scope.model = {
            post_content : '',
            image_url : ''
          };
          $scope.openModal();
        })
      }
    }
  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('home.test', {
            url: '/test',
            controller : 'mlTestController',
            templateUrl: 'mlife/test/test.html'
        })
  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .controller('mlTestController', function($rootScope, $scope, $localStorage, $timeout, mlAuthService, mlPostService, $firebaseObject) {


        $scope.test = {
            notification : '',
            success_status : '',
            error_status : ''
        };

        $scope.$on('pushReceived', function (evt, data) {
            $timeout(function () {
                $scope.test.notification = data;
            }, 100);
        });

        $scope.testPush = function () {
                mlAuthService.pushANotification([$rootScope.deviceToken], mlPostService.posts[1], "Hey how would you subscribe :smile:")
                .then(function (success) {
                        $timeout(function () {
                            $scope.test.success_status = success;
                        }, 100);

                    },
                    function (err) {

                        $timeout(function () {
                            $scope.test.error_status = err;
                        }, 100);

                    })
        };

        $scope.save = function () {
            mlAuthService.storeUserPushId( $scope.deviceToken)
                .then(function (success) {
                    $timeout(function () {
                        $scope.test.success_status =  success;
                    });

                }, function (error) {
                    $timeout(function () {
                        $scope.test.error_status = error;
                    });
                });
        };

      var ref = firebase.database().ref().child("data");
      // download the data into a local object
      var syncObject = $firebaseObject(ref);
      // synchronize the object with a three-way data binding
      // click on `index.html` above to see it used in the DOM!
      syncObject.$bindTo($scope, "data");

    });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('tabs', {
                url: '/tab',
                abstract: true,
                templateUrl: 'mlife/tabs/tabs_partial.html'
            })
    });

/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('showEmployeeProfile', function($rootScope, $timeout, $ionicModal, $state, mlKickOffService, $ionicTabsDelegate,
                                             $ionicPopover, $stateParams, $ionicHistory, mlWorkDayService, mlSuperAdminService) {
    return {

      scope: true,

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/profile/employee-profile-modal.html', {
          scope : $scope,
          animation : 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function(email) {

          mlWorkDayService.getEmployeeInfo(email)
            .then(function (successEmployeeData) {
              $scope.selected_employee = successEmployeeData;
              $scope.modal.show();
            });


          mlSuperAdminService.fetchEmployeeSaleHistory(email)
            .then(function (saleData) {
              $scope.sales = saleData;
            })

        };

        $scope.closeWhatsOnYourMindModal = function() {
          $scope.modal.hide();
        };

        $scope.selectOfficeTab = function() {
          $ionicTabsDelegate.select(2);
          $scope.modal.hide();
        };


        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          delete $scope.model;
          $scope.modal.remove();
        });


        $scope.selectPost = function (articleId) {
          $state.go('home.select_post', {id : articleId})
        };

        $scope.tabToShow = {
          details : true,
          sales : false,
          favorites : false
        };

        $scope.showTab = function (tab) {
          angular.forEach($scope.tabToShow, function (val, key) {
            $scope.tabToShow[key] = false;
          });
          $scope.tabToShow[tab] = true;
        };
      },

      link: function ($scope, element, attrs) {
        element.bind('click', function () {
          $scope.openModal(attrs.email);
        })
      }
    }
  });

/**
 * Created by KayGee on 21-Jun-16.
 */


angular.module('mLife')
  .controller('ProfileCtrl', function(
    $scope, $state, mlPostService,
    $ionicPopover, $stateParams, $timeout, $ionicHistory, mlWorkDayService,
    $ionicModal, ionicMaterialInk, ionicMaterialMotion, employeeData, mlFirebaseService) {


    $scope.selectPost = function (articleId) {
      $state.go('home.select_post', {id : articleId})
    };

    $scope.tabToShow = {
      details : true,
      sales : false,
      favorites : false
    };

    $scope.showTab = function (tab) {
      angular.forEach($scope.tabToShow, function (val, key) {
        $scope.tabToShow[key] = false;
      });
      $scope.tabToShow[tab] = true;
    };


    $scope.selected_employee = employeeData;

    if (employeeData) {
      try{
        mlFirebaseService.increaseAnEmployeesViewCount(employeeData)
      }catch (e){
        console.log(e);
      }
    }

    mlWorkDayService.getEmployeeInfo($stateParams.email)
      .then(function (successEmployeeData) {
        $scope.selected_employee = successEmployeeData;
      });


    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });

    $scope.goBack = function(){
      // var historyObj = $ionicHistory.backView();
      // var params = {};
      // if (historyObj.stateParams){
      //   params = historyObj.stateParams
      // }
      // $state.go(historyObj.stateName, params )
    }


  })



  .controller('ProfileDetailCtrl', function($scope, $stateParams, $timeout,
                                            mlNotifierService, $ionicPopup, $ionicLoading, $ionicTabsDelegate) {

    $scope.sendMail = function(email, title){
      var mailWindow = window.open('mailto:'+email+'?subject='+title);
      mailWindow.close();
    };

    $scope.selectOfficeTab = function() {
      $ionicTabsDelegate.select(2);
    };


    $scope.thisyear = new Date().getFullYear();

  })






  .controller('ProfileSalesCtrl', function($scope, $stateParams, $timeout,
                                           ionicMaterialMotion, mlSuperAdminService) {

    if ($stateParams.email) {
      mlSuperAdminService.fetchEmployeeSaleHistory($stateParams.email)
        .then(function (saleData) {
          $scope.sales = saleData;
        })
    }

  })









  .controller('ProfileFavoritesCtrl', function($scope, $stateParams, $timeout, mlPostService) {

    if ($stateParams.email) {
      mlPostService.getFavoritePosts($stateParams.email, 0, 5)
        .then(
          function (successData) {
            /*broadcast fired populates scope*/
          }, function (errorData) {
            console.log('getFav error');
          })
        .finally(function () {
          $scope.$broadcast('scroll.refreshComplete');
        });
    }


    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };

    $scope.doRefresh = function () {
      mlPostService.favouritePosts = [];

      if (!mlPostService.fetchingFavorites && $stateParams.email){

        mlPostService.getFavoritePosts($stateParams.email, 0, 5)
          .then(function (data) {
          }, function () {
          })
          .finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
          })
      }
    };



    function prepPosts(){
      $scope.favPosts = mlPostService.favouritePosts;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }


    $scope.$on('favPostsLoaded', function () {
      prepPosts();
    });


    $scope.loadMore = function () {
      if ($scope.favPosts && $stateParams.email) {
        mlPostService.getFavoritePosts($stateParams.email, $scope.favPosts.length + 1, 5)
          .then(function (data) {
            if (!data) {
              $scope.models.more_posts_is_available = false;
            }
          }, function () {
          })
          .finally(function () {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          })
      }else{
        $scope.doRefresh();
      }
    };

    if(mlPostService.favouritePosts && mlPostService.favouritePosts.length ){
      prepPosts();
    }else{
      $scope.doRefresh();
    }

  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home.profile', {
                url: '/profile/:email',
                // abstract : true,
                views : {
                    'profile-tab' : {
                        templateUrl: 'mlife/profile/profile_main.html',
                        controller : 'ProfileCtrl'
                    }
                },
                resolve : {
                    employeeData : function (mlWorkDayService, $stateParams) {
                        return mlWorkDayService.getEmployeeInfo($stateParams.email);
                    }
                }
            })
    });

/**
 * Created by KayGee on 21-Jun-16.
 */


angular.module('mLife')
  .controller('TodayCtrl', function($scope, $stateParams, $timeout,
                                    mlSuperAdminService, $ionicNavBarDelegate) {

    // $ionicNavBarDelegate.showBar(false);

    $scope.todayData = {};

    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };

    $scope.fetchTodaySales = function(){
      mlSuperAdminService.fetchData()
        .then(
          /*success*/
          function (data) {
            $scope.todayData = data;
            $scope.$broadcast('scroll.refreshComplete');
          },
          /*error*/
          function (data) {
          });
    };

    $scope.fetchTodaySales();



  })
  .controller('TodayMainCtrl', function($scope, $stateParams, $timeout, mlSuperAdminService,
                                        $ionicHistory, $state, $ionicPopover, $ionicNavBarDelegate) {

    // $ionicNavBarDelegate.showBar(false);
    $ionicNavBarDelegate.title('');

    $timeout(function () {
      // $ionicHistory.clearHistory();
      // $ionicHistory.clearCache();
      delete $state.params.index;

      // delete mlPostService.deletecategoryPosts;
      // delete mlPostService.lookupCategoryPost;
    }, 1000);

    if (mlSuperAdminService.todaySalesData) {
      $scope.todayData = mlSuperAdminService.todaySalesData;
    }

    $scope.$on('todaySalesLoaded', function () {
      $scope.todayData = mlSuperAdminService.todaySalesData;
    });

    $scope.changeSalesAreaShowing = function (areaToShow){
      $scope.saleAreaShowing = areaToShow || '';

      if (!$scope.todayData || !$scope.todayData.companyAtAGlance || !$scope.todayData.companyAtAGlance.dealNumber) {
        return;
      }

      if(areaToShow == '') {
        $scope.todayData.dealNumber = $scope.todayData.companyAtAGlance.dealNumber;
        $scope.todayData.trialNumber = $scope.todayData.companyAtAGlance.trialNumber;
        $scope.todayData.totalSales = $scope.todayData.companyAtAGlance.totalSales;
      }else{
        for (var i = 0; i < $scope.todayData.companyAtAGlance.superarea.length; i++) {
          var compGlance = $scope.todayData.companyAtAGlance.superarea[i];

          if (compGlance.superareaName == areaToShow) {
            $scope.todayData.dealNumber = compGlance.dealNumber;
            $scope.todayData.trialNumber = compGlance.trialNumber;
            $scope.todayData.totalSales = compGlance.totalSales;
          }
        }
      }

      if ($scope.popover) {
        $scope.popover.hide();
      }
    };

    $scope.saleAreaShowing = '';
    $scope.$on('scroll.refreshComplete', function () {
      $scope.changeSalesAreaShowing($scope.saleAreaShowing);
    });


    $scope.checkIfItIsAnyonesFirstDeal = function (collaborators) {
      for (var i = 0; i < collaborators.length; i++) {
        var person = collaborators[i];
        if (person.firstDeal == 'true') {
          return true;
        }
      }
      return false;
    };


    $scope.openSaleOnSwipeRight = function (indexSwiped) {
      $state.go('home.today.select_sale', {index : indexSwiped}, {});
    };

    $scope.doRefresh = function () {
      $scope.fetchTodaySales();
    };


    $ionicPopover.fromTemplateUrl('mlife/today/sales_areas_popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });


    $scope.openPopover = function($event) {
      $scope.popover.show($event);
    };

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.popover.remove();
    });

    $scope.model = {
      salesShowLimit : 6,
      more_sales_is_available : true
    };

    $scope.loadMore = function () {
      if ($scope.todayData && $scope.todayData.sales.length) {
        if ($scope.model.salesShowLimit >= $scope.todayData.sales.length) {
          $scope.model.more_sales_is_available = false;
        }else{
          $timeout(function () {
            $scope.model.salesShowLimit += 5;
          }, 1000);
        }
      }
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };


  })

  .controller('SelectedSaleInTodayCtrl', function($scope, $stateParams, $timeout, $state, $localStorage, mlNotifierService,
                                                  mlSuperAdminService, $ionicScrollDelegate, mlFirebaseService) {
    if ($stateParams.index && mlSuperAdminService.todaySalesData.sales.length) {
      $scope.selected_sale = mlSuperAdminService.todaySalesData.sales[$stateParams.index];
      mlFirebaseService.increaseASalesViewCount(angular.copy($scope.selected_sale));
    }

    if ($stateParams.scroll && $stateParams.scroll == 'toComment' ) {
      $timeout(function () {
        $ionicScrollDelegate.scrollBottom(true);
      },100);

    }

    $scope.comment = {
      comment : ''
    };

    $scope.peopleInMentions = [];
    $scope.atPushIds = [];


    $scope.postComment = function () {
      // console.log($scope.peopleInMentions);
      // console.log($scope.atPushIds);

      var comment = angular.copy($scope.comment.comment);
      if (angular.isDefined(comment) && comment.length > 0) {
        $scope.postingComment = true;


        if (!$scope.edittingComment) {
          mlSuperAdminService.postSaleComment($localStorage.user.data.user_email, $scope.selected_sale.todayFeedItemId, comment)
            .then(function (successData) {

              if (successData.code == '200') {
                $scope.selected_sale.comments = successData.data;
                $scope.selected_sale.commentCount = successData.data.length;
                $timeout(function () {
                  $scope.todayData.sales[$stateParams.index] = angular.copy($scope.selected_sale);
                }, 10);
              }
              /*update comment count on firebase*/
              // mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), false);
              // mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

            }, function (errorData) {
              $scope.error = errorData;
            })
            .then(function () {
              $scope.postingComment = false;
              $scope.comment.comment = '';

              /*do app notification tagging*/
              if ($scope.atPushIds.length) {
                $scope.selected_sale.id = $stateParams.index;
                $scope.selected_sale.post_title = 'Today';

                mlNotifierService.prepTaggingArray(
                  $scope.atPushIds,
                  $scope.peopleInMentions, angular.copy($scope.selected_sale),
                  comment, 'today', 'tagged_in_sales_comment')
              }


            })
        }
        else{
          mlPostService.editPostComment($scope.edittingComment.comment_ID, comment)
            .then(function (successData) {

                // mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

              },
              function (errorData) {
                $scope.error = errorData;
              })
            .then(function () {
              $scope.postingComment = false;
              $scope.comment.comment = '';
              delete $scope.edittingComment;

            })
        }
      }
    };

  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home.today', {
        url: '/today',
        views : {
          'today-tab' : {
            template: '<ion-nav-view name="today"/>',
            controller : 'TodayCtrl'
          }
        }
      })

      .state('home.today.main', {
        url: '/main',
        views : {
          'today' : {
            templateUrl: 'mlife/today/today_main.html',
            controller : 'TodayMainCtrl'
          }
        }
      })

      .state('home.today.select_sale', {
        url: '/sales/:index/:scroll',
        views : {
          'today' : {
            templateUrl: 'mlife/today/selected_today_sale.html',
            controller : 'SelectedSaleInTodayCtrl'
          }
        }

      })
  });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('tabs.users', {
                url: '/users',
                views : {
                    "company-tab" : {
                        controller : 'mlUsersController',
                        templateUrl: 'mlife/users/users.html'
                    }
                }
            })
    });

/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .controller('mlUsersController', function($rootScope, $scope, $timeout,
                                              mlPostService, mlAuthService, $localStorage, $cordovaToast) {


        $scope.fetchUsers = function () {
            delete $localStorage.userList;
            mlAuthService.getUsersList();
        };

        function prepUsers() {
            $scope.users = mlAuthService.userList;
            $scope.models = {
                more_users_is_available : true,
                userShowLimit : 15,
                isShowing : false,
                searchTerm : ''
            };
        }

        if(mlAuthService.userList && mlAuthService.userList.length){
            prepUsers();
        }

        $scope.$on('usersLoaded', function () {
            prepUsers();
        });



        $scope.loadMore = function () {
            if ($scope.users && $scope.users.length) {
                if ($scope.models.userShowLimit >= $scope.users.length) {
                    $scope.models.more_users_is_available = false;
                }else{
                    $timeout(function () {
                        $scope.models.userShowLimit += 15;
                    });
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }
        };

        $scope.tagUser = function (user) {
            mlAuthService.pushANotification([user.push_id], mlPostService.posts[0], "@ " + user.display_name + ", you were tagged", "@ " + user.display_name + ", you were tagged")
                .then(function (success) {

                        if ($cordovaToast) {
                            $cordovaToast.showShortBottom('Successfully tagged ' +  '@' + user.display_name)
                                .then(function(success) {
                                    // success
                                }, function (error) {
                                    // error
                                });

                        }
                    },
                    function (err) {


                    })
        }
        ;

    });
