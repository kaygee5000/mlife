/**
 * Created by Kaygee on 08/03/2016.
 */
// angular.module is a global place for creating, registering and retrieving Angular modules
angular.module('mLife', [
  'ionic',
  'ngStorage',
  'ngSanitize',
  'angularMoment',
  'ionicLazyLoad',
  'mn',
  'firebase',
  'ionic-material',
  'ngCordova',
  'ngFileUpload',
  // 'angular-autogrow',
  'ionic-link-tap.tapAction',
  'ionic.ion.headerShrink',
  'ngImgCrop',
  'ionic-zoom-view',
  'pouchdb',
  'cloudinary',
  'angular-cache',
  'alexjoffroy.angular-loaders',
  'templates'
])
  .config(function($stateProvider, $urlRouterProvider,
                   $httpProvider, $ionicConfigProvider, cloudinaryProvider) {

    $urlRouterProvider.otherwise('/login');

    //Http Interceptor to check auth failures for xhr requests and fix in the access token to each request
    $httpProvider.interceptors.push('mlHttpInterceptor');

    // if (!ionic.Platform.isIOS()) {
    //   $ionicConfigProvider.scrolling.jsScrolling(true);
    // }

    cloudinaryProvider
      .set("cloud_name", "meltwater")
      .set("upload_preset", "mlamlifemediaassets");


    $.ajaxSetup({
      /*security token for headers*/
      headers : {
        "token" :  "PnAax2Dy1dje8DCRexHg1IbW8siYrFyRaZ"
      }
    });




    if (!String.prototype.startsWith) {
      String.prototype.startsWith = function(searchString, position){
        position = position || 0;
        return this.substr(position, searchString.length) === searchString;
      };
    }

    // if (/webkit.*mobile/i.test(navigator.userAgent)) {
    //   (function($) {
    //     $.fn.offsetOld = $.fn.offset;
    //     $.fn.offset = function() {
    //       var result = this.offsetOld();
    //       result.top -= window.scrollY;
    //       result.left -= window.scrollX;
    //       return result;
    //     };
    //   })(jQuery);
    // }

  });
