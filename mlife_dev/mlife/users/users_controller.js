/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .controller('mlUsersController', function($rootScope, $scope, $timeout,
                                              mlPostService, mlAuthService, $localStorage, $cordovaToast) {


        $scope.fetchUsers = function () {
            delete $localStorage.userList;
            mlAuthService.getUsersList();
        };

        function prepUsers() {
            $scope.users = mlAuthService.userList;
            $scope.models = {
                more_users_is_available : true,
                userShowLimit : 15,
                isShowing : false,
                searchTerm : ''
            };
        }

        if(mlAuthService.userList && mlAuthService.userList.length){
            prepUsers();
        }

        $scope.$on('usersLoaded', function () {
            prepUsers();
        });



        $scope.loadMore = function () {
            if ($scope.users && $scope.users.length) {
                if ($scope.models.userShowLimit >= $scope.users.length) {
                    $scope.models.more_users_is_available = false;
                }else{
                    $timeout(function () {
                        $scope.models.userShowLimit += 15;
                    });
                }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            }
        };

        $scope.tagUser = function (user) {
            mlAuthService.pushANotification([user.push_id], mlPostService.posts[0], "@ " + user.display_name + ", you were tagged", "@ " + user.display_name + ", you were tagged")
                .then(function (success) {

                        if ($cordovaToast) {
                            $cordovaToast.showShortBottom('Successfully tagged ' +  '@' + user.display_name)
                                .then(function(success) {
                                    // success
                                }, function (error) {
                                    // error
                                });

                        }
                    },
                    function (err) {


                    })
        }
        ;

    });
