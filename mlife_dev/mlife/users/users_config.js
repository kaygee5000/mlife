/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('tabs.users', {
                url: '/users',
                views : {
                    "company-tab" : {
                        controller : 'mlUsersController',
                        templateUrl: 'mlife/users/users.html'
                    }
                }
            })
    });
