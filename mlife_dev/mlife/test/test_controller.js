/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .controller('mlTestController', function($rootScope, $scope, $localStorage, $timeout, mlAuthService, mlPostService, $firebaseObject) {


        $scope.test = {
            notification : '',
            success_status : '',
            error_status : ''
        };

        $scope.$on('pushReceived', function (evt, data) {
            $timeout(function () {
                $scope.test.notification = data;
            }, 100);
        });

        $scope.testPush = function () {
                mlAuthService.pushANotification([$rootScope.deviceToken], mlPostService.posts[1], "Hey how would you subscribe :smile:")
                .then(function (success) {
                        $timeout(function () {
                            $scope.test.success_status = success;
                        }, 100);

                    },
                    function (err) {

                        $timeout(function () {
                            $scope.test.error_status = err;
                        }, 100);

                    })
        };

        $scope.save = function () {
            mlAuthService.storeUserPushId( $scope.deviceToken)
                .then(function (success) {
                    $timeout(function () {
                        $scope.test.success_status =  success;
                    });

                }, function (error) {
                    $timeout(function () {
                        $scope.test.error_status = error;
                    });
                });
        };

      var ref = firebase.database().ref().child("data");
      // download the data into a local object
      var syncObject = $firebaseObject(ref);
      // synchronize the object with a three-way data binding
      // click on `index.html` above to see it used in the DOM!
      syncObject.$bindTo($scope, "data");

    });
