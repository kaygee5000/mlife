/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('home.test', {
            url: '/test',
            controller : 'mlTestController',
            templateUrl: 'mlife/test/test.html'
        })
  });
