/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('tabs', {
                url: '/tab',
                abstract: true,
                templateUrl: 'mlife/tabs/tabs_partial.html'
            })
    });
