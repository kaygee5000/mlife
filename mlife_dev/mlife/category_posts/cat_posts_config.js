/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home.categories_posts', {
        url: '/:cat_id/categories/:cat_name',
        views: {
          'home-tab': {
            controller : 'mlCategoryPostListController',
            templateUrl: 'mlife/category_posts/cat_post_list.html'
          }
        }

      })

      .state('home.select_category_post', {
        url: '/category_post/:id/:scroll',
        views: {
          'home-tab': {
            controller : 'mlSelectedCategoryPostController',
            templateUrl: 'mlife/base/select_post.html'
          }
        }

      })


  });
