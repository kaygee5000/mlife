/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlCategoryPostListController', function($scope, $state, mlPostService, $rootScope, $timeout, $ionicLoading, mlFirebaseService) {


    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };


    if ($state.params.cat_id) {
      $ionicLoading.show({
        template: 'Fetching posts...',
        duration : 30000,
        noBackdrop : true
      });

      if (mlPostService.lookupCategoryType && mlPostService.lookupCategoryType[$state.params.cat_id]) {
        var categoryChosen = mlPostService.lookupCategoryType[$state.params.cat_id];
        $scope.cat_name = categoryChosen.name;
        if (categoryChosen.parent_name) {
          $scope.cat_parent_name = categoryChosen.parent_name;
          $scope.cat_parent_id = categoryChosen.parent_term_id;
        }
      }else{
        $scope.cat_parent_name = $state.params.cat_name;
      }


      delete mlPostService.categoryPosts;
      mlPostService.getCategoryPosts($state.params.cat_id)
        .finally(function () {
          $ionicLoading.hide();
        });
    }

    function prepPosts(){
      $scope.posts = mlPostService.categoryPosts;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }

    if(mlPostService.categoryPosts && mlPostService.categoryPosts.length ){
      prepPosts();
    }

    $scope.$on('categoryPostsLoaded', function () {
      prepPosts();
    });

    $scope.loadComments = function (postId) {
      mlPostService.getPostComments(postId);
    };


    $scope.selectPost = function (articleId, scroll) {
      var obj = {id : articleId};
      if (scroll) {
        obj.scroll = 'toComment';
      }

      $state.go('home.select_category_post', obj)
    };

    $scope.loadMore = function () {

      if ( $scope.posts && $state.params.cat_id) {
        mlPostService.getCategoryPosts($state.params.cat_id, $scope.posts.length, 5)
          .then(function (data) {
            if (!data) {
              $scope.models.more_posts_is_available = false;
            }
          })
          .finally(function () {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          })
      }
    };

    $scope.doRefresh = function () {
      mlPostService.categoryPosts = [];
      mlPostService.getCategoryPosts($state.params.cat_id, 0, 5)
        .then(function (data) {
          if (data) {
            $scope.$broadcast('scroll.refreshComplete');
          }
        })
    };

  })
  .controller('mlSelectedCategoryPostController',
    function($scope, $state, mlPostService, $ionicPopup, $ionicHistory,
             $ionicScrollDelegate, $timeout, mlNotifierService, mlFirebaseService, $localStorage, $cordovaToast) {


      $scope.$on('categoryPostsLoaded', function () {
        prepCommentsAndLikes();
      });


      function getPostComments(){
        $scope.loadingComments = true;
        mlPostService.getPostComments($state.params.id)
          .then(function (successData) {
            if(successData == "no_comments"){
              $scope.post_comments = []
            }else{
              $scope.post_comments = successData;
            }
          }, function (errorData) {
            $scope.error = errorData;
          })
          .finally(function () {
            $scope.loadingComments = false;
            if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
              $timeout(function () {
                scrolltoLastComment();
              },100);
              // $ionicScrollDelegate.scrollBottom(true);

            }
          });
      }

      function prepCommentsAndLikes() {
        if ($state.params.id) {
          if (mlPostService.lookupCategoryPost[$state.params.id]) {

            $scope.selected_post = mlPostService.lookupCategoryPost[$state.params.id];

            if (!$scope.selected_post.read) {
              mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
              mlPostService.lookupCategoryPost[$state.params.id].read = true;
            }

            getPostComments();

          }
        }
      }
      prepCommentsAndLikes();


      $scope.$on('$ionicView.loaded', function(e) {
        if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
          scrolltoLastComment();
        }
      });

      $scope.comment = {
        comment : ''
      };

      function scrolltoLastComment() {
        $timeout(function () {
          var position = $ionicScrollDelegate.$getByHandle('comments').getScrollPosition();

          $ionicScrollDelegate.resize();

          $ionicScrollDelegate.scrollBottom(true);
        }, 100);
      }

      $scope.peopleInMentions = [];
      $scope.atPushIds = [];
      $scope.postComment = function () {

        var comment = angular.copy($scope.comment.comment);
        if (angular.isDefined(comment) && comment.length > 0  && $state.params.id) {
          $scope.postingComment = true;

          if (!$scope.edittingComment) {
            mlPostService.createPostComment($state.params.id, comment)
              .then(function (successData) {
                $scope.selected_post.comments_count ++;

                /*update comment count on firebase*/
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), false);
                mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment);


                getPostComments();
                scrolltoLastComment();

              }, function (errorData) {
                $scope.error = errorData;
              })
              .then(function () {
                $scope.postingComment = false;
                $scope.comment.comment = '';

                /*do app notification tagging*/
                if ($scope.atPushIds.length) {
                  mlNotifierService.prepTaggingArray(
                    $scope.atPushIds,
                    $scope.peopleInMentions, angular.copy($scope.selected_post),
                    comment, 'category', 'tagged_in_news_comment')
                }
              })
          }
          else{
            mlPostService.editPostComment($scope.edittingComment.comment_ID, comment)
              .then(function (successData) {
                  getPostComments();
                  mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

                },
                function (errorData) {
                  $scope.error = errorData;
                })
              .then(function () {
                $scope.postingComment = false;
                $scope.comment.comment = '';
                delete $scope.edittingComment;

              })
          }


        }
      };

      $scope.postLike = function () {
        if ($state.params.id) {
          mlPostService.createPostLike($state.params.id)
            .then(function (successData) {

              $scope.selected_post.user_likes = successData;

              /*update comment count on firebase*/
              mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), false);
              mlFirebaseService.updateUserLastLike(angular.copy($scope.selected_post))

            }, function (errorData) {
              $scope.error = errorData;
            })
            .finally(function () {
            });

          $timeout(function () {
            if ($scope.selected_post.is_liked) {
              $scope.selected_post.user_likes --;
            }else{
              $scope.selected_post.user_likes ++;
            }
            $scope.selected_post.is_liked = !$scope.selected_post.is_liked;

          });


        }
      };

      $scope.$on('editComment', function (event, data) {
        $scope.edittingComment = data.comment;
        $scope.comment.comment =  data.comment.comment_content ;
        $ionicScrollDelegate.scrollBottom(true);
      });

      $scope.$on('cancelEditComment', function () {
        delete $scope.edittingComment;
        $scope.comment.comment = "";
      });

      $scope.$on('deleteComment', function (event, data) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Delete Comment',
          template: 'Are you sure you want to delete <br><div class="text-center assertive"><small >'+ data.comment.comment_content +'</small></div>',
          okText: 'Delete', // String (default: 'OK'). The text of the OK button.
          okType : 'button-assertive'
        });

        confirmPopup.then(function(res) {
          if(res) {
            // $scope.postingComment = true;
            for(var i = 0; i < $scope.post_comments.length; i++){
              if ($scope.post_comments[i].comment_ID == data.comment.comment_ID) {
                $scope.post_comments.splice(i, 1);
              }
            }
            mlPostService.deletePostComment(data.comment.comment_ID)
              .then(function (successData) {
                // getPostComments();
                $scope.selected_post.comments_count --;

                /*update comment count on firebase*/
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
              }, function (errorData) {
                $scope.error = errorData;
              })
              .then(function () {
                for(var i = 0; i < $scope.post_comments.length; i++){
                  if ($scope.post_comments[i].comment_ID == data.comment.comment_ID) {
                    $timeout(function () {
                      $scope.post_comments.splice(i, 1);
                    }, 10);
                  }
                }
                $scope.comment.comment = '';
              })

          } else {
            console.log('You are not sure');
          }
        });
      });


      $scope.moveToNextArticle = function () {
        for (var i = 0; i < mlPostService.categoryPosts.length; i++) {
          var post = mlPostService.categoryPosts[i];
          if (post.ID == $state.params.id) {
            if ((i + 1) < mlPostService.categoryPosts.length) {
              $state.go('home.select_category_post', { id : mlPostService.categoryPosts[i + 1].ID });
            }else{
              $ionicPopup.alert({
                title: 'No more articles'
                // template: 'It might taste good'
              });
            }
            break;
          }
        }
      };

      $scope.moveToPreviousArticle = function () {
        // for (var i = 0; i < mlPostService.categoryPosts.length; i++) {
        //     var post = mlPostService.categoryPosts[i];
        //     if (post.ID == $state.params.id) {
        //         if ((i - 1) >= 0) {
        //             $state.go('home.select_category_post', { id : mlPostService.categoryPosts[i - 1].ID });
        //         }else{
        //             $state.go('home.categories_posts');
        //         }
        //         break;
        //     }
        // }
      };



    });

