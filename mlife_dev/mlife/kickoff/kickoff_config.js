/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

          .state('home.kickoff', {
            url: '/kickoff',
            // abstract : true,
            views : {
              'kickoff-tab' : {
                template: '<ion-nav-view/>',
                controller : 'mlKickOffController'
              }
            }
          })

          .state('home.kickoff.main', {
            url: '/main',
            templateUrl: 'mlife/kickoff/kickoff_home.html',
            controller : 'mlKickOffMainController'
          })

          .state('home.kickoff.selected', {
            url: '/selected/:id/:scroll',
            templateUrl: 'mlife/kickoff/selected_kickoff_post.html',
            controller : 'mlSelectedKickOffPostController'
          })
    });


