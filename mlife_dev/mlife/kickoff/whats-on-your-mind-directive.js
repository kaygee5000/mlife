/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('whatsOnYourMind', function($rootScope, $timeout, $ionicModal, mlFirebaseService, mlNotifierService, $state, mlKickOffService, $cordovaToast, UploadImage, $ionicActionSheet, $q, $ionicLoading, Blobber) {
    return {

      scope: true,

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/kickoff/whats_on_your_mind_modal.html', {
          scope : $scope,
          animation : 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function() {

          $scope.peopleInMentions = [];
          $scope.atPushIds = [];

          $scope.modal.show();
        };



        $scope.closeWhatsOnYourMindModal = function() {
          $scope.data ='';
          delete $scope.data;
          $scope.model = '';
          delete $scope.model;
          $scope.venueSet = '';
          delete $scope.venueSet;

          $scope.modal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          delete $scope.model;
          $scope.modal.remove();
        });

        $scope.hideKeyBoard = function(){
          if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.close()
          }
        };

        $scope.data = {
          file : '',
          video : ''
        };


        $scope.model = {
          post_content : '',
          image_url : ''
        };



        $scope.cropPicture = false;

        var originalFileName = '';

        $scope.upload = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
          $scope.image = {
            originalImage: '',
            croppedImage: ''
          };
          if ($newFiles && $newFiles.length) {
            var file = $newFiles[0];
            originalFileName = file.name;
            var reader = new FileReader();
            reader.onload = function (evt) {
              $scope.$apply(function($scope){
                $scope.image.originalImage = evt.target.result;
                $scope.data.file = Blobber.blobify($scope.image.originalImage);

                $scope.data.fileProper = file;

                $scope.cropPicture = false;
              });
            };
            reader.readAsDataURL(file);
          }

        };

        $scope.saveCroppedImage = function () {
          // $timeout(function () {
          //   $scope.data.file = Blobber.blobify($scope.image.croppedImage);
          //   $scope.cropPicture = false;
          // });
        };

        $scope.cancelImageCropping = function () {
          $scope.data = {
            file : '',
            video : ''
          };

          delete $scope.data.file;
          $scope.cropPicture = false;
        };


        function uploadImageToS3() {
          var defer = $q.defer();
          var fileName =  new Date().getTime() + '_' + originalFileName || $scope.data.file.name;

          $ionicLoading.show({
            template: 'Uploading image, hang on...',
            duration : 10000
          });

          UploadImage.upload($scope.data.fileProper, 'mlifeimage_' + fileName)
            .then(function (data) {
              console.log("image uploaded" + data);

              $scope.model.image_url = $rootScope.mlifeImageCloud+'mlifeimage_'+fileName;
              defer.resolve(true);
              $ionicLoading.hide();
            }, function () {
              console.log('upload error');
              // $cordovaToast.showShortBottom('Image not uploaded, check network connection' );
              $scope.posting = false;
              $ionicLoading.hide();
              defer.reject(false);
            });
          return defer.promise;

        }

        function uploadVideoToS3() {
          var defer = $q.defer();
          var fileName =  new Date().getTime() + '_' +$scope.data.video.name;

          $ionicLoading.show({
            template: 'Uploading video, hang on...',
            duration : 10000
          });

          UploadImage.upload($scope.data.video, 'mlifevideo_' + fileName)
            .then(function (data) {
              $scope.model.video_url = $rootScope.mlifeImageCloud+'mlifevideo_'+fileName;
              $ionicLoading.hide();
              defer.resolve(true);
            }, function () {
              // $cordovaToast.showShortBottom('Video not uploaded, check network connection' );
              $scope.posting = false;
              $ionicLoading.hide();
              defer.reject(false);

            });
          return defer.promise;
        }

        function createPostAndUploadToServer() {
          mlKickOffService.createAKickOffPost(angular.copy($scope.model))
            .then(function (post) {

              console.log('on success post : ',post);
              /*do app notification tagging*/
              if ($scope.atPushIds.length) {
                console.log('$scope.atPushIds', $scope.atPushIds);
                mlNotifierService.prepTaggingArray(
                  $scope.atPushIds,
                  $scope.peopleInMentions, post,
                  "You've been tagged in a post", "kickoff", "tagged_in_kickoff_post")
              }

            });

          try{
            $cordovaToast.showShortBottom('Post was successful')
          }catch (e){
          }



          $scope.closeWhatsOnYourMindModal();
          $ionicLoading.hide();
          $timeout(function () {
            $scope.posting = false;
            delete $scope.data;
            delete $scope.model;
            $scope.venueSet = null;
            delete $scope.venueSet;

          }, 3000);
        }

        $scope.postToServer = function () {
          if ($scope.model.post_content.length > 1 || $scope.venueSet || ($scope.data && $scope.data.file) || ($scope.data && $scope.data.video)) {

            if ($scope.venueSet) {
              $scope.model.checkedIn = {
                name : angular.copy($scope.venueSet.name),
                location : angular.copy($scope.venueSet.location)
              }
            }

            $scope.posting = true;

            if (($scope.data && $scope.data.file) || ($scope.data && $scope.data.video)) {
              var buttons = [];
              if ($scope.data.file) {
                buttons.push({ text: 'Upload Image' })
              }
              if ($scope.data.video) {
                buttons.push({ text: 'Upload Video' })
              }
              var hideSheet = $ionicActionSheet.show({
                buttons: buttons,
                // destructiveText: 'Delete',
                titleText: 'Select media to upload',
                cancelText: 'Cancel',
                cancel: function() {
                  // add cancel code..
                  $scope.posting = false;
                  hideSheet();
                },
                buttonClicked: function(index) {
                  var uploader;
                  if (index == 0 && $scope.data.file) {
                    uploader = uploadImageToS3()
                  }else if (index == 0 && $scope.data.video) {
                    uploader = uploadVideoToS3()
                  }else if(index == 1){
                    uploader = uploadVideoToS3()
                  }
                  uploader.then(function () {
                    createPostAndUploadToServer()
                  }, function () {
                    try{
                      $cordovaToast.showShortBottom('Media could not be uploaded')
                    }catch (e){
                    }
                  });
                  hideSheet();

                  return true;
                }
              });
            }
            else{
              createPostAndUploadToServer();
            }
          }else{
            try{
              $cordovaToast.showShortBottom('Please write something in the text area')
            }catch (e){
            }
          }
        }


      },

      link: function ($scope, element, attrs) {
        element.bind('click', function () {
          if (mlKickOffService.venueSet != '') {
            $scope.venueSet = mlKickOffService.venueSet;
          }
          $scope.data = {
            file : '',
            video : ''
          };

          $scope.model = {
            post_content : '',
            image_url : ''
          };
          $scope.openModal();
        })
      }
    }
  });
