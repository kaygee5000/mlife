/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlKickOffController', function(mlFirebaseService, mlKickOffService, $scope, $ionicPopover, $cordovaToast, $timeout){

    $ionicPopover.fromTemplateUrl('mlife/kickoff/share_options_popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });


    $scope.openPopover = function($event, post) {
      $scope.popover.show($event);
      $scope.tapped_post = post;
    };

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.popover.remove();
    });

    $scope.shareDirectly = function () {
      try{
        $cordovaToast.showShortBottom('Sharing post...' )
      }catch (e){
      }

      var post = angular.copy($scope.tapped_post);
      delete post.checkedIn;
      mlKickOffService.shareAPost(post)
        .then(function (data) {
          try{
            $cordovaToast.showShortBottom('Post shared successfully' )
          }catch (e){
          }
        });
      $scope.popover.hide();
    };

    $scope.writeAndShare = function () {
      $scope.popover.hide();
    };


    $scope.venueTapped = function (venue) {
      $scope.$broadcast('closeModal');
      $timeout(function () {
        $('#whats-on-your-mind').trigger('click');
      });
    };


    $scope.hasImageHeight = function (post) {
      if (post.image_url) return '230px';
      else return 'auto'
    }



  })
  .controller('mlKickOffMainController', function($scope, $state, mlKickOffService, $rootScope, $timeout,
                                                  $ionicHistory, mlFirebaseService, $ionicScrollDelegate, pouchDB) {

    // var db = pouchDB('kickoff');

    $scope.kick_off_posts = mlKickOffService.kickOffTimelineArray;

    // console.log( ' $scope.kick_off_posts' ,$scope.kick_off_posts);

    $scope.$on('kickOffPostsLoaded', function () {
      // humane.log("New posts",
      //   { timeout: 4000, waitForMove : true, clickToClose: true, addnCls: 'humane-error' }, function () {
      //   });

      $scope.kick_off_posts = mlKickOffService.kickOffTimelineArray;
      $ionicScrollDelegate.scrollTop(true);
      $scope.$broadcast('scroll.refreshComplete');

    });


    $scope.model = {
      postShowLimit : 100,
      more_posts_available : true
    };

    $scope.loadMore = function () {

      // if ($scope.kick_off_posts && $scope.kick_off_posts.length) {
      //   if ($scope.model.postShowLimit >= $scope.kick_off_posts.length) {
      //     $scope.model.more_posts_available = false;
      //   }else {
      //     $timeout(function () {
      //       $scope.model.postShowLimit += 3;
      //     }, 1000);
      // console.log("fired load more");

      mlKickOffService.loadMoreKickOffPosts($scope.kick_off_posts[$scope.kick_off_posts.length - 1].descSort)
        .then(function (newArray) {
          angular.forEach(newArray, function (item) {
            $scope.kick_off_posts.push(item);
          });
          $scope.$broadcast('scroll.infiniteScrollComplete');

        }, function () {
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $scope.model.more_posts_available = false;

        });

    };

    $scope.doRefresh = function () {
      mlKickOffService.initKickOff();
    }



  })
  .controller('mlSelectedKickOffPostController',
    function($scope, $state, $ionicScrollDelegate, $localStorage, $ionicPopup,
             $cordovaToast, $ionicLoading, $ionicHistory, mlNotifierService, mlKickOffService, $timeout,
             mlFirebaseService) {

      $scope.resetCommentForm = function () {
        $scope.model = {
          comment : '',
          owner : {
            email:  $localStorage.user.data.email,
            user_email:  $localStorage.user.data.user_email,
            firstName: $localStorage.user.data.firstName,
            lastName: $localStorage.user.data.lastName,
            userImageLink : $localStorage.user.data.userImageLink
          }
        };
      };

      $scope.setSelectedPost = function() {
        $scope.selected_post = mlKickOffService.lookupPost[$state.params.id];

        $scope.selected_post.$id = $state.params.id;
        $scope.peopleWhoLikedPost = [];
        angular.forEach($scope.selected_post.likers, function (item) {
          $scope.peopleWhoLikedPost.push(item)
        })
      };

      $scope.showmFeedButton = false;

      if ($state.params.id) {
        try{
          $scope.setSelectedPost();
        }catch (e){

          $scope.showmFeedButton = true;

          mlKickOffService.initKickOff();
          $ionicLoading.show({
            template: 'Loading mFeed post',
            duration : 30000
          });
          $scope.$on('kickOffPostsLoaded', function () {
            $scope.setSelectedPost();
          });
        }

        $scope.resetCommentForm();

        $scope.showMfeed = function () {
          $state.go('home.kickoff.main', {}, {reload : true});
        };

        mlFirebaseService.retrieveAPostsComments($state.params.id);
      }


      $scope.scrolltoLastComment = function() {
        $timeout(function () {
          // var position = $ionicScrollDelegate.$getByHandle('comments').getScrollPosition();
          $ionicScrollDelegate.resize();
          $ionicScrollDelegate.scrollBottom(true);
        }, 100);
      };


      $scope.$on('kickOffCommentLoaded', function () {

        $timeout(function () {
          $scope.post_comments = mlKickOffService.selected_posts_comments;

          if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
            $timeout(function () {
              $scope.scrolltoLastComment();
              $ionicLoading.hide();
            },100);
          }
        });
      });

      $scope.peopleInMentions = [];
      $scope.atPushIds = [];


      $scope.postComment = function () {

        $scope.postingComment = true;

        $scope.model.dateCreated = new Date().getTime();

        var commentObj = angular.copy($scope.model);

        $scope.resetCommentForm();

        mlFirebaseService.doCommentOnKickOffPost($state.params.id, commentObj)
          .then(function (data) {

            $scope.selected_post = data;
            $scope.selected_post.ID = $state.params.id;

            mlKickOffService.updateKickoffPost(data);


            /*do app notification tagging*/
            if ($scope.atPushIds.length) {
              console.log('$scope.atPushIds', $scope.atPushIds);
              mlNotifierService.prepTaggingArray(
                $scope.atPushIds,
                $scope.peopleInMentions, angular.copy($scope.selected_post),
                commentObj.comment, 'kickoff', 'tagged_in_kickoff_comment')
            }

            mlFirebaseService.retrieveAPostsComments($state.params.id);
            $scope.postingComment = false;
            // $cordovaToast.showShortBottom('Comment was successful' )
          })
      };






    });
