/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home.profile', {
                url: '/profile/:email',
                // abstract : true,
                views : {
                    'profile-tab' : {
                        templateUrl: 'mlife/profile/profile_main.html',
                        controller : 'ProfileCtrl'
                    }
                },
                resolve : {
                    employeeData : function (mlWorkDayService, $stateParams) {
                        return mlWorkDayService.getEmployeeInfo($stateParams.email);
                    }
                }
            })
    });
