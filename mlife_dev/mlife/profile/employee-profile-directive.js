/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('showEmployeeProfile', function($rootScope, $timeout, $ionicModal, $state, mlKickOffService, $ionicTabsDelegate,
                                             $ionicPopover, $stateParams, $ionicHistory, mlWorkDayService, mlSuperAdminService) {
    return {

      scope: true,

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/profile/employee-profile-modal.html', {
          scope : $scope,
          animation : 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function(email) {

          mlWorkDayService.getEmployeeInfo(email)
            .then(function (successEmployeeData) {
              $scope.selected_employee = successEmployeeData;
              $scope.modal.show();
            });


          mlSuperAdminService.fetchEmployeeSaleHistory(email)
            .then(function (saleData) {
              $scope.sales = saleData;
            })

        };

        $scope.closeWhatsOnYourMindModal = function() {
          $scope.modal.hide();
        };

        $scope.selectOfficeTab = function() {
          $ionicTabsDelegate.select(2);
          $scope.modal.hide();
        };


        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          delete $scope.model;
          $scope.modal.remove();
        });


        $scope.selectPost = function (articleId) {
          $state.go('home.select_post', {id : articleId})
        };

        $scope.tabToShow = {
          details : true,
          sales : false,
          favorites : false
        };

        $scope.showTab = function (tab) {
          angular.forEach($scope.tabToShow, function (val, key) {
            $scope.tabToShow[key] = false;
          });
          $scope.tabToShow[tab] = true;
        };
      },

      link: function ($scope, element, attrs) {
        element.bind('click', function () {
          $scope.openModal(attrs.email);
        })
      }
    }
  });
