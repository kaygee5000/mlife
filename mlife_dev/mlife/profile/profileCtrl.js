/**
 * Created by KayGee on 21-Jun-16.
 */


angular.module('mLife')
  .controller('ProfileCtrl', function(
    $scope, $state, mlPostService,
    $ionicPopover, $stateParams, $timeout, $ionicHistory, mlWorkDayService,
    $ionicModal, ionicMaterialInk, ionicMaterialMotion, employeeData, mlFirebaseService) {


    $scope.selectPost = function (articleId) {
      $state.go('home.select_post', {id : articleId})
    };

    $scope.tabToShow = {
      details : true,
      sales : false,
      favorites : false
    };

    $scope.showTab = function (tab) {
      angular.forEach($scope.tabToShow, function (val, key) {
        $scope.tabToShow[key] = false;
      });
      $scope.tabToShow[tab] = true;
    };


    $scope.selected_employee = employeeData;

    if (employeeData) {
      try{
        mlFirebaseService.increaseAnEmployeesViewCount(employeeData)
      }catch (e){
        console.log(e);
      }
    }

    mlWorkDayService.getEmployeeInfo($stateParams.email)
      .then(function (successEmployeeData) {
        $scope.selected_employee = successEmployeeData;
      });


    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });

    $scope.goBack = function(){
      // var historyObj = $ionicHistory.backView();
      // var params = {};
      // if (historyObj.stateParams){
      //   params = historyObj.stateParams
      // }
      // $state.go(historyObj.stateName, params )
    }


  })



  .controller('ProfileDetailCtrl', function($scope, $stateParams, $timeout,
                                            mlNotifierService, $ionicPopup, $ionicLoading, $ionicTabsDelegate) {

    $scope.sendMail = function(email, title){
      var mailWindow = window.open('mailto:'+email+'?subject='+title);
      mailWindow.close();
    };

    $scope.selectOfficeTab = function() {
      $ionicTabsDelegate.select(2);
    };


    $scope.thisyear = new Date().getFullYear();

  })






  .controller('ProfileSalesCtrl', function($scope, $stateParams, $timeout,
                                           ionicMaterialMotion, mlSuperAdminService) {

    if ($stateParams.email) {
      mlSuperAdminService.fetchEmployeeSaleHistory($stateParams.email)
        .then(function (saleData) {
          $scope.sales = saleData;
        })
    }

  })









  .controller('ProfileFavoritesCtrl', function($scope, $stateParams, $timeout, mlPostService) {

    if ($stateParams.email) {
      mlPostService.getFavoritePosts($stateParams.email, 0, 5)
        .then(
          function (successData) {
            /*broadcast fired populates scope*/
          }, function (errorData) {
            console.log('getFav error');
          })
        .finally(function () {
          $scope.$broadcast('scroll.refreshComplete');
        });
    }


    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };

    $scope.doRefresh = function () {
      mlPostService.favouritePosts = [];

      if (!mlPostService.fetchingFavorites && $stateParams.email){

        mlPostService.getFavoritePosts($stateParams.email, 0, 5)
          .then(function (data) {
          }, function () {
          })
          .finally(function () {
            $scope.$broadcast('scroll.refreshComplete');
          })
      }
    };



    function prepPosts(){
      $scope.favPosts = mlPostService.favouritePosts;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }


    $scope.$on('favPostsLoaded', function () {
      prepPosts();
    });


    $scope.loadMore = function () {
      if ($scope.favPosts && $stateParams.email) {
        mlPostService.getFavoritePosts($stateParams.email, $scope.favPosts.length + 1, 5)
          .then(function (data) {
            if (!data) {
              $scope.models.more_posts_is_available = false;
            }
          }, function () {
          })
          .finally(function () {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          })
      }else{
        $scope.doRefresh();
      }
    };

    if(mlPostService.favouritePosts && mlPostService.favouritePosts.length ){
      prepPosts();
    }else{
      $scope.doRefresh();
    }

  });
