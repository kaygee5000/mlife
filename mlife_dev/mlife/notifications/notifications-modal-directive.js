/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('notificationAlerts', function($rootScope, $timeout, $ionicModal, mlFirebaseService, $state, mlAuthService, $localStorage, mlNotifierService, $ionicLoading, $cordovaInAppBrowser, $ionicPopup, $ionicTabsDelegate) {
    return {

      scope: true,

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/notifications/notification_alerts_modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function() {
          $scope.modal.show();
        };

        $scope.closeModal = function() {
          $scope.modal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.modal.remove();
        });


        $scope.models = {
          more_users_is_available : false,
          notificationShowLimit : 15,
          isShowing : false,
          shouldShowDelete : false,
          listCanSwipe  : true,
          searchTerm : ''
        };

        $scope.deleteUpdate = function () {
          delete $scope.update;
        };

        $scope.checkIfUpdateIsAvailable = function () {
          mlNotifierService.checkForUpdates()
            .success(function (data) {
              if (data && (data.version != $scope.appVersion || data.build != $scope.appBuild)) {
                $scope.update = {
                  type : 'new_update',
                  available : true,
                  version : data.version+'.'+ data.build
                };
              }else{
                $scope.deleteUpdate();
              }
            })
        };
        $scope.checkIfUpdateIsAvailable();



        $scope.loadMore = function () {
          if ($localStorage.notification && $localStorage.notification.length) {
            if ($scope.models.userShowLimit >= $localStorage.notification.length) {
              $scope.models.more_users_is_available = false;
            }else{
              $timeout(function () {
                $scope.models.notificationShowLimit += 15;
              });
            }
          }
          $scope.$broadcast('scroll.infiniteScrollComplete');
        };

        $scope.openUpdateSite = function () {

          $ionicLoading.show({
            template: 'Preparing to update...',
            duration : 3000,
            // noBackdrop : true,
            hideOnStateChange : true
          });

          if ($rootScope.isAndroid) {
            window.open('https://play.google.com/store/apps/details?id=app.pollafrique.mlife', '_system', 'location=yes');
          }else{
            window.open('https://mlifeapp.co/updatemlife/', '_system', 'location=yes');
          }

          $scope.deleteUpdate();

        };

        $scope.selectNotification = function (post, index) {
          var objParams = {
            id : post.ID,
            scroll : 'toComment'
          };

          var tabToGo = 0;

          if (post.section == 'main') {
            $ionicTabsDelegate.select(tabToGo);
            $state.go("home.select_post", objParams);

          }else if (post.section == 'category'){
            $ionicTabsDelegate.select(tabToGo);
            $state.go('home.select_category_post', objParams);

          }else if (post.section == 'today'){
            $ionicTabsDelegate.select(tabToGo);
            objParams.index = objParams.id;
            $state.go('home.today.select_sale', objParams);

          }else if (post.section == "kickoff"){
            tabToGo = 4;
            $ionicTabsDelegate.select(tabToGo);
            $state.go('home.kickoff.selected', objParams);
          }

          $scope.closeModal();

          $localStorage.notifications.splice(index, 1)
        };

      },

      link: function (scope, element, attrs) {
        scope.checkIfUpdateIsAvailable();

        element.bind('click', function () {
          scope.checkIfUpdateIsAvailable();
          scope.openModal();
        })
      }
    }
  });
