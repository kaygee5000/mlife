/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('home.notifications', {
        url: '/notifications',
        controller : 'mlNotificationsController',
        templateUrl: 'mlife/notifications/notifications.html'
      })




  });
