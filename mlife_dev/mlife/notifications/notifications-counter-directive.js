/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('countNotifications', function($rootScope, $timeout, $ionicModal, mlFirebaseService, $state, mlAuthService, $localStorage, mlNotifierService, $ionicLoading, $cordovaInAppBrowser, $ionicPopup, $ionicTabsDelegate) {
    return {

      scope: true,

      link: function ($scope, element, attrs) {
        $scope.$watchGroup(['update', '$localStorage'], function(newValues, oldValues, scope){
            if ($scope.update && $scope.update.available) {

              if ($localStorage.notifications && $localStorage.notifications.length) {
                $scope.avaliableNotifications = $localStorage.notifications.length + 1;
                console.log('$scope.avaliableNotifications', $scope.avaliableNotifications);
                console.log('$localStorage.notifications',$localStorage.notifications);
              }else{
                $scope.avaliableNotifications = 1;
                console.log('$scope.avaliableNotifications', "one p3");
              }

            }else{
              if ($localStorage.notifications && $localStorage.notifications.length) {
                $scope.avaliableNotifications = $localStorage.notifications.length;
                console.log('else down $localStorage.notifications',$localStorage.notifications);

              }else{
                $scope.avaliableNotifications = 0;
                console.log('$scope.avaliableNotifications', "zero kpooor");

              }
            }

          })

      }
    }
  });
