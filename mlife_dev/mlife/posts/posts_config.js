/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home.posts', {
        url: '/:cat_id/posts',
        views: {
          'home-tab': {
            templateUrl: 'mlife/posts/post_list.html',
            controller : 'mlPostListController'
          }
        }
      })

      .state('home.select_post', {
        url: '/post/:id/:scroll',
        views: {
          'home-tab': {
            controller : 'mlSelectedPostController',
            templateUrl: 'mlife/base/select_post.html'
          }
        }
      })


  });
