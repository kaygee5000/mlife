/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlPostListController', function($scope, $state, mlPostService,
                                               $rootScope, $timeout, $ionicLoading, mlNotifierService, $ionicHistory, mlFirebaseService) {

    // $ionicLoading.show({
    //   template: '',
    //   duration : 9900000000000
    // });

    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };
    $scope.options = {
      loop: false,
      effect: 'slide',
      speed: 500
    };

    $scope.doRefresh = function () {
      mlNotifierService.showLoader();

      mlPostService.posts = [];
      if (!mlPostService.fetching ) {

        mlPostService.getPosts(0, 5)
          .then(function (data) {
            $scope.$broadcast('scroll.refreshComplete');
          });

        mlPostService.getNewsSlider()
          .then(function (data) {
            $scope.sliderData = data.result;
            $timeout(function () {
              dataChangeHandler();

              mlNotifierService.hideLoader();

            }, 2000)
          });

      }
    };

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      // grab an instance of the slider
      $scope.slider = data.slider;
    });

    function dataChangeHandler(){
      // call this function when data changes, such as an HTTP request, etc
      if ( $scope.slider ){
        $scope.slider.updateLoop();
      }
    }

    $scope.loadComments = function (postId) {
      // mlPostService.getPostComments(postId);
    };



    function prepPosts(){
      $scope.posts = mlPostService.posts;
      // $scope.postsForSlider = mlPostService.postsForSlider;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    }


    $scope.$on('postsLoaded', function () {
      prepPosts();
    });

    $scope.selectPost = function (articleId, scroll) {
      var obj = {id : articleId};
      if (scroll) {
        obj.scroll = 'toComment';
      }

      $state.go('home.select_post', obj)
    };

    $scope.loadMore = function () {
      // $scope.$broadcast('scroll.infiniteScrollComplete');
      //
      // return false;
      if ($scope.posts) {
        mlPostService.getPosts($scope.posts.length + 1, 5)
          .then(function (data) {
            if (!data) {
              $scope.models.more_posts_is_available = false;
            }
          })
          .finally(function () {
            $scope.$broadcast('scroll.infiniteScrollComplete');
          })
      }else{
        $scope.doRefresh();
      }
    };

    if(mlPostService.posts && mlPostService.posts.length ){
      prepPosts();
    }else{
      // $scope.loadInitialPosts();
      // $scope.doRefresh();
      $scope.loadMore();
    }



    $timeout(function () {
      // $ionicHistory.clearHistory();
      // $ionicHistory.clearCache();
      $state.params.cat_id = 0;

      // delete mlPostService.deletecategoryPosts;
      // delete mlPostService.lookupCategoryPost;

    }, 1000);




  })
  .controller('mlSelectedPostController',
    function($scope, $state, mlPostService, $ionicSideMenuDelegate, $ionicPopup,
             $cordovaToast, $ionicLoading, $ionicHistory, mlNotifierService, $ionicScrollDelegate, $timeout,
             mlFirebaseService, $localStorage) {

      $scope.$on('postsLoaded', function () {
        prepCommentsAndLikes();
      });


      function getPostComments(){
        $scope.loadingComments = true;
        if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
          $ionicLoading.show({
            template: 'Loading comments, just a sec...',
            duration : 30000,
            noBackdrop : true
          });
        }
        mlPostService.getPostComments($state.params.id)
          .then(function (successData) {
            if(successData == "no_comments"){
              $scope.post_comments = []
            }else{
              $scope.post_comments = successData;
            }
          }, function (errorData) {
            $scope.error = errorData;
          })
          .finally(function () {
            $scope.loadingComments = false;
            if ($state.params.scroll && $state.params.scroll == 'toComment' ) {
              $timeout(function () {
                scrolltoLastComment();
                $ionicLoading.hide();
              },100);
              // $ionicScrollDelegate.scrollBottom(true);

            }
          });
      }

      function prepCommentsAndLikes() {
        if ($state.params.id) {
          if (mlPostService.lookupPost[$state.params.id]) {

            $scope.selected_post = mlPostService.lookupPost[$state.params.id];

            if (!$scope.selected_post.read) {/*increase read per user, may not always apply on refresh*/
              mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
              mlPostService.lookupPost[$state.params.id].read = true;
            }
            getPostComments();

          }else{
            mlPostService.getSinglePostById($state.params.id)
              .then(function (postDataReturned) {
                $scope.selected_post = postDataReturned;
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
                getPostComments();
              })
          }
        }

      }

      prepCommentsAndLikes();

      $scope.comment = {
        comment : ''
      };

      function scrolltoLastComment() {
        $timeout(function () {
          // var position = $ionicScrollDelegate.$getByHandle('comments').getScrollPosition();
          $ionicScrollDelegate.resize();
          $ionicScrollDelegate.scrollBottom(true);
        }, 100);
      }

      $scope.peopleInMentions = [];
      $scope.atPushIds = [];


      $scope.postComment = function () {
        // console.log($scope.peopleInMentions);
        // console.log($scope.atPushIds);

        var comment = angular.copy($scope.comment.comment);
        if (angular.isDefined(comment) && comment.length > 0  && $state.params.id) {
          $scope.postingComment = true;

          if (!$scope.edittingComment) {
            mlPostService.createPostComment($state.params.id, comment)
              .then(function (successData) {
                $state.params.scroll = 'toComment';


                getPostComments();
                $scope.selected_post.comments_count ++;

                /*update comment count on firebase*/
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), false);
                mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

              }, function (errorData) {
                $scope.error = errorData;
              })
              .then(function () {
                $scope.postingComment = false;
                $scope.comment.comment = '';

                /*do app notification tagging*/
                if ($scope.atPushIds.length) {
                  mlNotifierService.prepTaggingArray(
                    $scope.atPushIds,
                    $scope.peopleInMentions, angular.copy($scope.selected_post),
                    comment, 'main', 'tagged_in_news_comment')
                }


              })
          }
          else{
            mlPostService.editPostComment($scope.edittingComment.comment_ID, comment)
              .then(function (successData) {

                  getPostComments();
                  mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

                },
                function (errorData) {
                  $scope.error = errorData;
                })
              .then(function () {
                $scope.postingComment = false;
                $scope.comment.comment = '';
                delete $scope.edittingComment;

              })
          }
        }
      };

      $scope.postLike = function () {
        if ($state.params.id) {
          // $scope.loadingLikes = true;

          mlPostService.createPostLike($state.params.id)
            .then(function (successData) {

              $scope.likes = successData;

              /*update comment count on firebase*/
              mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);
              mlFirebaseService.updateUserLastLike(angular.copy($scope.selected_post))


            }, function (errorData) {
              $scope.error = errorData;
            })
            .finally(function () {
              // $scope.loadingLikes = false;
            });

          $timeout(function () {
            if ($scope.selected_post.is_liked) {
              $scope.selected_post.user_likes --;
            }else{
              $scope.selected_post.user_likes ++;
            }
            $scope.selected_post.is_liked = !$scope.selected_post.is_liked;
          });

        }
      };

      $scope.$on('editComment', function (event, data) {
        $scope.edittingComment = data.comment;
        $scope.comment.comment =  data.comment.comment_content ;
        $ionicScrollDelegate.scrollBottom(true);
      });

      $scope.$on('cancelEditComment', function () {
        delete $scope.edittingComment;
        $scope.comment.comment = "";
      });

      $scope.$on('deleteComment', function (event, data) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Delete Comment',
          template: 'Are you sure you want to delete <br><div class="text-center assertive"><small >'+ data.comment.comment_content +'</small></div>',
          okText: 'Delete', // String (default: 'OK'). The text of the OK button.
          okType : 'button-assertive'
        });

        confirmPopup.then(function(res) {
          if(res) {
            // $scope.postingComment = true;
            for(var i = 0; i < $scope.post_comments.length; i++){
              if ($scope.post_comments[i].comment_ID == data.comment.comment_ID) {
                $scope.post_comments.splice(i, 1);
              }
            }
            mlPostService.deletePostComment(data.comment.comment_ID)
              .then(function (successData) {
                // getPostComments();

                $scope.selected_post.comments_count --;

                /*update comment count on firebase*/
                mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), true);

              }, function (errorData) {
                $scope.error = errorData;
              })
              .then(function () {
                for(var i = 0; i < $scope.post_comments.length; i++){
                  if ($scope.post_comments[i].comment_ID == data.comment.comment_ID) {
                    $timeout(function () {
                      $scope.post_comments.splice(i, 1);
                    }, 10);
                  }
                }
                $scope.comment.comment = '';
              })

          } else {
            console.log('You are not sure');
          }
        });
      });


      $scope.moveToNextArticle = function () {

        for (var i = 0; i < mlPostService.posts.length; i++) {
          var post = mlPostService.posts[i];
          if (post.ID == $state.params.id) {
            if ((i + 1) < mlPostService.posts.length) {
              $state.go('home.select_post', { id : mlPostService.posts[i + 1].ID });
            }else{
              $ionicPopup.alert({
                title: 'No more articles'
                // template: 'It might taste good'
              });
            }
            break;
          }
        }
      };

      $scope.$on('$ionicView.enter', function(){
        $ionicSideMenuDelegate.canDragContent(false);
      });
      $scope.$on('$ionicView.leave', function(){
        $ionicSideMenuDelegate.canDragContent(true);
      });

      $scope.moveToPreviousArticle = function ($event) {
        // for (var i = 0; i < mlPostService.posts.length; i++) {
        //     var post = mlPostService.posts[i];
        //     if (post.ID == $state.params.id) {
        //         if ((i - 1) >= 0) {
        //             $state.go('tabs.select_post', { id : mlPostService.posts[i - 1].ID });
        //
        //         }else{
        //             $state.go('tabs.home.posts');
        //         }
        //         break;
        //     }
        // }
      };


      // window.addEventListener('native.keyboardshow', function(){
      //   scrollBy(left, top, [shouldAnimate])
      //   Param
      // });



    });
