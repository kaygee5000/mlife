/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('login', {
        url: '/login',
        controller : 'mlAuthController',
        templateUrl: 'mlife/authentication/auth_login.html'
      });
      /*.state('login', {
        url: '/login',
        controller : 'mlSSOController',
        templateUrl: 'mlife/authentication/sso_login.html'
      })*/

  });
