/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlAuthController', function($scope, $state, $ionicLoading, $localStorage,
                                           mlAuthService, mlNotifierService, mlFirebaseService, mlWorkDayService) {


    $scope.login = {
      username : ''
    };

    var timesTapped = 0;
    $scope.fillinLogin = function(){
      if (timesTapped > 7) {
        $scope.login.username = 'belen.aleman@meltwater.com';
        // $scope.login.password = 'pollafrique2016';
        $scope.showLoginForm = true;
      }
      //   $scope.login.username = 'Test.user5@meltwater.com ';
      //   $scope.login.password = 'Password12';
      timesTapped ++;
    };

    $scope.authenticateWithSSO = function () {

      if ($scope.showLoginForm && $scope.login.username) {
        $ionicLoading.show({
          template: 'Verifying details, hang on...',
          duration : 10000,
          noBackdrop : true
        });

        mlAuthService.loginUser($scope.login.username);

      }else {
        mlAuthService.authWithSSO()
      }


    };


  });
