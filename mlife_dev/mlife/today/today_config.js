/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home.today', {
        url: '/today',
        views : {
          'today-tab' : {
            template: '<ion-nav-view name="today"/>',
            controller : 'TodayCtrl'
          }
        }
      })

      .state('home.today.main', {
        url: '/main',
        views : {
          'today' : {
            templateUrl: 'mlife/today/today_main.html',
            controller : 'TodayMainCtrl'
          }
        }
      })

      .state('home.today.select_sale', {
        url: '/sales/:index/:scroll',
        views : {
          'today' : {
            templateUrl: 'mlife/today/selected_today_sale.html',
            controller : 'SelectedSaleInTodayCtrl'
          }
        }

      })
  });
