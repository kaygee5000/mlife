/**
 * Created by KayGee on 21-Jun-16.
 */


angular.module('mLife')
  .controller('TodayCtrl', function($scope, $stateParams, $timeout,
                                    mlSuperAdminService, $ionicNavBarDelegate) {

    // $ionicNavBarDelegate.showBar(false);

    $scope.todayData = {};

    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };

    $scope.fetchTodaySales = function(){
      mlSuperAdminService.fetchData()
        .then(
          /*success*/
          function (data) {
            $scope.todayData = data;
            $scope.$broadcast('scroll.refreshComplete');
          },
          /*error*/
          function (data) {
          });
    };

    $scope.fetchTodaySales();



  })
  .controller('TodayMainCtrl', function($scope, $stateParams, $timeout, mlSuperAdminService,
                                        $ionicHistory, $state, $ionicPopover, $ionicNavBarDelegate) {

    // $ionicNavBarDelegate.showBar(false);
    $ionicNavBarDelegate.title('');

    $timeout(function () {
      // $ionicHistory.clearHistory();
      // $ionicHistory.clearCache();
      delete $state.params.index;

      // delete mlPostService.deletecategoryPosts;
      // delete mlPostService.lookupCategoryPost;
    }, 1000);

    if (mlSuperAdminService.todaySalesData) {
      $scope.todayData = mlSuperAdminService.todaySalesData;
    }

    $scope.$on('todaySalesLoaded', function () {
      $scope.todayData = mlSuperAdminService.todaySalesData;
    });

    $scope.changeSalesAreaShowing = function (areaToShow){
      $scope.saleAreaShowing = areaToShow || '';

      if (!$scope.todayData || !$scope.todayData.companyAtAGlance || !$scope.todayData.companyAtAGlance.dealNumber) {
        return;
      }

      if(areaToShow == '') {
        $scope.todayData.dealNumber = $scope.todayData.companyAtAGlance.dealNumber;
        $scope.todayData.trialNumber = $scope.todayData.companyAtAGlance.trialNumber;
        $scope.todayData.totalSales = $scope.todayData.companyAtAGlance.totalSales;
      }else{
        for (var i = 0; i < $scope.todayData.companyAtAGlance.superarea.length; i++) {
          var compGlance = $scope.todayData.companyAtAGlance.superarea[i];

          if (compGlance.superareaName == areaToShow) {
            $scope.todayData.dealNumber = compGlance.dealNumber;
            $scope.todayData.trialNumber = compGlance.trialNumber;
            $scope.todayData.totalSales = compGlance.totalSales;
          }
        }
      }

      if ($scope.popover) {
        $scope.popover.hide();
      }
    };

    $scope.saleAreaShowing = '';
    $scope.$on('scroll.refreshComplete', function () {
      $scope.changeSalesAreaShowing($scope.saleAreaShowing);
    });


    $scope.checkIfItIsAnyonesFirstDeal = function (collaborators) {
      for (var i = 0; i < collaborators.length; i++) {
        var person = collaborators[i];
        if (person.firstDeal == 'true') {
          return true;
        }
      }
      return false;
    };


    $scope.openSaleOnSwipeRight = function (indexSwiped) {
      $state.go('home.today.select_sale', {index : indexSwiped}, {});
    };

    $scope.doRefresh = function () {
      $scope.fetchTodaySales();
    };


    $ionicPopover.fromTemplateUrl('mlife/today/sales_areas_popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });


    $scope.openPopover = function($event) {
      $scope.popover.show($event);
    };

    //Cleanup the popover when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.popover.remove();
    });

    $scope.model = {
      salesShowLimit : 6,
      more_sales_is_available : true
    };

    $scope.loadMore = function () {
      if ($scope.todayData && $scope.todayData.sales.length) {
        if ($scope.model.salesShowLimit >= $scope.todayData.sales.length) {
          $scope.model.more_sales_is_available = false;
        }else{
          $timeout(function () {
            $scope.model.salesShowLimit += 5;
          }, 1000);
        }
      }
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };


  })

  .controller('SelectedSaleInTodayCtrl', function($scope, $stateParams, $timeout, $state, $localStorage, mlNotifierService,
                                                  mlSuperAdminService, $ionicScrollDelegate, mlFirebaseService) {
    if ($stateParams.index && mlSuperAdminService.todaySalesData.sales.length) {
      $scope.selected_sale = mlSuperAdminService.todaySalesData.sales[$stateParams.index];
      mlFirebaseService.increaseASalesViewCount(angular.copy($scope.selected_sale));
    }

    if ($stateParams.scroll && $stateParams.scroll == 'toComment' ) {
      $timeout(function () {
        $ionicScrollDelegate.scrollBottom(true);
      },100);

    }

    $scope.comment = {
      comment : ''
    };

    $scope.peopleInMentions = [];
    $scope.atPushIds = [];


    $scope.postComment = function () {
      // console.log($scope.peopleInMentions);
      // console.log($scope.atPushIds);

      var comment = angular.copy($scope.comment.comment);
      if (angular.isDefined(comment) && comment.length > 0) {
        $scope.postingComment = true;


        if (!$scope.edittingComment) {
          mlSuperAdminService.postSaleComment($localStorage.user.data.user_email, $scope.selected_sale.todayFeedItemId, comment)
            .then(function (successData) {

              if (successData.code == '200') {
                $scope.selected_sale.comments = successData.data;
                $scope.selected_sale.commentCount = successData.data.length;
                $timeout(function () {
                  $scope.todayData.sales[$stateParams.index] = angular.copy($scope.selected_sale);
                }, 10);
              }
              /*update comment count on firebase*/
              // mlFirebaseService.increaseAPostsViewCount(angular.copy($scope.selected_post), false);
              // mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

            }, function (errorData) {
              $scope.error = errorData;
            })
            .then(function () {
              $scope.postingComment = false;
              $scope.comment.comment = '';

              /*do app notification tagging*/
              if ($scope.atPushIds.length) {
                $scope.selected_sale.id = $stateParams.index;
                $scope.selected_sale.post_title = 'Today';

                mlNotifierService.prepTaggingArray(
                  $scope.atPushIds,
                  $scope.peopleInMentions, angular.copy($scope.selected_sale),
                  comment, 'today', 'tagged_in_sales_comment')
              }


            })
        }
        else{
          mlPostService.editPostComment($scope.edittingComment.comment_ID, comment)
            .then(function (successData) {

                // mlFirebaseService.updateUserLastComment(angular.copy($scope.selected_post), comment)

              },
              function (errorData) {
                $scope.error = errorData;
              })
            .then(function () {
              $scope.postingComment = false;
              $scope.comment.comment = '';
              delete $scope.edittingComment;

            })
        }
      }
    };

  });
