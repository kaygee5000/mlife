/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .controller('mlBaseController', function($rootScope, $scope, $state, mlPostService,
                                           $ionicLoading, mlAuthService,
                                           $localStorage, $window, $timeout,
                                           $ionicHistory, $ionicNavBarDelegate) {

    // $scope.$on('$ionicView.loaded', function(e) {
    // console.log('$ionicView.loaded', $ionicHistory.viewHistory());
    // });
    /*
     * if given group is the selected group, deselect it
     * else, select the given group
     */

    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };

    $scope.$on('categoriesLoaded', function () {
      prepCategories();
    });

    // $scope.categories = [{
    //   name : "Test",
    //   sub_categories : [{
    //     name : "Test 2"
    //   }]},
    //   {
    //     name : "Science",
    //     sub_categories : [{
    //       name : "Mat2"
    //     }]
    //   }];

    function prepCategories() {
      $scope.categories = $localStorage.categories;
      mlPostService.lookupCategoryType = $localStorage.lookupCategoryType;
    }

    if ($localStorage.categories && $localStorage.categories.length) {
      prepCategories();
    }else{
      mlPostService.getPostCategories();
    }


    if(!(mlAuthService.userList && mlAuthService.userList.length)){
      // if ($localStorage.authorization) {
        mlAuthService.getUsersList();
      // }
    }

    $window.openExtUrlInSystemBrowser = function (url) {
      $window.open(url,  '_system', 'location=yes');
      return false;
    };


    $scope.$on('$ionicView.enter', function(e) {
      $ionicNavBarDelegate.showBar(true);
    });



  });
