/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'mlife/base/base.html',
        controller : 'mlBaseController'
        /*views: {
         'home-tab': {
         }
         }*/
      });
    // .state('tabs.home.profile', {
    //     url: '/profile',
    //     templateUrl: 'mlife/base/demo/mat_profile.html',
    //     controller : 'ProfileCtrl'
    // })
    // .state('tabs.home.friends', {
    //     url: '/friends',
    //     templateUrl: 'mlife/base/demo/mat_friends.html',
    //     controller : 'FriendsCtrl'
    // })
    // .state('tabs.home.gallery', {
    //     url: '/gallery',
    //     templateUrl: 'mlife/base/demo/mat_gallery.html',
    //     controller : 'GalleryCtrl'
    // })
    // .state('tabs.home.activity', {
    //     url: '/activity',
    //     templateUrl: 'mlife/base/demo/mat_activity.html',
    //     controller : 'ActivityCtrl'
    // })
  });
