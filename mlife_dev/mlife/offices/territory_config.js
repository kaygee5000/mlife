/**
 * Created by Kaygee on 08/03/2016.
 */

angular.module('mLife')
  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home.territories', {
        url: '/territories',
        views : {
          'territories-tab' : {
            template: '<ion-nav-view name="territories"/>',
            controller : 'TerritorySuperAreaCtrl'
          }
        }
      })

      .state('home.territories.superarea', {
        url: '/superarea',
        views : {
          'territories' : {
            templateUrl: 'mlife/offices/super_areas.html',
            controller : 'TerritorySuperAreaCtrl'
          }
        }
      })

      .state('home.territories.area', {
        url: '/area/:superarea',
        views : {
          'territories' : {
            templateUrl: 'mlife/offices/areas_list.html',
            controller : 'TerritoryAreaCtrl'
          }
        }
      })

      .state('home.territories.office', {
        url: '/office/:area',
        views : {
          'territories' : {
            templateUrl: 'mlife/offices/office_list.html',
            controller : 'TerritoryOfficeCtrl'
          }
        }
      })
      .state('home.territories.employees', {
        url: '/employees/:office/:officeId',
        views : {
          "territories" : {
            templateUrl: 'mlife/offices/offices/office_main.html',
            controller : 'TerritoryEmployeesCtrl'
          }
        }
      })
      .state('home.territories.users', {
        url: '/employees/:office',
        views : {
          "territories" : {
            templateUrl: 'mlife/offices/employees.html',
            controller : 'mlUsersController'
          }
        }
      })
  });
