/**
 * Created by KayGee on 21-Jun-16.
 */


angular.module('mLife')
  .controller('TerritorySuperAreaCtrl', function($scope, $stateParams, $timeout, mlOfficeAreaService, mlFirebaseService, $ionicLoading) {

    $scope.models = {
      searchTerm : '',
      isShowing : false,
      more_posts_is_available : true
    };


    $scope.reloadSuperAreas = function () {
      mlOfficeAreaService.getSuperAreas()
        .then(function (status) {
          $scope.superAreas = mlOfficeAreaService.superAreas;
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        });
    };

    if (mlOfficeAreaService.superAreas && mlOfficeAreaService.superAreas.length){
      $scope.superAreas = mlOfficeAreaService.superAreas;
      $scope.$broadcast('scroll.refreshComplete');
    }else{
      $ionicLoading.show({
        template: 'Getting Super Areas...',
        duration : 30000,
        noBackdrop : true
      });
      $scope.reloadSuperAreas()
    }

  })

  .controller('TerritoryAreaCtrl', function($scope, $stateParams, $timeout, mlOfficeAreaService, mlFirebaseService) {

    $scope.reloadAreas = function () {
      if ($stateParams.superarea) {
        mlOfficeAreaService.getAreasUnderSuperArea($stateParams.superarea)
          .then(function (successData) {
            $scope.areas = successData;
            $scope.$broadcast('scroll.refreshComplete');
          })
          .finally(function () {
            mlFirebaseService.increaseATerritorysViewCount({
              name : $stateParams.superarea,
              type : "Super Area"
            }, 'super_areas')
          })
      }
    };


    $scope.reloadAreas()

  })

  .controller('TerritoryOfficeCtrl', function($scope, $stateParams, $timeout, mlOfficeAreaService, mlFirebaseService) {

    $scope.reloadOffices = function () {
      if ($stateParams.area) {
        mlOfficeAreaService.getOfficesUnderArea($stateParams.area)
          .then(function (successData) {
            $scope.offices = successData;
            $scope.$broadcast('scroll.refreshComplete');
          })
          .finally(function () {
            mlFirebaseService.increaseATerritorysViewCount({
              name : $stateParams.area,
              type : "Area"
            }, 'areas')
          });
      }
    };

    $scope.selectOffice = function (office) {
      mlFirebaseService.increaseATerritorysViewCount(office, 'offices')
    };


    $scope.reloadOffices()

  })
  .controller('TerritoryEmployeesCtrl', function($scope, $stateParams, $timeout, mlWorkDayService, mlFirebaseService, mlSuperAdminService) {

    $scope.notloading = false;

    $scope.reloadEmployees = function () {
      if ($stateParams.office) {

        mlWorkDayService.getEmployeesInAnOffice($stateParams.office, $stateParams.officeId)
          .then(function (successData) {
            $scope.employees = successData;
            $scope.$broadcast('scroll.refreshComplete');
            $scope.notloading = true;

          })
          .finally(function () {
            // mlFirebaseService.increaseATerritorysViewCount({
            //   name : $stateParams.area,
            //   type : "Area"
            // }, 'areas')


            $scope.models = {
              more_users_is_available : true,
              userShowLimit : 8,
              isShowing : false,
              searchTerm : ''
            };
          });


        mlSuperAdminService.fetchRecentTerritorySalesData($stateParams.officeId)
          .then(function (successData) {
            $scope.salesData = successData;
          })
      }
    };

    $scope.tabToShow = {
      employees : true,
      sales : false
      // favorites : false
    };

    $scope.showTab = function (tab) {
      angular.forEach($scope.tabToShow, function (val, key) {
        $scope.tabToShow[key] = false;
      });
      $scope.tabToShow[tab] = true;
    };


    $scope.loadMore = function () {
      if ($scope.employees && $scope.employees.length) {
        if ($scope.models.userShowLimit >= $scope.employees.length) {
          $scope.models.more_users_is_available = false;
        }else{
          $timeout(function () {
            $scope.models.userShowLimit += 15;
          });
        }
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    $scope.reloadEmployees()

  });
