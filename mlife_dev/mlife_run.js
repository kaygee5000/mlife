/**
 * Created by Kaygee on 08/03/2016.
 */
// angular.module is a global place for creating, registering and retrieving Angular modules
angular.module('mLife')

  .run(function($ionicPlatform, $rootScope, $state, $ionicSideMenuDelegate, mlOfficeAreaService,
                $localStorage, $timeout, mlAuthService, mlNotifierService, mlPostService, mlSuperAdminService,
                mlFirebaseService, $ionicTabsDelegate, $cordovaAppVersion, $ionicLoading, $ionicPopup, mlKickOffService ) {

    $localStorage.$default({
      notifications : [],
      user : {}
    });



    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        // StatusBar.styleDefault();
        StatusBar.styleLightContent();
        //   $cordovaStatusbar.overlaysWebView(true);
        //   $cordovaStatusBar.style(1); //Light
        // $cordovaStatusBar.style(2); //Black, transulcent
        // $cordovaStatusBar.style(3); //Black, opaque
      }


      var notificationOpenedCallback = function(jsonData) {
        console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
      };

      // Enable to debug issues.
      // window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});



      window.plugins.OneSignal.init("29b7a8b4-7e51-4fef-86fe-c0f3d0c566f6",
        {googleProjectNumber: "671515376105"}, notificationOpenedCallback);

      // Show an alert box if a notification comes in when the user is in your app.
      // window.plugins.OneSignal.enableInAppAlertNotification(true);

      window.plugins.OneSignal.getIds(function(ids) {
        $localStorage.user.notificationID = ids.userId;
        $localStorage.user.notificationPushToken = ids.pushToken;

        mlNotifierService.storeUserPushId(ids.userId);
      });

      $rootScope.deviceIsReady = true;



      $cordovaAppVersion.getVersionNumber().then(function (version) {
        $rootScope.appVersion = version;
      });

      $cordovaAppVersion.getVersionCode().then(function (build) {
        $rootScope.appBuild = build;
      });


      $cordovaAppVersion.getAppName().then(function (name) {
        $rootScope.appName = name;
      });


      $cordovaAppVersion.getPackageName().then(function (packageName) {
        $rootScope.appPackage = packageName;
      });

      $rootScope.isIOS = ionic.Platform.isIOS();
      $rootScope.isAndroid = ionic.Platform.isAndroid();
    });

    $rootScope.loadInitialPosts = function() {
      // mlPostService.getPosts()
      //     .then(
      //         function (successData) {
      //             // mlPostService.posts = successData;
      //         }, function (errorData) {
      //             $timeout(function () {
      //                 $rootScope.error = errorData;
      //             });
      //         })
      //     .finally(function () {
      //     });
    };

    mlPostService.getPostCategories()
      .then(
        function (successData) {
          // mlPostService.posts = successData;
        }, function (errorData) {
          // $scope.error = errorData;
        })
      .finally(function () {
        /*  <preference name="AutoHideSplashScreen" value="true"/>*/
      });


    //var isWebView = ionic.Platform.isWebView();
    //var isIPad = ionic.Platform.isIPad();
    //var isIOS = ionic.Platform.isIOS();
    //var isAndroid = ionic.Platform.isAndroid();
    //var isWindowsPhone = ionic.Platform.isWindowsPhone();
    //var currentPlatformVersion = ionic.Platform.version();
    //ionic.Platform.exitApp(); // stops the app

    $rootScope.platformSpecificClass = function (iosClass, androidClass, genericClass) {
      if (!ionic.Platform.isAndroid || ionic.Platform.isIPad()) {
        return iosClass
      }else if (ionic.Platform.isAndroid()) {
        return androidClass
      }else{
        return genericClass
      }
    };

    $rootScope.$on('$stateChangeStart ed', function(event, toState, toStateParams) {
      // track the state the user wants to go to;
      /*if the stat is not the login screen*/
      if (toState.name !== 'login') {
        /*check if he has an authorization token*/
        if (angular.isUndefined($localStorage.authorization)){
          mlNotifierService.alert("Auth Check", "Please confirm your identity.");
          /*prevent him if he doesn't have, and redirect to login*/
          event.preventDefault();
          $state.go('login');
        }
      }
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState, toStateParams) {
      if($ionicSideMenuDelegate.isOpenLeft()){
        $ionicSideMenuDelegate.toggleLeft(false);
      }
    });

    $rootScope.$localStorage = $localStorage;
    $rootScope.$state = $state;

    $rootScope.mlifeImageCloud = "https://mogoimagecloud.s3-ap-southeast-1.amazonaws.com/";

    mlKickOffService.initKickOff();

    mlSuperAdminService.fetchData();

    mlOfficeAreaService.getSuperAreas();

    $rootScope.increasePostPageViewCount = function () {
      if ($localStorage.user && $localStorage.user.ID) {
        mlFirebaseService.increasePostPageViewCount();
      }
    };

    $rootScope.increaseTodayPageViewCount = function () {
      mlFirebaseService.increaseTodayPageViewCount();
    };

    $rootScope.increaseOfficesPageViewCount = function () {
      mlFirebaseService.increaseOfficesPageViewCount();
    };

    $rootScope.increaseProfilePageViewCount = function () {
      mlFirebaseService.increaseProfilePageViewCount();
    };

    $rootScope.increaseKickoffPageViewCount = function () {
      mlFirebaseService.increaseKickoffPageViewCount();
    };


    /*this runs once when the app bootstraps*/
    /*if the user is authorized*/

    if (angular.isDefined($localStorage.authorization)) {
      /*put his auth code in the headers*/
      $timeout(function () {
        $.ajaxSetup({
          headers : {
            "ACCESS-TOKEN" :  $localStorage.authorization
          }
        });
        $rootScope.loadInitialPosts();

        /*Setup Notification checker on Firebase*/
        mlFirebaseService.retrieveUserNotifications();


        $state.go('home.posts');

      });

    }else{
      $state.go('login');
    }



    $rootScope.openMenu = function () {
      $ionicSideMenuDelegate.toggleLeft();
    };

    $rootScope.showHomeTab = function() {
      $ionicTabsDelegate.select(0);
    };



    $rootScope.logout = function () {
      window.localStorage.clear();
      $localStorage.$reset({
        notifications : []
      });
      $.ajaxSetup({
        headers : { "ACCESS-TOKEN" :  "" }
      });
      $state.go('login');
    };

  });

