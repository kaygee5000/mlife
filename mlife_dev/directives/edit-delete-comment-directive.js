/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive that allows editting and deleting on comments in News Tab
 */


angular.module('mLife')
    .directive('modifyComment', function($timeout, $rootScope, $ionicActionSheet, mlPostService) {
      return{

        scope :{
          comment : '=modifyComment'
        },

        link :  function(scope, element, attrs) {
          var isBeingEdited = false;

          scope.$on('editComment', function (evt, data) {
            if (data.comment.comment_ID === scope.comment.comment_ID) {
              isBeingEdited = true;
            }
          });

          element.bind('click mousedown', function (evt) {
            var buttonsArray = [], deleteText ='';
            if ($rootScope.$localStorage && $rootScope.$localStorage.user) {
              if($rootScope.$localStorage.user.ID === scope.comment.user_id ){
                if (!isBeingEdited) {
                  buttonsArray.push({ text: '<span class="purple">Edit</span>' });
                }else{
                  buttonsArray.push({ text: '<span class="energized">Cancel Editing</span>' });
                }

                // buttonsArray.push({ text: '<span class="positive">Emoji Sample</span>' });
                deleteText = 'Delete';
              }else{
                // buttonsArray.push({ text: 'Tag Author' })
                evt.preventDefault();
                return;
              }
            }


            var commentTitle = scope.comment.comment_content.substring(0, 50);
            if (scope.comment.comment_content.length > 50) {
              commentTitle += '...';
            }

            var div = $('<div />');
            div.text(emojione.shortnameToUnicode(commentTitle));
            div.minEmoji();

            var hideSheet = $ionicActionSheet.show({
              buttons: buttonsArray,
              titleText: div.html(),
              destructiveText: deleteText,
              cancelText: 'Cancel',
              destructiveButtonClicked: function() {
                $rootScope.$broadcast('deleteComment', {comment : scope.comment });
                return true; /*return true to close Action sheet*/
              },
              cancel: function() {
                hideSheet();
              },
              buttonClicked: function(index) {
                if($rootScope.$localStorage.user && $rootScope.$localStorage.user.ID == scope.comment.user_id ){
                  if (index == 0) {
                    if (!isBeingEdited) {
                      $rootScope.$broadcast('editComment', {comment : scope.comment });
                    }else{
                      isBeingEdited = false;
                      $rootScope.$broadcast('cancelEditComment', {comment : scope.comment });
                    }
                  }
                  // if (index == 1) {
                  //   $rootScope.$broadcast('showEmojiComment');
                  // }
                }
                return true;
              }
            });
          });
        }
      };
    });
