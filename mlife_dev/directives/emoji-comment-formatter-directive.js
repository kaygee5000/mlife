/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive that makes emojis appear in comments
 */


angular.module('mLife')
    .directive('emojifyComment', function($timeout, $rootScope, $ionicActionSheet, mlPostService) {
        return{

            // scope :{
            //   comment : '@emojifyComment'
            // },

            link :  function(scope, element, attrs) {
                // emojione.toShort(str) - native unicode -> shortnames
                // emojione.shortnameToImage(str) - shortname -> images
                // emojione.unicodeToImage(str) - native unicode -> images
                // emojione.toImage(str) - native unicode + shortnames -> images (mixed input)
                //  emojione.shortnameToUnicode(input) - shortname to Unicode

                // $('.txt').minEmoji();
                // scope.covertedcomment = emojione.toImage(scope.comment);

                $timeout(function () {
                    $(element).text( emojione.shortnameToUnicode( $(element).text() ));
                    $(element).minEmoji();
                }, 10);



            }
            // template : '<span ng-bind-html="comment | embed">span>'
        };
    });
