/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('atInWoym', function($timeout, $ionicPopup, mlPostService, mlAuthService) {
    return{

      scope : {
        peopleInMentions : '=peopleInMentions',
        atPushIds : '=atPushIds'
      },

      controller : function ($scope) {


        // $scope.keepKeyboardInFocus = false;

        $scope.filterMWUsers = function (term) {
          $scope.people = [];
          if (term && term.length >= 3) {
            for (var i = 0; i < mlAuthService.userList.length; i++) {
              var user = mlAuthService.userList[i];

              if (user.display_name.toLowerCase().startsWith(term.toLocaleLowerCase())) {
                $scope.people.push(user)
              }
            }
          }
        };


      },

      link :  function(scope, element, attrs) {
        var tribute = new Tribute({
          // collection: [],
          // symbol that starts the lookup
          trigger: '@',

          // element to target for @mentions
          iframe: document.getElementById('menutag'),

          // class added in the flyout menu for active item
          selectClass: 'purple',

          // function called on select that returns the content to insert
          selectTemplate: function (item) {
            console.log('selected', item);
            var email = item.original.user_email;
            var pushId = item.original.push_id;
            //
            if(scope.peopleInMentions.indexOf(email) === -1){
              scope.peopleInMentions.push(email);
            }

            if(pushId !== 'undefined' && scope.atPushIds.indexOf(pushId) === -1){
              scope.atPushIds.push(pushId);
            }
            return '@' + item.original.display_name;
          },

          // template for displaying item in menu
          menuItemTemplate: function (item) {
            return item.string;
          },

          // template for when no match is found (optional),
          // If no template is provided, menu is hidden.
          noMatchTemplate: null,

          // specify an alternative parent container for the menu
          menuContainer: document.getElementById('menutag'),

          // column to search against in the object (accepts function or string)
          lookup: 'display_name',

          // column that contains the content to insert by default
          fillAttr: 'display_name',

          // REQUIRED: array of objects to match
          values: mlAuthService.userList,

          // specify whether a space is required before the trigger character
          requireLeadingSpace: false,

          // specify whether a space is allowed in the middle of mentions
          allowSpaces: false,

          // optionally specify a custom suffix for the replace text
          // (defaults to empty space if undefined)
          replaceTextSuffix: '\n'
        });
        tribute.attach(element);

      }
    };
  });
