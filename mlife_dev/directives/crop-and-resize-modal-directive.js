/**
 * Created by Kaygee on 28/12/2016.
 *
 * Directive for resizing and cropping images in mFeed
 */


angular.module('mLife')
  .directive('cropAndResize', function($rootScope, $timeout, $ionicModal, mlFirebaseService, $state, mlKickOffService, $cordovaToast, UploadImage, $ionicActionSheet, $q, $ionicLoading) {
    return {

      scope: {
        venueSet : '=whatsOnYourMind'
      },

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/kickoff/whats_on_your_mind_modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function() {
          $scope.modal.show();
        };

        $scope.closeModal = function() {
          $scope.modal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          delete $scope.model;
          $scope.modal.remove();
        });


        $scope.data = {
          file : '',
          video : ''
        };


        $scope.model = {
          post_content : '',
          image_url : ''
        };

        $scope.upload = function ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event) {
          console.log($files);
          console.log($file);
          console.log($newFiles);
          console.log($duplicateFiles);
          console.log($invalidFiles);
          console.log($event);
        };


        function uploadImageToS3() {
          var defer = $q.defer();
          var fileName =  new Date().getTime() + '_' +$scope.data.file.name;

          $ionicLoading.show({
            template: 'Uploading image, hang on...',
            duration : 10000
          });

          UploadImage.upload($scope.data.file, 'mlifeimage/' + fileName)
            .then(function (data) {
              $scope.model.image_url = $rootScope.mlifeImageCloud+'mlifeimage/'+fileName;
              defer.resolve(true);
              $ionicLoading.hide();
            }, function () {
              console.log('upload error');
              // $cordovaToast.showShortBottom('Image not uploaded, check network connection' );
              $scope.posting = false;
              $ionicLoading.hide();
              defer.resolve(false);
            });
          return defer.promise;

        }

        function uploadVideoToS3() {
          var defer = $q.defer();
          var fileName =  new Date().getTime() + '_' +$scope.data.video.name;

          $ionicLoading.show({
            template: 'Uploading video, hang on...',
            duration : 10000
          });

          UploadImage.upload($scope.data.video, 'mlifevideo/' + fileName)
            .then(function (data) {
              $scope.model.video_url = $rootScope.mlifeImageCloud+'mlifevideo/'+fileName;
              $ionicLoading.hide();
              defer.resolve(true);
            }, function () {
              // $cordovaToast.showShortBottom('Video not uploaded, check network connection' );
              $scope.posting = false;
              $ionicLoading.hide();
              defer.resolve(false);

            });
          return defer.promise;
        }

        function createPostAndUploadToServer() {
          mlFirebaseService.createAKickOffPost(angular.copy($scope.model));
          $scope.posting = false;
          $ionicLoading.hide();
          $scope.closeModal();

          // $cordovaToast.showShortBottom('Post was successful')
          //   .then(function(success) {
          //     $scope.closeModal();
          //   }, function (error) {
          //     // error
          //   });
        }

        $scope.postToServer = function () {
          if ($scope.model.post_content.length > 1) {

            $scope.posting = true;

            if ($scope.data.file || $scope.data.video) {
              var buttons = [];
              if ($scope.data.file) {
                buttons.push({ text: 'Upload Image' })
              }
              if ($scope.data.video) {
                buttons.push({ text: 'Upload Video' })
              }
              var hideSheet = $ionicActionSheet.show({
                buttons: buttons,
                // destructiveText: 'Delete',
                titleText: 'Select media to upload',
                cancelText: 'Cancel',
                cancel: function() {
                  // add cancel code..
                  $scope.posting = false;
                  hideSheet();
                },
                buttonClicked: function(index) {
                  var uploader;
                  if (index == 0 && $scope.data.file) {
                    uploader = uploadImageToS3()
                  }else if (index == 0 && $scope.data.video) {
                    uploader = uploadVideoToS3()
                  }else if(index == 1){
                    uploader = uploadVideoToS3()
                  }
                  uploader.then(function () {
                    createPostAndUploadToServer()
                  });
                  hideSheet();

                  return true;
                }
              });
            }
            else{
              createPostAndUploadToServer();
            }


            // if ($scope.data.file) {
            //   var fileName =  new Date().getTime() + '_' +$scope.data.file.name;
            //   // UploadImage.upload($scope.data.file, 'placeimage/'+successData._id)
            //   UploadImage.upload($scope.data.file, 'mlifeimage/' + fileName)
            //     .then(function (data) {
            //       $scope.model.image_url = $rootScope.mlifeImageCloud+'mlifeimage/'+fileName;
            //       createPostAndUploadToServer()
            //     }, function () {
            //       console.log('upload error');
            //     });
            // }else{
            //   createPostAndUploadToServer()
            // }
          }else{
            // $cordovaToast.showShortBottom('Please write something in the text area')
          }
        }


      },

      link: function (scope, element, attrs) {
        element.bind('click', function () {
          scope.openModal();
        })
      }
    }
  });
