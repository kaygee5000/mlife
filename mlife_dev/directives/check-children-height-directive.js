/**
 * Created by Kaygee on 21/03/2016.
 *
 * This directive is used in the Today Tab to maintain the same heights on each sale item/child
 */

angular.module('mLife')
  .directive('checkChildrenHeight', function($interval, $compile, mlPostService) {

    function doHeightCheck(element, attrs) {
      $(element).children("div").each(function () {
        var height = 0;
        if (height < $(this).height()) {
          height = $(this).height() + (attrs.height || 1);
        }
        $(element).height(height);
      });
    }

    return{

      scope :{},

      controller : function ($scope) {
        $scope.checkHeightAgain = function ($event, attrs) {
          doHeightCheck($event.target, attrs)
        }
      },

      link :  function(scope, element, attrs) {
        $interval(function () {
          doHeightCheck(element, attrs);
        }, 200, 2);
      }
    };
  });
