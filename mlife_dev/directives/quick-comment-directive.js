/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
    .directive('quickComment', function($timeout, $ionicPopup, mlPostService, $state, mlAuthService, $cordovaToast) {
      return{

        scope :{
          article : '=quickComment'
        },

        link :  function(scope, element, attrs) {
          scope.data = {
            comment : ''
          };
          function showCommentSuccess(commentEntered) {
            // A confirm dialog
            var confirmPopup = $ionicPopup.confirm({
              title: 'Comment Posted Successfully',
              subTitle: scope.article.post_title,
              template: ' <i>\" ' + commentEntered + ' \"</i> ',
              cancelText: 'Close',
              okText: 'View'
            });

            confirmPopup.then(function(res) {
              if(res) {
                $state.go('home.select_post', { id : scope.article.ID, scroll : 'toComment' })
              }
            });
          }

          scope.peopleInMentions = [];
          scope.atPushIds = [];

          element.bind('click', function () {
            var myPopup = $ionicPopup.show({
              template: '<textarea  ng-model="data.comment" ' +
              'at-in-comment="" ' +
              'people-in-mentions="peopleInMentions" ' +
              'at-push-ids="atPushIds" class="padding" rows="2" placeholder="Enter comment"></textarea>',
              title: 'Quick Comment',
              subTitle: scope.article.post_title,
              animation : 'none',
              scope: scope,
              buttons: [
                { text: 'Cancel' },
                {
                  text: '<b>Comment</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                    if (scope.postingComment) {
                      e.preventDefault();
                    }
                    scope.postingComment = true;

                    if (scope.data.comment) {
                      scope.article.comments_count ++ ;
                      myPopup.close();
                      /*do app notification tagging*/


                      mlPostService.createPostComment(scope.article.ID, scope.data.comment)
                          .then(function (successData) {
                            showCommentSuccess(angular.copy(scope.data.comment));

                            if (scope.atPushIds.length) {
                              mlAuthService.pushANotification(scope.atPushIds, scope.article, "You have been tagged in a comment", scope.data.comment)
                                  .then(function (success) {
                                        $cordovaToast.showShortBottom('Tagging was successful' )
                                            .then(function(success) {
                                              // success
                                            }, function (error) {
                                              // error
                                            });

                                      },
                                      function (err) {

                                      })
                            }


                          }, function (errorData) {
                            /*error stuff*/
                            // showCommentSuccess(angular.copy(scope.data.comment))

                          })
                          .then(function () {
                            scope.postingComment = false;
                            scope.data.comment = '';
                          })
                    }
                  }
                }
              ]
            });

          });
        }
      };
    });
