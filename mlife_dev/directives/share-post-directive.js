/**
 * Created by Kaygee on 28/12/2016.
 */


angular.module('mLife')
  .directive('sharePost', function($rootScope, $localStorage, $timeout, $ionicModal, mlFirebaseService, $state, mlKickOffService, $cordovaToast, UploadImage, $ionicActionSheet, $q, $ionicLoading) {
    return {

      scope: {
        sharedPost : '=sharePost'
      },

      controller : function ($scope) {
        $ionicModal.fromTemplateUrl('mlife/kickoff/sharing_post_modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });

        $scope.openModal = function() {
          $scope.modal.show();
        };

        $scope.closeModal = function() {
          $scope.modal.hide();
        };

        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          delete $scope.model;
          $scope.modal.remove();
        });

        $timeout(function () {
          $scope.$localStorage = $rootScope.$localStorage || $localStorage;
        });

        $scope.model = {
          shared_post_content : ''
        };


        function createPostAndUploadToServer() {
          mlKickOffService.shareAPost(angular.copy($scope.model));
          $scope.posting = false;
          $ionicLoading.hide();
          $scope.closeModal();
        }

        $scope.postToServer = function () {
          if ($scope.model.shared_post_content.length > 1) {

            $scope.sharedPost.shared_post_content = $scope.model.shared_post_content;

            delete $scope.sharedPost.checkedIn;

            angular.extend($scope.model, $scope.sharedPost);

            $scope.posting = true;

            createPostAndUploadToServer()

          }else{
            try{
              $cordovaToast.showShortBottom('Please write something in the text area')
            }catch(e){

            }
          }
        }


      },

      link: function (scope, element, attrs) {
        element.bind('click', function () {
          scope.openModal();
        })
      }
    }
  });
