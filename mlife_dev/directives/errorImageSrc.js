
/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive that shows an error image when the src url fails
 */

angular.module('mLife')
  .directive('errSrc', function() {
    return {
      link: function(scope, element, attrs) {
        element.bind('error', function() {
          if (attrs.src != attrs.errSrc) {
            attrs.$set('src', attrs.errSrc);
          }
        });
      }
    }
  });
