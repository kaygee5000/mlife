/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('likeKickOffPost', function($timeout, $compile, mlKickOffService) {
    return{

      scope :{
        article : '=likeKickOffPost'
      },

      link :  function(scope, element, attrs) {
        element.bind('click', function () {
          if (scope.article) {
            scope.article = mlKickOffService.doLikeOnKickOffPost(scope.article)
          }
        });
      },

      replace : true,
      template : '<span  class="button reg-font col action-row-buttons text-center no-border-active"' +
      ' ng-class="{\'purple\' : article.likers[$localStorage.user.ID].liked == true}">' +
      '<i class="icon ion-thumbsup" ng-hide="loadingLikes"></i>' +
      // '<ion-spinner class="spinner-positive text-center"  ng-show="loadingLikes"></ion-spinner>'{{ article.likeCount || 0 }}  +
      '&nbsp;Like'+
      '</span>'

    };
  });
