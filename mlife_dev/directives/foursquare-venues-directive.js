/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive that lists venues around a location
 */


angular.module('mLife')
  .directive('loadCheckInVenues', function($timeout, $http, $ionicModal, $ionicLoading, mlKickOffService, $cordovaGeolocation, $ionicPopup) {
    return{

      scope : {
        venueSet : '=loadCheckInVenues'
      },

      controller : function ($scope) {

      },

      link :  function(scope, element, attrs) {
        element.bind('click', function () {

          $ionicModal.fromTemplateUrl('mlife/kickoff/list_of_checkin_venues_modal.html', {
            scope: scope,
            animation: 'slide-in-up'
          }).then(function(modal) {
            scope.modal = modal;

            scope.models = {
              searchTerm :  '',
              loading : false
            };

            var posOptions = {timeout: 10000, maximumAge : 1000000, enableHighAccuracy: false};


            var watcher =scope.$watch('models.searchTerm', function (newVal, oldVal) {
              if (newVal && newVal.length > 1) {

                $cordovaGeolocation.getCurrentPosition(posOptions)
                  .then(function (position){

                    var link = 'https://api.foursquare.com/v2/venues/search' +
                      '?client_id=XUB3Z3QNTUMYN3GNZDJORWVON5CTXVMHWPRA51TVFPWDWVBM' +
                      '&client_secret=M34BPHLVRZXZRSGHVOVCB0VZBF5EVOW20SQTDB1IL5IUPWXT' +
                      '&v=20130815' +
                      '&ll='+position.coords.latitude +',' + position.coords.longitude +
                      '&query=' + newVal;
                    // var link = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json' +
                    //   '?key=AIzaSyDF1TcWI_vOha3yMH_wmMzBZ7OHBQ-ybIY' +
                    //   '&radius=500' +
                    //   '&location='+position.coords.latitude +',' + position.coords.longitude
                      /*'&keyword=' + newVal*/

                    scope.models.loading = true;

                    $http.get(link)
                      .success(function (data) {
                        console.log('data : ',data);
                        if (data.meta.code == 200) {
                          scope.venues = data.response.venues;
                        }
                        // if (data.status == "OK") {
                        //   scope.venues = data.results
                        // }
                      })
                      .error(function (some, tin) {
                        console.log(some);
                        console.log(tin);
                        console.log("error");
                      })
                      .finally(function () {
                        scope.models.loading = false;
                      })

                  }, function(err) {


                    $ionicPopup.alert({
                      title: 'Location Error',
                      template: '<p class="text-center">'+ err.message +'</p>'
                    });
                  });
              }

            });

            scope.venueTapped = function (venue) {
              $timeout(function () {
                mlKickOffService.venueSet = venue;
                $('#whats-on-your-mind').trigger('click');
                scope.venueSet = venue;
                // scope.closeModal();
                watcher();
                scope.modal.hide();
                scope.modal.remove();
              })
            };


            scope.closeModal = function() {
              if (scope.venueSet && scope.venueSet.name) {
                scope.venueSet = '';
                mlKickOffService.venueSet = '';
              }
              watcher();
              scope.modal.hide();
              scope.modal.remove();
            };

            // Cleanup the modal when we're done with it!
            scope.$on('$destroy', function() {
              delete scope.model;
              delete scope.venueSet;
              watcher();
              scope.modal.remove();
            });

            modal.show();

          });



        });
      }
    };
  });
