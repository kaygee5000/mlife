/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('likeSale', function($timeout, $compile, mlSuperAdminService, $localStorage) {
    return{

      scope :{
        sale : '=likeSale'
      },

      link :  function(scope, element, attrs) {
        element.bind('click', function () {
          if (scope.sale) {
            // scope.loadingLikes = true;
            mlSuperAdminService.postSaleLike($localStorage.user.data.user_email, scope.sale.todayFeedItemId)
              .then(function (successData) {

                if (successData.code == '200') {
                  scope.sale.is_liked = false;

                  scope.sale.likeCount = successData.data.length;
                  if (successData.data.length > 0) {

                    for (var i = 0; i < successData.data.length; i++) {
                      var liker = successData.data[i];
                      if (liker.name == $localStorage.user.data.firstName + "  " + $localStorage.user.data.lastName) {
                        scope.sale.is_liked = true;
                      }
                    }
                  }
                }

              }, function (errorData) {
                console.log('like error', errorData);
              })
              .finally(function () {
                // scope.loadingLikes = false;
              });
          }
        });

        if (scope.sale && scope.sale.likes && scope.sale.likes.length) {
          for (var i = 0; i < scope.sale.likes.length; i++) {
            var liker = scope.sale.likes[i];
            if (liker.name == $localStorage.user.data.firstName + "  " + $localStorage.user.data.lastName) {
              scope.sale.is_liked = true;
            }
          }
        }
      },
      replace : true,
      template : '<span class="subdued" ng-class="{\'purple\' : sale.is_liked == true}">' +
      '<i class="icon ion-thumbsup" ng-hide="loadingLikes"></i>' +
      // '<ion-spinner class="spinner-positive text-center"  ng-show="loadingLikes"></ion-spinner>' +
      '&nbsp;<span ng-show="sale.likeCount != 0">{{ sale.likeCount || "" }}</span> Like<span ng-show="sale.likeCount > 1">s</span>' +
      '</span>'
    };
  });
