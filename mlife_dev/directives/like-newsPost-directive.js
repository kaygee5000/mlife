/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .directive('likeArticle', function($timeout, $compile, mlPostService) {
    return{

      scope :{
        article : '=likeArticle'
      },

      link :  function(scope, element, attrs) {
        element.bind('click', function () {
          if (scope.article) {
            // scope.loadingLikes = true;
            mlPostService.createPostLike(scope.article.ID)
              .then(function (successData) {
                if (successData && successData.code && successData.code == 401) {
                }else{
                  scope.article.user_likes = successData
                }
              }, function (errorData) {
                // scope.error = errorData;
              })
              .finally(function () {
                // scope.loadingLikes = false;
              });

            $timeout(function () {
              if (scope.article.is_liked) {
                scope.article.user_likes --;
              }else{
                scope.article.user_likes ++;
              }
              scope.article.is_liked = !scope.article.is_liked;
            });

          }
        });
      },
      replace : true,
      template : '<span class="subdued col-50 text-center" ng-class="{\'purple\' : article.is_liked == true}">' +
      '<i class="icon ion-thumbsup" ng-hide="loadingLikes"></i>' +
      // '<ion-spinner class="spinner-positive text-center"  ng-show="loadingLikes"></ion-spinner>' +
      '&nbsp;<span ng-show="article.user_likes">{{ article.user_likes }}</span> Like<span ng-show="article.user_likes > 1">s</span>' +
      '</span>'
    };
  });
