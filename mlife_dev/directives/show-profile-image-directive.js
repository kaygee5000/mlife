/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
    .directive('profileImage', function($timeout, $compile, $ionicPopover) {
      return{

        scope : {
          name : '@',
          image : '@profileImage'
        },

        link :  function(scope, element, attrs) {

          $ionicPopover.fromTemplateUrl('mlife/profile/image_popover.html', {
            scope: scope
          }).then(function(popover) {
            scope.popover = popover;
          });


          element.bind('click', function () {
            scope.popover.show(element);
          });

          scope.closePopover = function() {
            scope.popover.hide();
          };

          //Cleanup the popover when we're done with it!
          scope.$on('$destroy', function() {
            scope.popover.remove();
          });

        }
      };
    });
