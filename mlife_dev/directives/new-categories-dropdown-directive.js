
angular.module('mLife')
  .directive('openCategories', function($ionicPopover, $localStorage) {
    return {

      scope : true,

      controller : function ($scope) {
        $ionicPopover.fromTemplateUrl('mlife/base/news_categories_popover.html', {
          scope: $scope
        }).then(function(popover) {
          $scope.popover = popover;
        });


        $scope.openPopover = function($event) {
          $scope.popover.show($event);
        };

        //Cleanup the popover when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.popover.remove();
        });

        $scope.lookupCategory = {
          95 : 'Jorn\'s Corner',
          164 : 'Culture',
          165 : 'Engineering',
          1037 : 'HR',
          1050 : 'IT',
          170 : 'Central Ops',
          000 : 'L&D',
          163 : 'Marketing',
          806 : 'MEST',
          607 : 'Product',
          945 : 'Sales',
          423 : 'SuperAreas'
        };

        $scope.changeNewsCategoryShowing = function (category) {

          $scope.newsCategoryShowing = category;
          if ($scope.popover) {
            $scope.popover.hide();
          }
        }
      },

      link: function(scope, element, attrs) {
        element.bind('click', function(event) {

          scope.openPopover(event);

        });
      }
    }
  });
