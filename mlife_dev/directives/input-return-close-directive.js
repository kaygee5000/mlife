/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive to hide keyboard when the return key is tapped.
 */


angular.module('mLife')
  .directive('inputReturn', function($timeout){
    return {
      restrict: 'E',
      // scope: {
      //   'returnClose': '=',
      //   'onReturn': '&'
      // },
      link: function(scope, element, attr){
        element.bind('keydown', function(e){
          if(e.which === 13){
            if(scope.returnClose){
              element[0].blur();
            }
            if(scope.onReturn){
              $timeout(function(){
                scope.onReturn();
              });
            }
          }
        });
      }
    }
  });
