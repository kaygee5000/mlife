/**
 * Created by Kaygee on 21/03/2016.
 *
 * Directive to show spinners when an image is loading
 */


angular.module('mLife')
  .directive('imageLoader', function($timeout, $compile, $ionicPopover) {
    function hideLoaderImage(element) {
      $timeout(function () {
        var spinner = $(element).parent().find('.img-spinner').find('ion-spinner');
        spinner.hide();
      });
    }
    return{

      link :  function(scope, element, attrs) {
// show loading image
//         $(element).parent().find('.img-spinner').removeClass('hide');


// main image loaded ?
        $(element).on('load', function(){
          // hide/remove the loading image
          hideLoaderImage(element)
        });

        $(element).on('abort', function(){
          // hide/remove the loading image
          hideLoaderImage(element)
        });

        $(element).on('error', function(){
          // hide/remove the loading image
          hideLoaderImage(element)
        });


      }
    };
  });
