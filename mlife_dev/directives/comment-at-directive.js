/**
 * Created by Kaygee on 21/03/2016.
 *
 * This is the directive that controls the Tag list in the comment section of News, Today and mFeed
 */


angular.module('mLife')
  .directive('atInComment', function($timeout, $ionicPopup, mlPostService, mlAuthService) {
    return{

      scope : {
        peopleInMentions : '=peopleInMentions',
        atPushIds : '=atPushIds'
      },

      controller : function ($scope) {


        // $scope.keepKeyboardInFocus = false;

        $scope.filterMWUsers = function (term) {
          $scope.people = [];
          if (term && term.length >= 3) {
            for (var i = 0; i < mlAuthService.userList.length; i++) {
              var user = mlAuthService.userList[i];

              if (user.display_name.toLowerCase().startsWith(term.toLocaleLowerCase())) {
                $scope.people.push(user)
              }
            }
          }
        };


      },

      link :  function(scope, element, attrs) {
        $(element).atwho({
          at: "@",
          data: mlAuthService.userList,
          searchKey: "display_name",
          limit: 10,
          maxLen: 10,
          displayTpl: "<li data-email='${user_email}' data-pushid='${push_id}' data-userid='${ID}'>${display_name}</li>",
          insertTpl: "${atwho-at}${display_name}",
          callbacks : {
            // beforeInsert : function (value, $li) {
            //   console.log(value);
            //   console.log($li);
            //   console.log('atwho-view');
            //   return value;
            // }
            // tplEval: function (tpl, map){
            //     console.log(tpl);
            //     console.log(map);
            // }
          }
        });

        // $(element).on("matched.atwho", function(event, flag, query) {
        //   console.log(event, "matched " + flag + " and the result is " + query);
        // });

        /*
         * inserted.atwho

         Triggered after user choose a popup item in any way. It would receive these arguments:

         atwho event - jQueryEvent : Just a jQuery Event.
         $li - jQuery Object : List Item which have been chosen
         browser event - jQueryEvent : real browser event
         */

        var keyBoardHeight = 0;
        window.addEventListener('native.keyboardshow', function(e){
          keyBoardHeight = e.keyboardHeight;
        });

        $(element).on("reposition.atwho", function(event, offset) {
          // console.log('reposition', event);
          // console.log('offset', offset);
          // console.log('keyBoardHeight', keyBoardHeight);
          // if (ionic.Platform.isIOS()) {
          var atWhoView = $($(this).data('atwho').$el[0]).find('.atwho-view');

          $(atWhoView).css('top', (offset.top - keyBoardHeight + 50) + "px");
          // }
        });


        $(element).on("beforeDestroy.atwho", function() {
          console.log("destroy called");
        });

        $(element).on("inserted.atwho", function(event, liElem, browser) {
          // element.on('blur', function() {
          //   element[0].focus();
          // });

          var email = $(liElem).data('email');
          var pushId = $(liElem).data('pushid');

          if(scope.peopleInMentions.indexOf(email) == -1){
            scope.peopleInMentions.push(email);
          }

          if(pushId !== 'undefined' && scope.atPushIds.indexOf(pushId) == -1){
            scope.atPushIds.push(pushId);
          }
          // $timeout(function () {
          //   element[0].focus();
          // },1000);
          // browser.preventDefault();
          // event.preventDefault();

          return false;
        });

      }
    };
  });
