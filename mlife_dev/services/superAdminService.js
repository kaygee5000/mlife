/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlSuperAdminService', function(mlConstants, $http, $q,
                                           $rootScope, $localStorage, $timeout) {

    this.todaySalesData = [];

    var that = this;

    this.fetchData = function () {
      var defer = $q.defer();

      var limit = 10;
      /*fetch today page on SuperAdmin*/
      $http.post(mlConstants.superAdmin+'?limit=' + limit, {})
        .success(function (successData, status, jqxhr) {
          if (successData.errorCode == "200") {
            that.todaySalesData = successData;
            $rootScope.$broadcast('todaySalesLoaded');
            defer.resolve(successData);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
          defer.resolve(false);
        });
      return defer.promise
    };

    this.fetchEmployeeSaleHistory = function (email) {
      var defer = $q.defer();

      /*fetch sale history on SuperAdmin of an employee*/
      $http.post(mlConstants.salesHistory+'?email=' + email, {})
        .success(function (successData, status, jqxhr) {
          // console.log("success", successData);
          if (successData.code == "200") {
            defer.resolve(successData.data);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
        });
      return defer.promise
    };

    this.postSaleLike = function (email, feedItemId) {
      var defer = $q.defer();

      $http.post(mlConstants.todaySaleLike+'?userEmail=' + email + '&todayFeedItemId=' + feedItemId , {})
        .success(function (successData, status, jqxhr) {
          if (successData.code == "200") {
            defer.resolve(successData);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
        });
      return defer.promise
    };

    this.postSaleComment = function (email, feedItemId, comment) {
      var defer = $q.defer();
      $http.post(mlConstants.todaySaleComment+'?email=' + email + '&userEmail=' + email + '&todayFeedItemId=' + feedItemId + '&comment=' + comment, {})
        .success(function (successData, status, jqxhr) {
          if (successData.code == "200") {
            defer.resolve(successData);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
        });
      return defer.promise
    };

    this.fetchRecentTerritorySalesData = function (territoryId) {
      var defer = $q.defer();

      /*fetch today page by territory on SuperAdmin*/
      $http.post(mlConstants.territorySaleHistory+'?territoryId=' + territoryId, {})
        .success(function (successData, status, jqxhr) {
          if (successData.code == "200") {
            defer.resolve(successData.data);
          }
          defer.reject(false);

        })

        .error(function (errorData) {
          // defer.resolve(sample.data);
        });

      return defer.promise
    };

  });
