/**
 * Created by Kaygee on 21/03/2016.
 */



angular.module('mLife')
  .service('mlNotifierService', function($ionicPopup, $q, $http, $window,  mlConstants, $localStorage, $cordovaToast, mlFirebaseService) {

    var that = this;

    this.alert = function (title, message) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: message
      });
    };

    this.showLoader = function () {
      $('.loader')
        .css('display','block')
        .css('position','fixed')
        .css('z-index',1000)
        .css('top', ($window.innerHeight / 2) - $('.loader').css( "height"))
        .css('left', ($window.innerWidth / 2) - $('.loader').width());
    };

    this.hideLoader = function () {
      $('.loader').css('display','none')
    };

    this.checkForUpdates = function () {
      return $http.get('https://mlife-1287.firebaseapp.com/version.json')
    };


    /*this stores/updates a users push ID in the system*/
    this.storeUserPushId = function (pushID) {
      var defer = $q.defer();

      $.ajax({
        url : mlConstants.storeUserPushId,
        data : {'action' : 'storePushId', 'push_id' : pushID},
        method : "POST",
        dataType : 'json',
        headers : {'ACCESS-TOKEN' : $localStorage.authorization },
        success : function (successData, status, jqxhr) {
          if (successData.code == 200) {
            defer.resolve(successData);
          }
        },
        error : function (errData, err2, err3) {
          defer.reject(errData);
        }
      });


      return defer.promise
    };



    this.prepTaggingArray =
      function (atPushIds, peopleInMentions, selected_post, comment, section_to_go_open_open, tag_type) {

        var postThatWasTagged = {
          comment_posted : comment,
          ID : selected_post.ID || selected_post.$id ||selected_post.id,
          section : section_to_go_open_open,
          type : tag_type,
          post_title : selected_post.post_title || 'mFeed',
          tagger  : {
            firstName : $localStorage.user.data.firstName,
            lastName : $localStorage.user.data.lastName,
            email : $localStorage.user.data.email,
            user_email : $localStorage.user.data.user_email,
            userImageLink : $localStorage.user.data.userImageLink
          }

        };

        for (var i = 0; i < peopleInMentions.length; i++) {
          var peepObj = peopleInMentions[i];
          mlFirebaseService.saveUserTaggingAsNotification(peepObj, postThatWasTagged);
        }

        var phraseToShow = postThatWasTagged.tagger.firstName + " " + postThatWasTagged.tagger.lastName;

        that.pushANotification(atPushIds, postThatWasTagged, comment, phraseToShow)
          .then(function (success) {
            },
            function (err) {

            })

      };

    /*this posts a push notification One Signal Push*/
    this.pushANotification = function (pushIdsArray, selected_post, comment, phraseToShow) {

      var defer = $q.defer();

      // Build the request object
      var req = {
        method: 'POST',
        url: 'https://onesignal.com/api/v1/notifications',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + mlConstants.pushTokenAPI
        },
        data: {
          "app_id": "29b7a8b4-7e51-4fef-86fe-c0f3d0c566f6",
          "ios_badgeType" : "Increase",
          "ios_badgeCount" : "1",
          "contents": {"en": comment},
          "content_available" : true,
          "headings": {"en": phraseToShow}
        }
      };

      if (pushIdsArray && pushIdsArray.length) {
        req.data.include_player_ids = pushIdsArray
      }else{
        req.data.included_segments = ['All']
      }


      // Make the API call
      $http(req).success(function(resp){
        // Handle success
        // console.log("One Signal: Push success", resp);
        defer.resolve(resp)
      }).error(function(error){
        // Handle error
        console.log("One Signal: Push error", error);
        defer.reject(error)

      });



      return defer.promise
    };



  });
