/**
 * Created by Kaygee on 22/03/2016.
 */



angular.module('mLife')
  .service('mlHttpInterceptor', function ($localStorage) {


    function getCurrentTimeTokenHeader(){
      var timeNow = new Date().getTime();
      var timeCreated = timeNow;

      /*get stored token*/
      if ($localStorage["header_token"]) {
        var storedToken = atob($localStorage["header_token"]);
        timeCreated = storedToken.split(':')[1];
      }

      /*check if the token is expired*/
      var minutes = Math.floor((timeNow - timeCreated) / 60000);

      /*token expires after 70 minutes*/
      if (minutes > 60 || minutes == 0) {
        var mwTokenBase = "M3ltW@ter:"+ timeNow;

        /*regenerate token and save in localstorage*/
        $localStorage["header_token"] = btoa(mwTokenBase);
      }
      return  $localStorage["header_token"];
    }


    return{
      'request' : function (config) {
        if (config.url.startsWith("https://admin.meltwater.com")) {
          var token = getCurrentTimeTokenHeader();
          config.headers['token'] = token;
          if (config.params) {
          config.params.token = token;
          }else{
            config.params = {
              token : token
            };
          }
        }
        config.headers['Content-Type'] = "application/json";
        return config;
      },

      'response' : function (config) {

        if (config.data && config.data.code == 400) {
          // console.log("intercepted");
        }

        return config;
      }
    }
  });
