/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlKickOffService', function(mlConstants, $firebaseArray, $q, $rootScope, $localStorage, $http, $timeout) {

    this.venueSet = '';

    var that = this;

    this.lookupComment = {};

    this.fireKickOffPostRef = firebase.database().ref('kickoff_timeline_posts');

    this.selected_posts_comments = [];

    this.lastLoadedTracker = '';

    this.lookupPost = {};

    var kickOffPostsRef = that.fireKickOffPostRef.orderByChild('descSort').limitToFirst(7);

    this.initKickOff = function () {
      this.kickOffTimelineArray = [];

      $firebaseArray(kickOffPostsRef).$loaded()
        .then(function(x) {
          for (var i = 0; i < x.length; i++) {
            var obj = x[i];
            that.lookupPost[obj.$id] = obj;
            that.kickOffTimelineArray.push(obj)
          }

          $rootScope.$broadcast('kickOffPostsLoaded');

        });

    };



    this.loadMoreKickOffPosts = function (lastKnownValue) {
      var defer = $q.defer();

      if (that.lastLoadedTracker === lastKnownValue) {
        defer.reject(false);
        return;
      }

      that.lastLoadedTracker = lastKnownValue;

      var kickOffPostsRef = that.fireKickOffPostRef.orderByChild('descSort').startAt(lastKnownValue).limitToFirst(7);

      var newArray = [];
      $firebaseArray(kickOffPostsRef).$loaded()
        .then(function(x) {
          for (var i = 0; i < x.length; i++) {
            var obj = x[i];
            that.lookupPost[obj.$id] = obj;
            newArray.push(obj)
          }
          defer.resolve(newArray);
        })
        .catch(function(error) {
          defer.reject(false)
        });

      return defer.promise;
    };

    this.createAKickOffPost = function (dataToPost) {

      var defer = $q.defer();
      dataToPost.dateCreated = firebase.database.ServerValue.TIMESTAMP;
      dataToPost.descSort =  -1 * (new Date().getTime());
      dataToPost.owner = {
        email:  $localStorage.user.data.email,
        user_email:  $localStorage.user.data.user_email,
        firstName: $localStorage.user.data.firstName,
        lastName: $localStorage.user.data.lastName
      };
      dataToPost.likeCount = 0;
      dataToPost.likers = {};
      dataToPost.commentCount = 0;
      dataToPost.commenters = {};
      dataToPost.shareCount = 0;
      dataToPost.sharers = {};

      if ($localStorage.user.data.userImageLink) {
        dataToPost.owner.userImageLink = $localStorage.user.data.userImageLink;
      }else{
        // dataToPost.owner.userImageLink = "http://admin.meltwater.com/superadmin/public/image.html?file=user_904_today_1453834543133.jpg";
      }

      $firebaseArray(that.fireKickOffPostRef).$add(dataToPost).then(function(ref) {
        dataToPost.$id = ref.key;

        dataToPost.dateCreated = new Date().getTime(); /*temporal by pass*/
        that.lookupPost[ref.key] = dataToPost;
        that.kickOffTimelineArray.unshift(dataToPost);

        $rootScope.$broadcast('kickOffPostsLoaded');

        defer.resolve(dataToPost);
        // var empEmailKey = $localStorage.user.data.user_email.replace(/\./g, "_");
        // empEmailKey = empEmailKey.split('@')[0];

        // that.kickOffTimelineArray.$indexFor(newPostKey); // returns location in the array
      });

// rs
//       wc
//       sc
//       West Hills Mall



      // // Write the new post's data simultaneously in the posts list and the user's post list.
      // var updates = {};
      // updates['/kickoff_timeline_posts/' + newPostKey] = dataToPost;
      // updates['/kickoff_user_posts/' + empEmailKey + '/' + newPostKey] = dataToPost;
      //
      // /* This function saves a the post by an employee being tagged*/
      // return fireBaseRef.update(updates);
      return defer.promise;

    };

    this.updateKickoffPost = function (dataToPost) {
      $firebaseArray(that.fireKickOffPostRef).$add(dataToPost).then(function(ref) {
        that.lookupPost[ref.key] = dataToPost;
        $rootScope.$broadcast('kickOffPostsLoaded');
      });
    };

    this.doLikeOnKickOffPost = function (post) {
      if (post) {
        var uid = $localStorage.user.ID;

        if (!post.likers) {
          post.likers = {};
          post.likeCount = 0;
        }
        if (post.likers[uid]) {
          post.likeCount--;
          post.likers[uid] = null;
        } else {
          post.likeCount++;
          post.likers[uid] = {
            email:  $localStorage.user.data.email || $localStorage.user.data.user_email,
            name: $localStorage.user.data.firstName + ' ' + $localStorage.user.data.lastName,
            id : uid,
            liked : true
          };
        }
        $firebaseArray(that.fireKickOffPostRef).$save(post);
      }
      return post;
    };


    this.shareAPost = function (dataToPost) {

      var defer = $q.defer();

      delete dataToPost.$$hashKey;

      dataToPost.dateCreated = firebase.database.ServerValue.TIMESTAMP;
      dataToPost.descSort =  -1 * (new Date().getTime());

      dataToPost.originalDateCreated = dataToPost.dateCreated;
      dataToPost.sharer = {
        email:  $localStorage.user.data.email,
        user_email:  $localStorage.user.data.user_email,
        firstName: $localStorage.user.data.firstName,
        lastName: $localStorage.user.data.lastName
      };
      dataToPost.likeCount = 0;
      dataToPost.likers = {};
      dataToPost.commentCount = 0;
      dataToPost.commenters = {};
      dataToPost.shareCount = 0;
      dataToPost.sharers = {};

      if ($localStorage.user.data.userImageLink) {
        dataToPost.sharer.userImageLink = $localStorage.user.data.userImageLink;
      }else{
        // dataToPost.sharer.userImageLink = "http://admin.meltwater.com/superadmin/public/image.html?file=user_904_today_1453834543133.jpg";
      }

      dataToPost = that.doShareOnKickOffPost(dataToPost);


      // Write the new post's data simultaneously in the posts list and the user's post list.
      // var updates = {};
      // updates['kickoff_timeline_posts/' + newPostKey] = dataToPost;
      // updates['kickoff_user_posts/' + empEmailKey + '/' + newPostKey] = dataToPost;

      if (!dataToPost.parent_id) {
        dataToPost.parent_id = dataToPost.$id;
      }

      $firebaseArray(that.fireKickOffPostRef).$add(dataToPost).then(function(ref) {
        dataToPost.$id = ref.key;
        dataToPost.dateCreated = new Date().getTime(); /*temporal by pass*/
        that.lookupPost[ref.key] = dataToPost;
        that.kickOffTimelineArray.unshift(dataToPost);

        $rootScope.$broadcast('kickOffPostsLoaded');

        defer.resolve(true);
        // var empEmailKey = $localStorage.user.data.user_email.replace(/\./g, "_");
        // empEmailKey = empEmailKey.split('@')[0];

        // that.kickOffTimelineArray.$indexFor(newPostKey); // returns location in the array
      });

      return defer.promise;
    };


    this.doShareOnKickOffPost = function (post) {
      if (post) {
        var uid = $localStorage.user.ID;

        if (!post.sharers) {
          post.sharers = {};
          post.shareCount = 0;
        }

        post.shareCount++;
        post.sharers[uid] = true;

      }

      return post;
    };


    this.filterComments = function () {

      if (this.selected_posts_comments.length) {
        this.lookupComment = {};

        for (var i = 0; i < this.selected_posts_comments.length; i++) {
          var commentItem = this.selected_posts_comments[i];

          this.lookupComment[commentItem.id] = commentItem;
        }
      }

      $rootScope.$broadcast('kickOffCommentLoaded');

    };

  });
