/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlOfficeAreaService', function(mlConstants, $http, $q,
                                           $rootScope, $ionicLoading, $timeout) {

    var that = this;


    this.getSuperAreas = function () {
      var defer = $q.defer();

      var ter = {
        "errorMessage": null,
        "territories": [
          {
            "id": 1535,
            "name": "Americas",
            "active": true,
            "type": "Super Area"
          },
          {
            "id": 1543,
            "name": "APAC",
            "active": true,
            "type": "Super Area"
          },
          {
            "id": 1731,
            "name": "EMEA",
            "active": true,
            "type": "Super Area"
          }
        ],
        "errorCode": 200
      };

      function pushExtraSuperAreas() {
        that.superAreas.push({
            "id": "1541CO",
            "name": "CentralOps",
            "active": true,
            "type": "Super Area"
          },
          {
            "id": "1539ENG",
            "name": "Engineering",
            "active": true,
            "type": "Super Area"
          })
      }
      /*fetch employee info on WorkDay*/
      $http.post(mlConstants.allTerritories+'?type=superarea&includeDeactive=false', {cache : true})
        .success(function (successData, status, jqxhr) {

          if (successData.errorCode == "200") {
            that.superAreas = successData.territories;
            pushExtraSuperAreas();
            defer.resolve(true);
          }else{
            defer.reject(false);
          }
        })
        .error(function (errorData) {
          that.superAreas = ter.territories;
          pushExtraSuperAreas();

          defer.resolve(true);
        })
        .finally(function () {
        });

      return defer.promise
    };


    this.getAreasUnderSuperArea = function (superArea) {
      var defer = $q.defer();

      $ionicLoading.show({
        template: 'Getting areas in ' + superArea + '...',
        duration : 30000,
        noBackdrop : true
      });

      var ter = {
        "errorMessage": null,
        "errorCode": 200,
        "areas": [
          {
            "id": 1713,
            "name": "Admin APAC",
            "active": true,
            "type": "Area"
          },
          {
            "id": 1583,
            "name": "APAC Enterprise",
            "active": true,
            "type": "Area"
          },
          {
            "id": 1585,
            "name": "Australia",
            "active": true,
            "type": "Area"
          },
          {
            "id": 1587,
            "name": "China",
            "active": true,
            "type": "Area"
          },
          {
            "id": 1589,
            "name": "Japan & SEA",
            "active": true,
            "type": "Area"
          }
        ]
      };

      $http.post(mlConstants.allAreasBySuperArea+'?superareaName='+superArea+'&excludeParent=true&includeDeactive=false', {cache : true})
        .success(function (successData, status, jqxhr) {

          if (successData.errorCode == "200") {
            defer.resolve(successData.areas);
          }else{
            defer.reject(false);
          }
        })
        .error(function (errorData) {
          defer.resolve(ter.areas);
        })
        .finally(function () {
          $ionicLoading.hide();
        });

      return defer.promise
    };


    this.getOfficesUnderArea = function (area) {
      var defer = $q.defer();

      $ionicLoading.show({
        template: 'Getting offices in ' + area + '...',
        duration : 30000,
        noBackdrop : true
      });

      var ter = {
        "errorMessage": null,
        "offices": [
          {
            "id": 1611,
            "name": "APAC Australia Admin",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1211,
            "name": "CA Brisbane",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1251,
            "name": "CA Melbourne",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1477,
            "name": "CA Perth",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1281,
            "name": "CA Sydney",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1393,
            "name": "CS Melbourne",
            "active": true,
            "type": "Sales Office"
          },
          {
            "id": 1411,
            "name": "CS Sydney",
            "active": true,
            "type": "Sales Office"
          }
        ],
        "errorCode": 200
      };

      $http.post(mlConstants.allOfficesByArea+'?areaName='+area+'&excludeParent=true&includeDeactive=false', {cache : true})
        .success(function (successData, status, jqxhr) {

          if (successData.errorCode == "200") {
            defer.resolve(successData.offices);
          }else{
            defer.reject(false);
          }
        })
        .error(function (errorData) {
          defer.resolve(ter.offices);
        })
        .finally(function () {
          $ionicLoading.hide();
        });

      return defer.promise
    };



  });
