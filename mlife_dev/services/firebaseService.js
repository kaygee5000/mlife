/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlFirebaseService', function(mlConstants, $localStorage, $q,
                                         $rootScope, $ionicLoading, $timeout, mlKickOffService) {

    var that = this;


    var fireBaseRef = firebase.database().ref();

    this.increasePostPageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the post page has been viewed*/
      fireBaseRef.child('post/page_views/' + dateKey).transaction(function(currentPostObj){

        if (currentPostObj && currentPostObj.views) {

          currentPostObj.views ++;

          currentPostObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentPostObj = {};

          currentPostObj.views = 1;

          currentPostObj.users = {};

          currentPostObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentPostObj;
      });

    };


    /*params :
     * postObj is the post/article object
     * increasePageReads is to update the post when a comment or a like occurs,
     * without increasing read count*/
    this.increaseAPostsViewCount = function (postObj, increasePageReads) {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();



      /* This function increases the number of times a post has been viewed/read by day/date */
      fireBaseRef.child('post/posts_by_date/'+dateKey+'/'+postObj.ID).transaction(function(currentPostArticleObject){
        if (currentPostArticleObject) {

          currentPostArticleObject.comments = postObj.comments_count;

          currentPostArticleObject.likes = postObj.user_likes;

          if (increasePageReads) {
            if (currentPostArticleObject.reads) {
              currentPostArticleObject.reads ++;
            }else{
              currentPostArticleObject.reads = 1;
            }
          }


          currentPostArticleObject.title = postObj.post_title;
          currentPostArticleObject.post_image = postObj.image;

          currentPostArticleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentPostArticleObject = {};

          currentPostArticleObject.comments = postObj.comments_count;

          currentPostArticleObject.likes = postObj.user_likes;

          if (increasePageReads) {
            currentPostArticleObject.reads = 1;
          }

          currentPostArticleObject.title = postObj.post_title;
          currentPostArticleObject.post_image = postObj.image;


          currentPostArticleObject.users = {};
          currentPostArticleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentPostArticleObject;

      });


      /* This function increases the number of times a post has been viewed/read with post content */
      fireBaseRef.child('post/posts_viewed/'+postObj.ID).transaction(function(currentPostArticleObject){
        if (currentPostArticleObject) {

          currentPostArticleObject.comments = postObj.comments_count;

          currentPostArticleObject.likes = postObj.user_likes;

          currentPostArticleObject.content = postObj.post_content;

          if (increasePageReads) {
            if (currentPostArticleObject.reads) {
              currentPostArticleObject.reads ++;
            }else{
              currentPostArticleObject.reads = 1;
            }
          }


          currentPostArticleObject.title = postObj.post_title;
          currentPostArticleObject.post_image = postObj.image;


          currentPostArticleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentPostArticleObject = {};


          currentPostArticleObject.content = postObj.post_content;

          currentPostArticleObject.comments = postObj.comments_count;

          currentPostArticleObject.likes = postObj.user_likes;

          if (increasePageReads) {
            currentPostArticleObject.reads = 1;
          }

          currentPostArticleObject.title = postObj.post_title;
          currentPostArticleObject.post_image = postObj.image;


          currentPostArticleObject.users = {};
          currentPostArticleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentPostArticleObject;

      });
    };



    this.increaseTodayPageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the today page has been viewed*/
      fireBaseRef.child('today/page_views/' + dateKey).transaction(function(currentTodayObj){

        if (currentTodayObj && currentTodayObj.views) {

          currentTodayObj.views ++;

          currentTodayObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentTodayObj = {};

          currentTodayObj.views = 1;

          currentTodayObj.users = {};

          currentTodayObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentTodayObj;
      });

      /*fireBaseRef.child('posts/'+postObj.id).transaction(function(currentValue){
       return (currentValue || 0) + 1;
       });*/

    };


    /*params :
     * saleObject is the post/article object
     */
    this.increaseASalesViewCount = function (saleObject) {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();



      /* This function increases the number of times a post has been viewed/read by day/date */
      fireBaseRef.child('today/sale_by_date/'+dateKey+'/'+saleObject.todayFeedItemId).transaction(function(currentSaleObject){
        if (currentSaleObject) {

          /*
           * "time": "08:15",
           "accountName": "The University of Tennessee Medical Center",
           "comments": null,
           "likes": null,
           "products": [
           "Fairhair"
           ],
           "salesValue": "1,500",
           "salesTerm": "12",
           "commentCount": "0",
           "likeCount": "0",
           "salesCollaborators": [
           {
           "userName": "Stephanie Alvarez",
           "userId": "11059",
           "division": "CS",
           "salesOffice": "CS Miami",
           "userEmail": "stephanie.alvarez@meltwater.com",
           "firstDeal": "true",
           "userImageLink": "http://admin.meltwater.com/superadmin/public/image.html?file=user_11059_today_1447210077481.jpg",
           "userProfileLink": "http://admin.meltwater.com/superadmin/userprofile/detail.html?id=11059"
           }
           ],
           "todayFeedItemId": "844815",
           "orderLink": "http://admin.meltwater.com/superadmin/orderfeed/detail.html?id=683709",
           "accountLink": "http://admin.meltwater.com/superadmin/accountfeed/detail.html?id=76233"
           */

          currentSaleObject.accountName = saleObject.accountName;
          currentSaleObject.products = saleObject.products;
          currentSaleObject.salesCollaborators = saleObject.salesCollaborators;

          currentSaleObject.salesValue = saleObject.salesValue;
          currentSaleObject.salesTerm = saleObject.salesTerm;
          currentSaleObject.comments = saleObject.commentCount;
          currentSaleObject.likes = saleObject.likeCount;


          if (currentSaleObject.views) {
            currentSaleObject.views ++;
          }else{
            currentSaleObject.views = 1;
          }

          currentSaleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentSaleObject = {};

          currentSaleObject.accountName = saleObject.accountName;
          currentSaleObject.products = saleObject.products;
          currentSaleObject.salesCollaborators = saleObject.salesCollaborators;

          currentSaleObject.salesValue = saleObject.salesValue;
          currentSaleObject.salesTerm = saleObject.salesTerm;
          currentSaleObject.comments = saleObject.commentCount;
          currentSaleObject.likes = saleObject.likeCount;

          currentSaleObject.views = 1;

          currentSaleObject.users = {};
          currentSaleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentSaleObject;

      });


      /* This function increases the number of times a post has been viewed/read with post content */
      fireBaseRef.child('today/sales_viewed/'+saleObject.todayFeedItemId).transaction(function(currentSaleObject){
        if (currentSaleObject) {

          currentSaleObject.accountName = saleObject.accountName;
          currentSaleObject.products = saleObject.products;
          currentSaleObject.salesCollaborators = saleObject.salesCollaborators;

          currentSaleObject.salesValue = saleObject.salesValue;
          currentSaleObject.salesTerm = saleObject.salesTerm;
          currentSaleObject.comments = saleObject.commentCount;
          currentSaleObject.likes = saleObject.likeCount;


          if (currentSaleObject.views) {
            currentSaleObject.views ++;
          }else{
            currentSaleObject.views = 1;
          }

          currentSaleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentSaleObject = {};



          currentSaleObject.accountName = saleObject.accountName;
          currentSaleObject.products = saleObject.products;
          currentSaleObject.salesCollaborators = saleObject.salesCollaborators;

          currentSaleObject.salesValue = saleObject.salesValue;
          currentSaleObject.salesTerm = saleObject.salesTerm;
          currentSaleObject.comments = saleObject.commentCount;
          currentSaleObject.likes = saleObject.likeCount;


          if (currentSaleObject.views) {
            currentSaleObject.views ++;
          }else{
            currentSaleObject.views = 1;
          }

          currentSaleObject.users = {};
          currentSaleObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentSaleObject;

      });
    };



    this.increaseOfficesPageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the offices page has been viewed*/
      fireBaseRef.child('offices/page_views/' + dateKey).transaction(function(currentOfficeObj){

        if (currentOfficeObj && currentOfficeObj.views) {

          currentOfficeObj.views ++;

          currentOfficeObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentOfficeObj = {};

          currentOfficeObj.views = 1;

          currentOfficeObj.users = {};

          currentOfficeObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentOfficeObj;
      });

    };

    /*params :
     * territoryObject is the territory object
     */
    this.increaseATerritorysViewCount = function (territoryObject, territoryTypeKey) {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();

      /* This function increases the number of times a post has been viewed/read by day/date */
      fireBaseRef.child(territoryTypeKey + '/' + territoryTypeKey + '_by_date/'+dateKey+'/'+territoryObject.name).transaction(function(currentTerritoryObject){
        if (currentTerritoryObject) {

          /* {
           "name": "Americas",
           "active": true,
           "type": "Super Area"
           "type": "Area"
           "type": "Sales Office"

           }*/

          currentTerritoryObject.name = territoryObject.name;
          currentTerritoryObject.type = territoryObject.type;


          if (currentTerritoryObject.views) {
            currentTerritoryObject.views ++;
          }else{
            currentTerritoryObject.views = 1;
          }

          currentTerritoryObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentTerritoryObject = {};


          currentTerritoryObject.name = territoryObject.name;
          currentTerritoryObject.type = territoryObject.type;

          currentTerritoryObject.views = 1;

          currentTerritoryObject.users = {};
          currentTerritoryObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentTerritoryObject;

      });


      /* This function increases the number of times a post has been viewed/read with post content */
      fireBaseRef.child(territoryTypeKey + '/' + territoryTypeKey + '_viewed/'+territoryObject.name).transaction(function(currentTerritoryObject){
        if (currentTerritoryObject) {

          currentTerritoryObject.name = territoryObject.name;
          currentTerritoryObject.type = territoryObject.type;


          if (currentTerritoryObject.views) {
            currentTerritoryObject.views ++;
          }else{
            currentTerritoryObject.views = 1;
          }

          currentTerritoryObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentTerritoryObject = {};


          currentTerritoryObject.name = territoryObject.name;
          currentTerritoryObject.type = territoryObject.type;

          currentTerritoryObject.views = 1;

          currentTerritoryObject.users = {};
          currentTerritoryObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentTerritoryObject;

      });
    };



    this.increaseProfilePageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the profile page has been viewed*/
      fireBaseRef.child('profile/page_views/' + dateKey).transaction(function(currentProfileObj){

        if (currentProfileObj && currentProfileObj.views) {

          currentProfileObj.views ++;

          currentProfileObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }else{
          currentProfileObj = {};

          currentProfileObj.views = 1;

          currentProfileObj.users = {};

          currentProfileObj.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentProfileObj;
      });

    };

    this.increaseKickoffPageViewCount = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that increases the number of times the profile page has been viewed*/
      /* fireBaseRef.child('profile/page_views/' + dateKey).transaction(function(currentProfileObj){

       if (currentProfileObj && currentProfileObj.views) {

       currentProfileObj.views ++;

       currentProfileObj.users[ $localStorage.user.ID ] = $localStorage.user.data

       }else{
       currentProfileObj = {};

       currentProfileObj.views = 1;

       currentProfileObj.users = {};

       currentProfileObj.users[ $localStorage.user.ID ] = $localStorage.user.data

       }

       return currentProfileObj;
       });*/

    };


    this.increaseAnEmployeesViewCount = function (employeeObj) {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      var empEmailKey = employeeObj.email.replace(/\./g, "_");
      empEmailKey = empEmailKey.split('@')[0];

      delete employeeObj.errorCode;
      delete employeeObj.errorMessage;
      delete employeeObj.linkedin;
      delete employeeObj.phone;
      delete employeeObj.skype;
      delete employeeObj.twitter;

      /* This function increases the number of times a post has been viewed/read by day/date */
      fireBaseRef.child('profile/profile_by_date/'+dateKey+'/'+empEmailKey).transaction(function(currentEmpObject){
        if (currentEmpObject) {

          /*
           {
           active: "false",
           area: "Japan & SEA",
           email: "aaron.soh@meltwater.com",
           errorCode: "200",
           errorMessage: "",
           firstName: "Aaron",
           lastName: "Soh",
           linkedin: null,
           manager: "Weldon Fung",
           phone: null,
           salesOffice: "CA Singapore",
           skype: "aaron.soh-meltwater",
           superArea: "APAC",
           title: "Sales Manager-SGP",
           twitter: null
           }
           */

          currentEmpObject = angular.copy(employeeObj);

          if (currentEmpObject.views) {
            currentEmpObject.views ++;
          }else{
            currentEmpObject.views = 1;
          }

          if (currentEmpObject.users) {
            currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data
          }else{
            currentEmpObject.users = {};
            currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data
          }

        }else{
          currentEmpObject = angular.copy(employeeObj);

          currentEmpObject.views = 1;

          currentEmpObject.users = {};
          currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentEmpObject;

      });


      /* This function increases the number of times a post has been viewed/read with post content */
      fireBaseRef.child('profile/users_viewed/'+empEmailKey).transaction(function(currentEmpObject){
        if (currentEmpObject) {

          currentEmpObject = angular.copy(employeeObj);

          if (currentEmpObject.views) {
            currentEmpObject.views ++;
          }else{
            currentEmpObject.views = 1;
          }

          if (currentEmpObject.users) {
            currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data
          }else{
            currentEmpObject.users = {};
            currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data
          }



        }else{
          currentEmpObject = angular.copy(employeeObj);

          currentEmpObject.views = 1;

          if (currentEmpObject.views) {
            currentEmpObject.views ++;
          }else{
            currentEmpObject.views = 1;
          }

          currentEmpObject.users = {};
          currentEmpObject.users[ $localStorage.user.ID ] = $localStorage.user.data

        }

        return currentEmpObject;

      });
    };



    this.updateUserLogin = function () {

      var date = new Date();

      var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that updates the user and his login times*/
      fireBaseRef.child('users/user_data/' + $localStorage.user.ID).transaction(function(currentProfileObj){

        if (currentProfileObj) {

          angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.last_login = date;

        }else{
          currentProfileObj = {};

          angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.last_login = date;

        }

        return currentProfileObj;
      });

      /*This is the function that updates logins by date*/
      fireBaseRef.child('users/logins_by_date/' + dateKey).transaction(function(usersObject){

        if (usersObject) {

          if (usersObject.logins) {
            usersObject.logins ++;
          }else{
            usersObject.logins = 1;
          }
          usersObject.users[$localStorage.user.ID] = $localStorage.user.data;


        }else{
          usersObject = {
            logins : 1,
            users : {}
          };

          usersObject.users[$localStorage.user.ID] = $localStorage.user.data;
        }

        return usersObject;
      });

    };

    this.updateUserLastLike = function (postArticleObj) {

      var date = new Date();

      // var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that updates the user and his login times*/
      fireBaseRef.child('users/' + $localStorage.user.ID).transaction(function(currentProfileObj){

        if (currentProfileObj) {

          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.most_recent_activity = date;
          currentProfileObj.recent_post_like = {
            title : postArticleObj.post_title,
            postId : postArticleObj.ID,
            like_date : date,
            post_image : postArticleObj.image
          };

        }else{
          currentProfileObj = {};

          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);
          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.most_recent_activity = date;

          currentProfileObj.recent_post_like = {
            title : postArticleObj.post_title,
            postId : postArticleObj.ID,
            like_date : date,
            post_image : postArticleObj.image
          };
        }

        return currentProfileObj;
      });

    };

    this.updateUserLastComment = function (postArticleObj, userComment) {

      var date = new Date();

      // var dateKey = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();


      /*This is the function that updates the user and his login times*/
      fireBaseRef.child('users/' + $localStorage.user.ID).transaction(function(currentProfileObj){

        if (currentProfileObj) {

          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.most_recent_activity = date;
          currentProfileObj.recent_post_comment = {
            title : postArticleObj.post_title,
            postId : postArticleObj.ID,
            comment_date : date,
            post_image : postArticleObj.image,
            commentPosted : userComment
          };

        }else{
          currentProfileObj = {};

          currentProfileObj = angular.merge(currentProfileObj, $localStorage.user.data);

          currentProfileObj.most_recent_activity = date;
          currentProfileObj.recent_post_comment = {
            title : postArticleObj.post_title,
            postId : postArticleObj.ID,
            comment_date : date,
            post_image : postArticleObj.image,
            commentPosted : userComment
          };

        }

        return currentProfileObj;
      });

    };


    this.saveUserTaggingAsNotification = function (employeeToBeTaggedObj, tagObject) {

      var date = new Date();

      tagObject.dateCreated = date.getDate() + '-' + (date.getMonth() + 1) + "-" + date.getFullYear();

      var empEmailKey = employeeToBeTaggedObj.replace(/\./g, "_");
      empEmailKey = empEmailKey.split('@')[0];

      /* This function saves a notification for an employee being tagged*/
      fireBaseRef.child('notifications/'+empEmailKey).push()
        .set(tagObject)

    };


    this.retrieveUserNotifications = function () {

      var empEmailKey = ($localStorage.user.data.email || $localStorage.user.data.user_email).replace(/\./g, "_");
      empEmailKey = empEmailKey.split('@')[0];

      // empEmailKey = 'abhinav_bhardwaj';

      var notificationRef = fireBaseRef.child('notifications/' + empEmailKey).orderByKey();

      notificationRef.on('value', function(snapshot) {
        angular.forEach(snapshot.val(), function (notificationItem) {
          $localStorage.notifications.unshift(notificationItem);
        });


        /* This function removes all the notifications after it has been read*/
        fireBaseRef.child('notifications/'+empEmailKey).remove();
      });

    };




    this.doCommentOnKickOffPost = function (postId, comment) {

      var defer = $q.defer();

      /* This function saves a comment in the comment table and updates the count on the post table*/
      fireBaseRef.child('/kickoff_posts_comments/' + postId).push().set(comment)
        .then(function(){
          fireBaseRef.child('kickoff_timeline_posts/'+postId).transaction(function(post) {

            if (post){
              var uid = $localStorage.user.ID;

              if (!post.commenters) {
                post.commenters = {};
                post.commentCount = 0;
              }
              post.commentCount++;
              post.commenters[uid] = true;
            }

            defer.resolve(post);

            return post;

          });
        });

      return defer.promise
    };

    this.retrieveAPostsComments = function (postId) {

      var kickOffPostsRef = fireBaseRef.child('kickoff_posts_comments/' + postId).orderByKey();

      kickOffPostsRef.once('value', function(snapshot) {
        mlKickOffService.selected_posts_comments = [];

        angular.forEach(snapshot.val(), function (commentItem) {
          mlKickOffService.selected_posts_comments.unshift(commentItem);
        });

        mlKickOffService.filterComments();

      });

    };

  });
