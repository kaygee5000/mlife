/**
 * Created by KayGee on 14-Jul-16.
 */

angular.module('mLife')
    .service("UploadImage", ['mlConstants','$q', 'Upload', 'cloudinary',  function (mlConstants, $q, $upload, cloudinary) {


        // Configure The S3 Object
        AWS.config.update({ accessKeyId: mlConstants.aws_access_key, secretAccessKey: mlConstants.aws_secret_access_key });
        AWS.config.region = mlConstants.aws_bucket_region;
        var bucket = new AWS.S3({ params: { Bucket: mlConstants.aws_image_bucket } });

        this.upload = function(file, nameStructure){
            var defer = $q.defer();

            var params = {
                Key : nameStructure || file.name,
                ContentType : file.type|| 'image/jpg',
                Body : file,
                ServerSideEncryption : 'AES256' };

            bucket.putObject(params, function(err, data) {
                if(err) {
                    // There Was An Error With Your S3 Config
                    console.log("error", err.message);
                    defer.reject(false);
                    return false;
                }
                else {
                    // Success!
                    defer.resolve(data);
                }
            })
           /* .on('httpUploadProgress',function(progress) {
                // Log Progress Information
                console.log(Math.round(progress.loaded / progress.total * 100) + '% done');
            })*/;

          // console.log('file', file);
          // console.log('cloudinary.config().upload_preset', cloudinary.config().upload_preset);
          // console.log('cloudinary.config().cloud_name', cloudinary.config().cloud_name);
          // console.log('nameStructure', nameStructure);


         /* $upload.upload({
                  url: "https://api.cloudinary.com/v1_1/" + cloudinary.config().cloud_name + "/upload",
                  data: {
                    upload_preset: cloudinary.config().upload_preset,
                    tags: 'mfeed',
                    context: 'photo=' + nameStructure,
                    file: file
                  }
                }).progress(function (e) {
                  // file.progress = Math.round((e.loaded * 100.0) / e.total);
                  // file.status = "Uploading... " + file.progress + "%";
                }).success(function (data, status, headers, config) {
                  // $rootScope.photos = $rootScope.photos || [];
                  // data.context = {custom: {photo: $scope.title}};
                  // file.result = data;
                  // $rootScope.photos.push(data);
                 defer.resolve(data)
                }).error(function (data, status, headers, config) {
                  // file.result = data;
                });
*/

            return defer.promise;
        };

    }]);


