/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlPostService', function(mlConstants, $http, $q, $rootScope, $localStorage, $timeout) {

    this.lookupPost = {};
    this.posts = [];

    this.favouritePosts = [];

    this.categoryPosts = [];
    this.lookupCategoryPost = {};
    this.lookupCategoryType = {};

    var that = this;

    function prepPosts(postObject) {

      if (postObject.post_data) {
        for (var i = 0; i < postObject.post_data.length; i++) {
          var postItem = postObject.post_data[i];

          /*cut off time from date*/
          if (postItem.post_date) {
            postItem.post_date = postItem.post_date.split(' ')[0];
          }

          /*If the image url isnt a real image, cut it off*/
          if (!(postItem.image && (postItem.image.substring(0, 4) == 'http'))) {
            postItem.image = "";

          }

          that.lookupPost[postItem.ID] = postItem;
        }
        $.merge(that.posts, postObject.post_data);

      }
      $rootScope.$broadcast('postsLoaded');
    }




    this.fetching = false;
    this.getPosts = function (start, quantity) {
      var defer = $q.defer();

      if (this.fetching) {
        defer.reject("false");
        return defer.promise
      }
      this.fetching = true;

      var that = this;


      if (!start) start = 0;

      if (!quantity) quantity = 5;

      $.ajax({
        url : mlConstants.getPosts,
        data :  {action : 'getPost' , number : quantity, start : start },
        method : "POST",
        dataType : 'json',
        success : function (successData, status, jqxhr) {
          if (successData.post_data && successData.post_data.length) {
            prepPosts(successData);
            defer.resolve(true);
          }
          that.fetching = false;
          defer.resolve(false);
        },
        error : function (errorData) {
          // prepPosts(tempPosts);
          that.fetching = false;
          defer.reject(errorData);
        }
      });


      return defer.promise
    };


    this.fetchingSlider = false;


    this.getNewsSlider = function () {
      var defer = $q.defer();

      if (this.fetchingSlider) {
        defer.reject("false");
        return defer.promise
      }
      this.fetchingSlider = true;

      var that = this;

      $.ajax({
        url : mlConstants.getTopSlider,
        data :  {action : 'getSliderImages' , slider_type : 'top', cat : "" },
        method : "POST",
        dataType : 'json',
        success : function (successData, status, jqxhr) {
          if (successData.code == 200 && successData.result.length) {
            defer.resolve(successData);
          }
          that.fetchingSlider = false;
        },
        error : function (errorData) {
          that.fetchingSlider = false;
          defer.reject(false);
        }
      });


      return defer.promise
    };


    function prepFavPosts(postObject) {
      if (postObject.post_data) {
        for (var i = 0; i < postObject.post_data.length; i++) {
          var postItem = postObject.post_data[i];

          /*cut off time from date*/
          if (postItem.post_date) {
            postItem.post_date = postItem.post_date.split(' ')[0];
          }

          if (!(postItem.image && (postItem.image.substring(0, 4) == 'http'))) {
            postItem.image = ""
          }
          that.lookupPost[postItem.ID] = postItem;

          that.favouritePosts.push(postItem);
        }
      }
      $rootScope.$broadcast('favPostsLoaded');
    }

    this.fetchingFavorites = false;

    this.getFavoritePosts = function (empEmail, start, quantity) {
      var defer = $q.defer();

      if (this.fetchingFavorites) {
        defer.reject("false");
        return defer.promise
      }

      this.fetchingFavorites = true;

      var that = this;


      if (!start) start = 0;

      if(start == 0) that.favouritePosts = [];

      if (!quantity) quantity = 5;

      $.ajax({
        url : mlConstants.getUserFavoritePosts,
        data :  {action : 'getUserProfilePost', user_email : empEmail, number : quantity, start : start },
        method : "POST",
        dataType : 'json',
        success : function (successData, status, jqxhr) {
          if (successData.post_data && successData.post_data.length) {
            prepFavPosts(successData);
            defer.resolve(true);
          }
          that.fetching = false;
          defer.reject(false);
        },
        error : function (errorData) {
          // prepFavPosts(tempPosts, 0);
          that.fetchingFavorites = false;
          defer.reject(errorData);
        }
      });


      return defer.promise
    };

    this.getSinglePostById = function (postID) {
      var defer = $q.defer();

      $.ajax({
        url : mlConstants.getUserFavoritePosts,
        data :  {action : 'getUserProfilePost', post_id : postID },
        method : "POST",
        dataType : 'json',
        success : function (successData, status, jqxhr) {
          if (successData.post_data && successData.post_data.length) {
            defer.resolve(successData.post_data[0]);
          }
          defer.reject(false);
        },
        error : function (errorData) {
          // defer.resolve(tempPosts.post_data[0]);

          // defer.reject(errorData);
        }
      });


      return defer.promise
    };


    this.getPostComments = function (postId) {
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.getPostComments,
        data: {action: "fetchComments", post_id: postId},
        dataType : 'json',
        cache : true,
        success: function (successData) {
          if (successData.code && successData.code == '200') {
            defer.resolve(successData.result)
          }else if(successData.code && successData.code == '401'){
            defer.resolve("no_comments");
          }else{
            defer.reject(successData);
          }
        },
        error: function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                defer.resolve(responseTextObj.result);
              }else if(responseTextObj.code && responseTextObj.code == '401'){
                defer.resolve("no_comments");
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };


    this.createPostComment = function (postId, comment) {
      comment = emojione.toShort(comment);

      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.createPostComment,
        dataType : 'json',
        data: {action : "addPostComment", post_id : postId, comment_data : comment},
        success : function (successData) {
          if (successData.code == 200){
            defer.resolve(true);
          }else{
            defer.reject(successData);
          }
        },
        error : function (errorData) {
          defer.reject(errorData);
        }
      });

      return defer.promise
    };

    this.editPostComment = function (commentId, comment) {
      comment = emojione.toShort(comment);
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.editPostComment,
        data: {action: "editComment", comment_id: commentId, comment_content : comment},
        dataType : 'json',
        cache : false,
        success: function (successData) {
          if (successData.code && successData.code == '200') {
            defer.resolve(successData.result)
          }else if(successData.code && successData.code == '401'){
            defer.resolve("something");
          }else{
            defer.reject(successData);
          }
        },
        error: function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                defer.resolve(responseTextObj.result);
              }else if(responseTextObj.code && responseTextObj.code == '401'){
                defer.resolve("no_comments");
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };

    this.deletePostComment = function (commentId) {
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.deletePostComment,
        data: {action: "deleteComment", comment_id: commentId, force_delete : true},
        dataType : 'json',
        cache : false,
        success: function (successData) {
          if (successData.code && successData.code == '200') {
            defer.resolve(successData.result)
          }else if(successData.code && successData.code == '401'){
            defer.resolve("something");
          }else{
            defer.reject(successData);
          }
        },
        error: function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                defer.resolve(responseTextObj.result);
              }else if(responseTextObj.code && responseTextObj.code == '401'){
                defer.resolve("no_comments");
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };

    this.getPostLikes = function (postId) {
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.getPostLikes,
        data: {action: "fetchPostLikes", post_id: postId},
        dataType : 'json',
        success: function (successData) {
          if (successData.code && successData.code == '200') {
            defer.resolve(successData.result)
          }else{
            defer.reject(successData);
          }
        },
        error: function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                defer.resolve(responseTextObj.result);
              }else  defer.reject(errData);
            }else  defer.reject(errData);
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };


    this.createPostLike = function (postId) {
      var defer = $q.defer();

      $.ajax({
        method: 'POST',
        url: mlConstants.createPostLike,
        data: {action : "doLike", id  : postId,  type  : "likeThis" },
        dataType : 'json',
        success : function (successData) {
          if (successData.code && successData.code == '200') {
            var numberoflikes = 0;
            try{
              numberoflikes = successData.result.split('+')[0]
            }catch (e){
              console.log(e);
            }
            defer.resolve(numberoflikes);
          }
          defer.resolve(successData);
        },
        error : function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                var numberoflikes = 0;
                try{
                  numberoflikes =responseTextObj.result.split('+')[0];
                }catch (e){
                  console.log(e);
                }
                defer.resolve(numberoflikes);
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };

    function prepCategories(categoryArray) {
      $localStorage.categories = [];
      /*loop through all the categories*/
      for(var p = 0; p < categoryArray.length; p++){
        var tempParentObj = categoryArray[p];
        /*if it is a parent category, remove it from the parent array*/
        if (tempParentObj.parent == '0') {
          tempParentObj.sub_categories = [];
          that.lookupCategoryType[tempParentObj.term_id] = tempParentObj;
          /*loop through the array and find all of its children*/
          for(var c = 0; c < categoryArray.length; c++){
            var subItem = categoryArray[c];
            if (subItem.parent == tempParentObj.term_id) {
              subItem.parent_name = tempParentObj.name;
              subItem.parent_term_id = tempParentObj.term_id;
              tempParentObj.sub_categories.push(subItem);
              that.lookupCategoryType[subItem.term_id] = subItem;
            }
          }
          // categoryArray.splice(p, 1);
          $localStorage.categories.push(tempParentObj);
        }

      }
      $localStorage.lookupCategoryType = that.lookupCategoryType;
      $rootScope.$broadcast('categoriesLoaded');
    }

    this.getPostCategories = function () {
      var defer = $q.defer();

      if ($localStorage.categories && $localStorage.categories.length) {
        $timeout(function () {
          $rootScope.$broadcast('categoriesLoaded');
          defer.resolve(true);
        }, 1000);
        return defer.promise
      }

      $.ajax({
        method: 'POST',
        url: mlConstants.getPostCategories,
        data: {action : "fetchCategories"},
        dataType : 'json',
        success : function (successData) {
          if (successData.code == '200') {
            prepCategories(successData.result);
            defer.resolve(true);
          }else {
            defer.reject(false);
          }
        },
        error : function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code && responseTextObj.code == 200) {
                prepCategories(responseTextObj.result);
                defer.resolve(true);
              }
            }
          }else{
            defer.reject(errData);
          }
        }
      });

      return defer.promise
    };

    function prepCategoryPosts(postObject, hasPrevContent) {
      if (postObject.post_data) {
        for (var i = 0; i < postObject.post_data.length; i++) {
          var postItem = postObject.post_data[i];

          /*cut off time from date*/
          if (postItem.post_date) {
            postItem.post_date = postItem.post_date.split(' ')[0];
          }

          if (!(postItem.image && (postItem.image.substring(0, 4) == 'http'))) {
            postItem.image = ""
          }
          else {
            // if (i < 5) {/*load images for slider*/
            // that.categoryPostsForSlider.push(postItem)
            // }
          }
          that.lookupCategoryPost[postItem.ID] = postItem;
        }
        if (hasPrevContent == 0) {
          that.categoryPosts = postObject.post_data;
        }else{
          $.merge(that.categoryPosts, postObject.post_data);
        }
      }
      $rootScope.$broadcast('categoryPostsLoaded');
    }


    this.getCategoryPosts = function (category_id, start, quantity) {
      var defer = $q.defer();

      if (!quantity) quantity = 5;

      if (!start) start = 0;

      $.ajax({
        url : mlConstants.getPostUnderACategory,
        data :  {action : 'getPostByCategory', cat_id : category_id, number : quantity, start : start },
        method : "POST",
        dataType : 'json',
        success : function (successData) {
          if (successData.code == 200) {
            if ($.trim(successData.message) == 'Query returned 0 Rows') {
              $rootScope.$broadcast('categoryPostsLoaded');
              defer.resolve(false);
            }
          }else{
            prepCategoryPosts(successData, start);
          }
          defer.resolve(true);
        },
        error : function (errData) {
          if (errData.responseText) {
            var responseTextObj = JSON.parse(errData.responseText);
            if (angular.isObject(responseTextObj)) {
              if (responseTextObj.code == 200) {
                if ($.trim(responseTextObj.message) == 'Query returned 0 Rows') {
                  $rootScope.$broadcast('categoryPostsLoaded');
                  defer.resolve(false);
                }
              }else{
                prepCategoryPosts(responseTextObj, start);
              }
              defer.resolve(true);
            }
          }else{
            defer.reject(errData);
          }
        }
      });


      return defer.promise
    };

  });
