/**
 * Created by Kaygee on 21/03/2016.
 */

angular.module('mLife')
  .service('mlWorkDayService', function(mlConstants, $http, $q,
                                        $rootScope, $ionicLoading, $timeout) {

    this.getEmployeesInAnOffice = function (office, officeId) {
      var defer = $q.defer();

      $ionicLoading.show({
        template: 'Getting employees in ' + office,
        duration : 30000,
        noBackdrop : true
      });

      var empData = {"message":"SUCCESS","data":[{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"georgina.bitcon@meltwater.com","phone":"03 9667 9506 ext. 306","firstName":"Georgina","lastName":"Bitcon","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Manager-AUS","skype":"georgiebitcon","salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_9365_today_1412057775819.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=9365"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"beena.yamin@meltwater.com","phone":"03 9667 9514 exr. 313","firstName":"Beena","lastName":"Yamin","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Manager - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_10127_today_1431858118646.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=10127"},{"errorCode":"200","active":"true","manager":"David Hickey","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"cimon.constantine@meltwater.com","phone":"03 9667 9505 ext. 305","firstName":"Cimon","lastName":"Constantine","salesOffice":"CA Melbourne","area":"Australia","title":"Managing Director-AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":"https://au.linkedin.com/in/cimonc","userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_2417_today_1406084355137.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=2417"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"emma.boccalatte@meltwater.com","phone":null,"firstName":"Emma","lastName":"Boccalatte","salesOffice":"CA Melbourne","area":"Australia","title":"Contractor - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=null","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=10463"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"amira.enayatzada@meltwater.com","phone":null,"firstName":"Amira","lastName":"Enayatzada","salesOffice":"CA Melbourne","area":"Australia","title":"Intern - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=null","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=11331"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"michael.oneill@meltwater.com","phone":null,"firstName":"Michael","lastName":"O'Neill","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Consultant - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_11741_today_1475630452760.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=11741"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"amanda.miltinan@meltwater.com","phone":null,"firstName":"Amanda","lastName":"Miltinan","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Consultant - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=user_11747_today_1466655165617.jpg","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=11747"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"steven.wan@meltwater.com","phone":null,"firstName":"Steven","lastName":"Wan","salesOffice":"CA Melbourne","area":"Australia","title":"Intern - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=null","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=12235"},{"errorCode":"200","active":"true","manager":"Cimon Constantine","errorMessage":"","division":"Client Acquisition","superArea":"APAC","email":"cushla.mccarthny@meltwater.com","phone":null,"firstName":"Cushla","lastName":"McCarthny","salesOffice":"CA Melbourne","area":"Australia","title":"Sales Consultant - AUS","skype":null,"salesPerson":"true","twitter":null,"linkedin":null,"userImageLink":"http://admin.meltwater.com/superadmin/public/image.html?file=null","userProfileLink":"http://admin.meltwater.com/superadmin/userprofile/detail.html?id=12343"}],"description":null,"errorMessages":[],"code":"200","success":"true"};

      /*fetch employee info on WorkDay*/
      $http.post(mlConstants.allEmployeesInAnOffice+'?territoryId='+officeId, {}, {cache : true})
        .success(function (successData, status, jqxhr) {
          if (successData.code == "200") {
            defer.resolve(successData.data);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
          defer.resolve(empData.data);
        }).finally(function () {
          $ionicLoading.hide();
      });
      return defer.promise
    };


    this.getEmployeeInfo = function (userEmail) {
      var defer = $q.defer();

      $ionicLoading.show({
        template: 'Getting employee details...',
        duration : 30000,
        noBackdrop : true
      });


      if (userEmail == 'info@pollafrique.com') {
        userEmail = 'belen.aleman@meltwater.com';
      }

      /*fetch employee info on WorkDay*/
      $http.post(mlConstants.workDay+'?email='+userEmail, {}, {cache : true})
        .success(function (successData, status, jqxhr) {
          if (successData.errorCode == "200") {
            defer.resolve(successData);
          }
          defer.reject(false);
        })
        .error(function (errorData) {
          // prepPosts(tempPosts, 0);
          defer.resolve({
            active: "false",
            userImageLink: "http://a2.mzstatic.com/us/r30/Purple69/v4/dc/55/d1/dc55d1e8-57a0-3b50-25cb-68de23ad2c74/icon350x350.png",
            area: "Unavailable",
            email: "Unavailable",
            errorCode: "200",
            errorMessage: "",
            firstName: "Details",
            lastName: "Unavailable",
            manager: "Unavailable",
            salesOffice: "Unavailable",
            superArea: "Unavailable",
            title: "Unavailable"
          });
        }).finally(function () {
        $ionicLoading.hide();
      });
      return defer.promise
    };



  });
