/**
 * Created by Kaygee on 21/03/2016.
 */

/*

var users = [
  {"ID":"306","user_login":"aaron.arakawa@meltwater.com","user_nicename":"aaron-arakawameltwater-com","user_email":"aaron.arakawa@meltwater.com","display_name":"Aaron Arakawa"},
  {"ID":"376","user_login":"aaron.galliner@meltwater.com","user_nicename":"aaron-gallinermeltwater-com","user_email":"aaron.galliner@meltwater.com","display_name":"Aaron Galliner"},
  {"ID":"1054","user_login":"aaron.low@meltwater.com","user_nicename":"aaron-lowmeltwater-com","user_email":"aaron.low@meltwater.com","display_name":"aaron.low@meltwater.com"},
  {"ID":"535","user_login":"aaron.soh@meltwater.com","user_nicename":"aaron-sohmeltwater-com","user_email":"aaron.soh@meltwater.com","display_name":"aaron.soh@meltwater.com"},
  {"ID":"21","user_login":"abhinav","user_nicename":"abhinav","user_email":"abhinav.bhardwaj@a3logics.in","display_name":"Abhinav","push_id":"jhjhdas"},
  {"ID":"1161","user_login":"adam.brender@meltwater.com","user_nicename":"adam-brendermeltwater-com","user_email":"adam.brender@meltwater.com","display_name":"adam.brender@meltwater.com"},
  {"ID":"1099","user_login":"adam.dealy@meltwater.com","user_nicename":"adam-dealymeltwater-com","user_email":"adam.dealy@meltwater.com","display_name":"adam.dealy@meltwater.com"},
  {"ID":"647","user_login":"adam.kragerud@meltwater.com","user_nicename":"adam-kragerudmeltwater-com","user_email":"adam.kragerud@meltwater.com","display_name":"adam.kragerud@meltwater.com"},
  {"ID":"774","user_login":"adam.naasz@meltwater.com","user_nicename":"adam-naaszmeltwater-com","user_email":"adam.naasz@meltwater.com","display_name":"adam.naasz@meltwater.com"}];
*/

angular.module('mLife')
  .service('mlAuthService', function(mlConstants, $http, $q, $localStorage, $timeout,  $rootScope,
                                     mlWorkDayService, mlFirebaseService, $state, $ionicLoading, CacheFactory) {

    var that = this;

    if (!CacheFactory.get('userCache')) {
      CacheFactory.createCache('userCache', {
        deleteOnExpire: 'aggressive',
        maxAge: 30 * 60 * 1000, // 30minutes
        recycleFreq: 5 * 60 * 1000,
        onExpire: function (key, value) {
          that.getUsersList()
        }
      });
    }

    var userCache = CacheFactory.get('userCache');



    /*this checks if the user is valid*/
    this.authenticateUser = function (loginData) {
      var defer = $q.defer();

      $http({
        method: 'POST',
        url: mlConstants.authenticateUser,
        //data: $.param(dataToPost),
        data: loginData
        // headers: {
        //   //'Content-Type': 'application/x-www-form-urlencoded',
        //   'Content-Type': 'application/json; charset=utf-8',
        //   'Access-Control-Allow-Origin': '*'
        // }
      })
        .success(function (successData) {
          if (successData.code == 200) {
            defer.resolve(true);
          }
        })
        .error(function (errorData) {
          defer.reject(false);
        });
      return defer.promise
    };

    /*this logs the user in*/
    function postAuthedUser(successData, defer, paEmail) {

      $localStorage.user = {
        data : {},
        roles : {}
      };

      if (successData.code == 200) {

        $localStorage.authorization = successData['ACCESS-TOKEN'];

        $.ajaxSetup({
          headers: {"ACCESS-TOKEN": $localStorage.authorization}
        });

        $localStorage.user = successData.result;

        mlWorkDayService.getEmployeeInfo($localStorage.user.data.user_email)
          .then(function (userData) {

            if (!userData.userImageLink) {
              userData.userImageLink = "http://a2.mzstatic.com/us/r30/Purple69/v4/dc/55/d1/dc55d1e8-57a0-3b50-25cb-68de23ad2c74/icon350x350.png"
            }

            angular.merge($localStorage.user.data, userData);

            delete $localStorage.user.data.active;
            delete $localStorage.user.allcaps;
            delete $localStorage.user.cap_key;
            delete $localStorage.user.caps;
            delete $localStorage.user.data.errorCode;
            delete $localStorage.user.data.errorMessage;
            delete $localStorage.user.filter;
            delete $localStorage.user.data.linkedin;
            delete $localStorage.user.data.phone;
            delete $localStorage.user.roles;
            delete $localStorage.user.data.twitter;
            delete $localStorage.user.data.skype;
            delete $localStorage.user.data.user_activation_key;
            delete $localStorage.user.data.user_pass;
            delete $localStorage.user.data.user_status;
            delete $localStorage.user.data.user_url;

            mlFirebaseService.updateUserLogin();

            $state.go('home.posts');

            /*Setup Notification checker on Firebase*/
            mlFirebaseService.retrieveUserNotifications();

            $ionicLoading.hide();


          }, function () {
            console.log('userData error');
          });

        defer.resolve(true);
      }
      else if(paEmail && paEmail.search('pollafrique.com') > 0){
        $localStorage.user.data.area = "Global";
        $localStorage.user.data.division = "Office of the Developers";
        $localStorage.user.data.email = $localStorage.user.data.user_email;
        $localStorage.user.data.manager = "Samuel Dzidzornu";
        $localStorage.user.data.salesOffice = "Software Development";
        $localStorage.user.data.salesPerson = "false";
        $localStorage.user.data.title = "App Developer";


        $state.go('home.posts');

        /*Setup Notification checker on Firebase*/
        mlFirebaseService.retrieveUserNotifications();

        $ionicLoading.hide();

        defer.reject(successData);
      }else {
        defer.reject(successData);
      }
    }

    this.loginUser = function (userEmail) {
      var defer = $q.defer();

      var loginData = {
        action : "login",
        username : userEmail
      };

      $.ajax({
        url : mlConstants.loginUser,
        data : loginData,
        method : "POST",
        dataType : 'json',
        success : function (successData) {
          postAuthedUser(successData, defer, userEmail);
        },
        error : function (errData, err2, err3) {
          defer.resolve(true);
        }
      });

      return defer.promise
    };


    /*this functions adds the label and value properties to the user objects*/
    function prepUserList(){
      for (var i = 0; i < that.userList.length; i++) {
        var user = that.userList[i];
        user.label = user.display_name;
        that.userList[i] = user;
      }
      $localStorage.userList = that.userList;
      $rootScope.$broadcast('usersLoaded');
    }

    /*this gets all users in the system*/
    this.getUsersList = function () {
      var defer = $q.defer(),

        cachedData = userCache.get(mlConstants.getUsersList);

      // that.userList = users;
      // prepUserList();
      // return;

      if (!cachedData) {
        $.ajax({
          url: mlConstants.getUsersList,
          data: {'action': 'getUsersList'},
          method: "POST",
          dataType: 'json',
          success: function (successData, status, jqxhr) {
            if (successData.code == 200) {
              that.userList = successData.result;
              userCache.put(mlConstants.getUsersList, successData.result);
              prepUserList();
              defer.resolve(true);
            }
          },
          error: function (errData, err2, err3) {
            defer.reject(false);
          }
        });
      }else{
        that.userList = cachedData;
        prepUserList();
        defer.resolve(true);
      }


      return defer.promise
    };


    /*this stores/updates a users push ID in the system*/
    this.storeUserPushId = function (pushID) {
      var defer = $q.defer();

      $.ajax({
        url : mlConstants.storeUserPushId,
        data : {'action' : 'storePushId', 'push_id' : pushID},
        method : "POST",
        dataType : 'json',
        headers : {'ACCESS-TOKEN' : $localStorage.authorization },
        success : function (successData, status, jqxhr) {
          if (successData.code == 200) {
            defer.resolve(successData);
          }
        },
        error : function (errData, err2, err3) {
          defer.reject(errData);
        }
      });


      return defer.promise
    };

    this.authWithSSO = function () {
      var appSecret = "1YduikPmGc7adywt";

      var tokenKey = "hrmbdjt8XrkwhMOI";

      var customerId = "41865";

      // var publicKey = "<CONTENT_OF_DOWNLOADED_CERTIFICATE";
      var publicKey = KEYUTIL.getKey(mlConstants.miniOrangeCertificate);

      var redirectUrl = "https://auth.miniorange.com/moas/broker/login/jwt/"

        + customerId;

      var responseUrl = "https://auth.miniorange.com/moas/jwt/mobile";


      var currentTimestamp = Date.now();

      var inputString = currentTimestamp + ":" + appSecret;

      var keyHex = CryptoJS.enc.Utf8.parse(tokenKey);

      var cipherText =

        CryptoJS.enc.Base64.stringify(CryptoJS.AES.encrypt(inputString,

          keyHex, {mode: CryptoJS.mode.ECB}).ciphertext);

      redirectUrl += "?token=" + encodeURIComponent(cipherText);

      // Note: The cipherText needs to URL Encoded.

      var ref = cordova.InAppBrowser.open(redirectUrl, '_blank',
        // var ref = window.open(redirectUrl, '_blank',
        'location=yes');


      ref.addEventListener('loadstop', function(event){

        //Read URL of In App Browser

        var eventUrl = event.url;

        if(eventUrl.indexOf(responseUrl) !== -1){
          var idToken = eventUrl.substring(eventUrl.indexOf("id_token") + 9, event.url.length);

          //  all user's details are in idToken
          var payloadObj = KJUR.jws.JWS.readSafeJSONString(b64utoutf8(idToken.split(".")[1]));

          var subject = payloadObj.sub;

          if (validateSignature(idToken, publicKey) && validateJWTToken(idToken, publicKey)) {

            payloadObj = KJUR.jws.JWS.readSafeJSONString(b64utoutf8(idToken.split(".")[1]));

            subject = payloadObj.sub;

            ref.close();

            // postAuthedUser(idToken, subject);
            that.loginUser(subject);

            $timeout(function(){
              $state.go('home.posts');
            });
            // Note: JWT Subject contains the values received in the NameID of the SAML Response from ADFS.
          }
        }


      });
    };


    function validateSignature(token, publicKey){

      return KJUR.jws.JWS.verify(token, publicKey,

        ['RS256']);

    }

    function validateJWTToken(token, publicKey){

      var acceptField = {

        alg: ['RS256', 'RS512', 'PS256', 'PS512'],

        iss: ['MO'],

        aud: ['https://auth.miniorange.com/moas/jwt/mobile']

      };

      return KJUR.jws.JWS.verifyJWT(token, publicKey, acceptField);

    }

  });
