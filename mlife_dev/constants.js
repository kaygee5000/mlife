/**
 * Created by Kaygee on 08/03/2016.
 */

var baseURL = "http://mlife.meltwater.com/wp-content/themes/mlife_mobile_api/api-v1.php";
// var baseURL = "/api-v1.php";
var superAdminURL = "https://admin.meltwater.com/superadmin/rest/";
// var superAdminURL = "/sales.ws/";
var firebaseURL = 'https://mlife-1287.firebaseio.com';
// var firebaseURL = '/firebasecom';


angular.module('mLife')
  .constant('mlConstants', {
    /*this checks if the user is valid*/
    authenticateUser : baseURL + "?action=authenticateUser",

    /*this logs in a user and returns access token with user privileges*/
    loginUser : baseURL /*"?action=login"*/,

    /*this allows a LOGGED IN user to change his password*/
    resetPassword : baseURL + "?action=resetPassword",

    /*get posts*/
    getPosts : baseURL + "?action=getPost",

    /*get slider images for news tab*/
    getTopSlider : baseURL + "?action=getSliderImages",


    /*get comments for a post*/
    getPostComments : baseURL + "?action=fetchComments",

    /*create comment on a post*/
    createPostComment : baseURL + "?action=addPostComment",

    /*edit comment on a post*/
    editPostComment : baseURL + "?action=editComment",

    /*delete comment on a post*/
    deletePostComment : baseURL + "?action=deleteComment",

    /*get comments for a post*/
    getPostLikes : baseURL + "?action=fetchPostLikes",

    /*create comment on a post*/
    createPostLike : baseURL + "?action=doLike",

    /*get categories of the posts*/
    getPostCategories : baseURL + "?action=fetchCategories",

    /*get post of a category*/
    getPostUnderACategory : baseURL + "?action=getPostByCategory",

    /*get all users in the system*/
    getUsersList : baseURL + "?action=getUsersList",

    /*get all users in the system*/
    getUserFavoritePosts : baseURL + "?action=getUserProfilePost",

    /*get all users in the system*/
    storeUserPushId : baseURL + "?action=storePushId",

    /*fetch data from mw Super Admin*/
    superAdmin : superAdminURL + 'today/sales.ws',

    /*do Like of Today Sale*/
    todaySaleLike : superAdminURL + 'today/likeToggle.ws',

    /*do comment on Today Sale*/
    todaySaleComment : superAdminURL + 'today/createComment.ws',

    /*fetch employee sales history from mw Super Admin*/
    salesHistory : superAdminURL + 'employee/getRecentSales.ws',

    /*fetch territory sales history from mw Super Admin*/
    territorySaleHistory : superAdminURL + 'employee/getRecentSalesByTerritory.ws',

    /*fetch data from mw WorkDay*/
    workDay : superAdminURL + 'employee/detail.ws',

    /*fetch all territories*/
    allTerritories : superAdminURL + 'territory/territories.ws',

    /*fetch data from mw WorkDay*/
    allAreasBySuperArea : superAdminURL + 'territory/getAreasBySuperArea.ws',

    /*fetch data from mw WorkDay*/
    allOfficesByArea : superAdminURL + 'territory/getOfficesByArea.ws',

    /*fetch data from mw WorkDay*/
    allEmployeesInAnOffice : superAdminURL + 'employee/getByTerritory.ws',

    /*Push Notification API Token*/
    securityProfile : 'mlife_security_profile',

    /*Push Notification API Token*/
    firebaseUrl  : firebaseURL,


    image_base_url: 'https://s3-ap-southeast-1.amazonaws.com/mogoimagecloud/',
    aws_username :"kaygee",/*this is for mogo cloud*/
    aws_image_bucket :"mogoimagecloud",/* mogoimagecloud - name this is for mogo cloud*/
    aws_bucket_region :"ap-southeast-1",/* - bucket region this is for mogo cloud*/
    aws_access_key : "AKIAIMO3YLSCEWCFZHIQ",/* key for mogocloud*/
    aws_secret_access_key : "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",/* - secret for mogocloud*/

    /*Push Notification API Token Ionic Push*/
    // pushTokenAPI : 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJiYTdmMDM0Mi1mOWUxLTQxODgtYjE0NC1iOWRlNDNlNGJjYTcifQ  .buEkLDiDp5INdfL-tYYHEpFLclyeze2qXBDv3hjsYgk'
    pushTokenAPI : 'MjBhMTYyOTAtZGFiNy00ZjA3LTkxMWYtYmRhYTYzMzU3NTI2',


    miniOrangeCertificate : '-----BEGIN CERTIFICATE-----MIID/zCCAuegAwIBAgIJAPmPOnjOfbO/MA0GCSqGSIb3DQEBCwUAMIGVMQswCQYDVQQGEwJJTjEUMBIGA1UECAwLTUFIQVJBU0hUUkExDTALBgNVBAcMBFBVTkUxEzARBgNVBAoMCk1JTklPUkFOR0UxEzARBgNVBAsMCk1JTklPUkFOR0UxEzARBgNVBAMMCk1JTklPUkFOR0UxIjAgBgkqhkiG9w0BCQEWE2luZm9AbWluaW9yYW5nZS5jb20wHhcNMTYwMTMwMDgyMzE0WhcNMjYwMTI3MDgyMzE0WjCBlTELMAkGA1UEBhMCSU4xFDASBgNVBAgMC01BSEFSQVNIVFJBMQ0wCwYDVQQHDARQVU5FMRMwEQYDVQQKDApNSU5JT1JBTkdFMRMwEQYDVQQLDApNSU5JT1JBTkdFMRMwEQYDVQQDDApNSU5JT1JBTkdFMSIwIAYJKoZIhvcNAQkBFhNpbmZvQG1pbmlvcmFuZ2UuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsSKksJcuQbZm/cw67JJkHvlzrzd3+aPLhfqy/FCKj/BmZ7mBZf0s3+89O0rOamsZP1NKzOE29ZL6ONLJHxU48LUbGciZupm/wf7dY/Av34uDcZ61ufKz0u17UTxgjLULIWBy//68EOr4Xbm/4bqhmKcB3CclMaom0LWeCrqittiLqunVCjFIbxXMT010WiBBnFwqjUqfuMnVLL+HrPPqgPqNhiKDdxBxHk9GDPq2+GEruM1jw7TUjVa+zbhekvdNTbj2Fo2sqf+mIkFLSaS6cHg0UeL7sX0wvQFDMwx+hpfRLMcYpFAmRMn3dI2zUnPgwvWeKHrnNOjHVkRTV4hgZQIDAQABo1AwTjAdBgNVHQ4EFgQU85bt1wVl/f2LftBu1jeO499VUbYwHwYDVR0jBBgwFoAU85bt1wVl/f2LftBu1jeO499VUbYwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEApenCY36LGThXLAIRIvDQ6XnHELL7Wm53m3tHy+GA2MxUbcTqQC3tgXM+yC18EstjRHgWdQMtOcq9ohb5/TqWPoYAYnbg1SG9jEHJ3LLaIMI0idYo+zfPCtwliHLnsuZXH6ZU3mh/IQEBqINini6R/cSh9BpIjqwXKpjWoegl9XLI/RQ7Bbbya89TUBwm5KR3deWXdMZEj/d7hV8XdSWyi2CvWTeHIfkZVhcHg1ues9+Mt3kaBr4Z5/NkQPANjfMdKjZ8tfNTN7PgYAYyRW6C8aXcw+w0zIoGrcO2gVM9/3oR4gHm5MUHOMdAyONkg59+T+7NDlN7y4YmVIZQBgHByA==-----END CERTIFICATE-----'


  });
