/**
 * Created by Kaygee on 21/03/2016.
 */


angular.module('mLife')
  .filter('newlines', function () {
    return function(text) {
      if(text) return text.replace(/\n/g, '<br/>');
      else return '';
    }
  })
  .filter('breaks', function () {
    return function(text) {
      if(text) return text
        .replace('&nbsp;', ' ')
        .replace('&amp;', '&')
        // .replace(/&/g, '&amp;')
        // .replace(/>/g, '&gt;')
        // .replace(/</g, '&lt;');
        ;return '';
    }
  }).filter('entityReplace', function () {
  return function(text) {
    if (text) return text
      .replace('&nbsp;', ' ')
      .replace('&amp;', '&')
      .replace('&gt;', '>')
      .replace('&lt;', '>');
    else return '';
  }
}).filter('parseWpShortCodes', function () {
  return function (input) {
    if (input) return input
      .replace(/\[([a-z0-9])+([^\]]+)*\]/g, '')
      .replace(/\[\/[a-z]+\]/g, '');
    else return '';

  };
})
  .filter('hrefToJS', function ($sce, $sanitize) {
    return function (text) {
      var hrefLink = /href="([\S]+)"/g;
      var targetAtr = /target="_blank"/g;
      var widthReplacement = /width="[\S]+"/g;
      var heightReplacement = /height="[\S]+"/g;
      var iframeDivParent = /<div><iframe/g;
      var iframePeeParent = /<p><iframe/g;
      var styleRemover = /style="[^"]+"/g;
      var iframeUrlProtocol = /<iframe([\w\d\s="])*src="([htps]+)/g;
      var brainsharkUrl = /https:\/\/www\.brainshark\.com\/meltwatergroupg/g;

      var newString = /*$sanitize*/(text)
        .replace(hrefLink, "href=\"#\" onclick=\"return openExtUrlInSystemBrowser('$1')\" ")
        .replace(targetAtr, "target=\"_system\"")
        .replace(widthReplacement, "")
        .replace(heightReplacement, "")
        .replace(styleRemover, " style=\"\" ")
        .replace(iframeDivParent, "<div class=\"iframeParent\"><iframe")
        .replace(iframePeeParent, "<p class=\"iframeParent\"><iframe");

      newString.replace(brainsharkUrl, "www.pollafrique.com");



      // .replace(iframeUrlProtocol, "<div class=\"iframeParent\"><iframe");

      return $sce.trustAsHtml(newString);/*onClick=\" cordova.InAppBrowser.open('$1',  '_system', 'location=yes')\"*/
    }
  })
  .filter('toTrusted', function ($sce) {
    return function (text) {
      return $sce.trustAsHtml(text);
    }
  })

  .filter('trustedResourceUrl', function ($sce) {
    return function (text) {
      return $sce.trustAsResourceUrl(text);
    }
  })

  .filter('reverseArray', function() {
    return function(items) {
      return items.slice().reverse();
    };
  });
